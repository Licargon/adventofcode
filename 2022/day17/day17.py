
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

from tqdm import tqdm

with open('day17.in') as f:
    lines = [line.strip() for line in f.readlines()]

instructions = lines[0]


class Tetris:
    B1 = [['#', '#', '#', '#']]
    B2 = [['.', '#', '.'], ['#', '#', '#'], ['.', '#', '.']]
    B3 = [['.', '.', '#'], ['.', '.', '#'], ['#', '#', '#']]
    B4 = [['#'], ['#'], ['#'], ['#']]
    B5 = [['#', '#'], ['#', '#']]


    BLOCKS = [B1, B2, B3, B4, B5]
    GRID_HEIGHT = 2000

    def __init__(self, instructions):
        self.G = set() # Set of all blocks
        self.instructions = instructions
        self.block_idx = 0
        self.ins_idx = 0

    def get_block(self):
        return self.BLOCKS[self.block_idx % len(self.BLOCKS)]

    def print(self):
        max_y = self.drop_position()[0]
        for r in range(max_y, -1, -1):
            row = []
            for c in range(0, 7):
                if (r, c) in self.G:
                    row.append('#')
                else:
                    row.append('.')
            print(f'{r:04}', row)
        print()

    def drop_position(self):
        """
        Each rock appears so that its left edge is two units away from the left wall and its bottom edge is three units
        above the highest rock in the room (or the floor, if there isn't one).
        """
        # Get the highest Y position of any block currently in the system
        if self.G:
            max_y = max(y for (y, x) in self.G) + 1
        else:
            max_y = 0
        block_height = len(self.BLOCKS[self.block_idx % len(self.BLOCKS)])
        return max_y + 2 + block_height, 2

    def drop_block(self):
        block = self.BLOCKS[self.block_idx % len(self.BLOCKS)]

        y, x = self.drop_position()

        while True:
            instruction = self.instructions[self.ins_idx % len(self.instructions)]
            self.ins_idx += 1

            blocked = False
            if instruction == '<':
                # Check if any collission on the left
                for ri, r in enumerate(block):
                    # Get the left most block to check for colission
                    if '#' in r:
                        idx = r.index('#')
                        if (y-ri, x + idx-1) in self.G:
                            blocked = True
                            break
                if not blocked:
                    # Check if we can move left
                    if x > 0:
                        x -= 1
            else:
                # Check for collission on the right
                for ri, r in enumerate(block):
                    # Find last index of #, if applicable
                    if '#' in r:
                        idx = 0
                        for ii, cc in enumerate(r):
                            if cc == '#':
                                idx = ii
                        # idx now points to the right-most occurrence of #
                        # coord = (y-ri, x+len(block[0]))
                        coord = (y-ri, x+idx+1)
                        if coord in self.G:
                            blocked = True
                            break
                if not blocked:
                    # Check if we can move the entire block right
                    if x + len(block[0]) < 7:
                        x += 1

            blocked = False
            # Check for bottoming out
            if y - 1 < 0:
                blocked = True
            else:
                # Need to check this for all rows because the cross-block can collide vertically in the
                # middle row
                for ri, r in enumerate(block):
                    for i, c in enumerate(r):
                        if c != '#':
                            # Cant collide with anything
                            continue
                        # coord = (y - len(block), x+i)
                        coord = (y-ri-1, x+i)
                        if coord in self.G:
                            blocked = True
                            break
            if blocked:
                break
            else:
                # Move the entire block down as nothing is blocking us
                y -= 1

        for r, rc in enumerate(block):
            for c, cc in enumerate(rc):
                if cc == '#':
                    new_coord = (y-r, x+c)
                    self.G.add(new_coord)
        # Switch to next block
        self.block_idx += 1

    def top_40_rows_fingerprint(self):
        height = max(y for (y, x) in T.G)
        return frozenset([(height-y, x) for (y, x) in self.G if height-y <= 40])

    def top(self):
        return max(y for (y, x) in self.G) + 1


T = Tetris(instructions)
# Unique key -> (cycle index, height)
cache = {}
END = 1000000000000
CUR = 0
to_add = 0
while CUR < END:
    T.drop_block()
    if CUR == 2022:
        print('Part 1: ', max(y for (y, x) in T.G) + 1)
    CUR += 1
    # Key is instruction idx, block idx, top 40 rows
    key = (T.ins_idx % len(instructions), T.block_idx % len(T.BLOCKS), T.top_40_rows_fingerprint())
    if key in cache and CUR > 2022:
        cycle_diff = CUR - cache[key][0]
        cur_height = max(y for (y, x) in T.G) + 1
        height_diff = cur_height - cache[key][1]
        times_to_add = (END-CUR) // cycle_diff
        to_add += times_to_add * height_diff
        CUR += times_to_add * cycle_diff # Jump ahead
        # print('Cycle found, jumping ahead by ', times_to_add * cycle_diff)
        # print('New cur: ', CUR)

    else:
        cache[key] = (CUR, max(y for (y, x) in T.G) + 1)

print('Part 2: ', T.top() + to_add)
