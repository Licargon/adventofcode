# -*- coding: utf-8 -*-

from functools import cmp_to_key

with open('day13.in') as f:
    lines = [line.strip() for line in f.readlines()]

pairs = []

cur_pair = []
for line in lines:
    if line == '':
        pairs.append(cur_pair)
        cur_pair = []
    else:
        cur_pair.append(eval(line))
pairs.append(cur_pair)


"""
None -> Equal
True -> correct (right is higher)
False -> incorrect (left is higher)
"""
def parse(left, right, level=0, verbose=False):
    if verbose:
        print('-' * level * 4, left)
        print('-' * level * 4, right)
        print()
    try:
        if isinstance(left, int) and isinstance(right, int):
            if left == right:
                return None
            elif left < right:
                return True
            elif right < left:
                return False
        elif isinstance(left, list) and isinstance(right, list):
            # Loop over and recurse where needed
            idx = 0
            while idx < len(left) and idx < len(right):
                ll = left[idx]
                rr = right[idx]
                res = parse(ll, rr, level=level+1, verbose=verbose)
                idx += 1
                if res is None:
                    continue
                else:
                    return res
            # Left exhausted, right not: Correct
            if idx == len(left) and idx < len(right):
                return True
            # Right exhausted and left not: incorrect
            elif idx == len(right) and idx < len(left):
                return False
            #Equal
            else:
                return None
        elif isinstance(left, int) and isinstance(right, list):
            left_ = [left]
            return parse(left_, right, level=level+1, verbose=verbose)
        elif isinstance(left, list) and isinstance(right, int):
            right_ = [right]
            return parse(left, right_, level=level+1, verbose=verbose)
    except Exception as e:
        print(e)
        raise

p1 = []
all_packets = []
# Counting is 1 based.
for idx, pair in enumerate(pairs, start=1):
    if parse(pair[0], pair[1]):
        p1.append(idx)
    all_packets.extend(pair)

print(p1)
print('Part 1: ', sum(p1))
# 6520 too high
# 6519 too high

all_packets.append([[2]])
all_packets.append([[6]])

def cmp_function(left, right):
    res = parse(left, right)
    if res is None:
        return 0
    elif res == True:
        return -1
    elif res == False:
        return 1

sorted_packets = sorted(all_packets, key=cmp_to_key(lambda l, r: cmp_function(l, r)))

p2 = (sorted_packets.index([[2]]) + 1) * (sorted_packets.index([[6]]) + 1)
print('Part 2: ', p2)
# 90902 too high

print('==== TESTCASES')
assert parse([1,1,3,1,1], [1,1,5,1,1]) == True
assert parse([[1],[2,3,4]], [[1],4]) == True
assert parse([9],[[8,7,6]]) == False
assert parse([[4,4],4,4], [[4,4],4,4,4]) == True
assert parse([7,7,7,7], [7,7,7]) == False
assert parse([], [3]) == True
assert parse([[[]]], [[]]) == False
assert parse([1,[2,[3,[4,[5,6,7]]]],8,9], [1,[2,[3,[4,[5,6,0]]]],8,9]) == False
