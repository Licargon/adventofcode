# -*- coding: utf-8 -*-

with open('day10.in') as f:
    lines = [line.strip() for line in f.readlines()]

X = 1
tick = 0
p1 = 0

D = [['X' for _ in range(40)] for _ in range(6)]


def process_tick(tick, X, p1, D):
    if tick in [20, 60, 100, 140, 180, 220]:
        p1 += tick * X
    if (tick-1) % 40 in [X-1, X, X+1]:
        D[(tick-1)//40][(tick-1) % 40] = '#'
    else:
        D[(tick-1) // 40][(tick-1) % 40] = ' '
    return p1

for line in lines:
    s = line.split(' ')
    if s[0] == 'noop':
        tick += 1
        p1 = process_tick(tick, X, p1, D)
    elif s[0] == 'addx':
        tick += 1
        p1 = process_tick(tick, X, p1, D)
        tick += 1
        p1 = process_tick(tick, X, p1, D)
        X += int(s[1])


print('Part 1: ', p1)
print('Part 2:')
for i in range(6):
    print(''.join(D[i]))
