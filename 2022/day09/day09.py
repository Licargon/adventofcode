# -*- coding: utf-8 -*-

with open('day09.in') as f:
    lines = [line.strip() for line in f.readlines()]


def print_grid(S):
    for i in range(-5, +5):
        row = []
        for j in range(-5, +5):
            if S[0] == (i, j):
                row.append('H')
            elif S[1] == (i, j):
                row.append('T')
            else:
                row.append('.')
        print(row)
    print()
    print()

def catchup(first, last):
    dy = first[0] - last[0]
    dx = first[1] - last[1]
    if abs(dy) + abs(dx) > 2: # diagonal
        offset_y = -1 if last[0] > first[0] else 1
        offset_x = -1 if last[1] > first[1] else 1
        return (last[0] + offset_y, last[1] + offset_x)
    elif abs(dy) >= 2: # vertical
        offset = -1 if last[0] > first[0] else 1
        return (last[0] + offset, last[1])
    elif abs(dx) >= 2: # horizontal
        offset = -1 if last[1] > first[1] else 1
        return (last[0], last[1] + offset)
    else: # Stay in position
        return last

def solve():
    # 0 will always be the head, the rest will be the tail
    VISUALIZE = False
    S = []
    S.append((0,0))
    for _ in range(9):
        S.append((0,0))

    assert len(S) == 10

    DX = {'L': -1, 'R': 1, 'U': 0, 'D': 0}
    DY = {'L': 0, 'R': 0, 'U': -1, 'D': 1}
    tail_1_positions = set()
    tail_9_positions = set()
    for line in lines:
        dir = line.split(' ')[0]
        mag = int(line.split(' ')[1])
        for _ in range(mag):
            HEAD = S[0]
            S[0] = (HEAD[0] + DY[dir], HEAD[1] + DX[dir])
            if VISUALIZE:
                print_grid(S)
            for i in range(1, 10):
                S[i] = catchup(S[i-1], S[i])
            tail_1_positions.add(S[1])
            tail_9_positions.add(S[9])
            if VISUALIZE:
                print_grid(S)
    print('Part 1: ', len(tail_1_positions))
    print('Part 2: ', len(tail_9_positions))
solve()
