# -*- coding: utf-8 -*-

from collections import deque

TOTAL_MODULOS = 1


class Monkey:
    def __init__(self):
        self.idx = 0
        self.inspected = 0
        self.items = deque([])
        self.test_divisible = None
        self.operation = ''
        self.if_true = None
        self.if_false = None

    def throw(self, part2=False):
        global TOTAL_MODULOS
        item = self.items.popleft()
        old = item.worry_level
        new = eval(self.operation)

        if not part2:
            # Divide by 3
            new = new // 3
        else:
            new = new % TOTAL_MODULOS
        item.worry_level = new

        if new % self.test_divisible == 0:
            dest = self.if_true
        else:
            dest = self.if_false

        self.inspected += 1

        return item, dest

    def catch(self, item):
        self.items.append(item)

    def print(self):
        print('Monkey ', self.idx)
        print('- Has items: ', [str(item) for item in self.items])
        print('- Has operation: ', self.operation)
        print('- Has test: ', self.test_divisible)
        print('-- If true: ', self.if_true)
        print('-- If false: ', self.if_false)
        print()


class Item:
    def __init__(self, worry_level):
        self.worry_level = worry_level

    def __str__(self):
        return str(self.worry_level)


def read_and_process_monkeys():
    global TOTAL_MODULOS
    with open('day11.in') as f:
        lines = [line.strip() for line in f.readlines()]

    current_monkey = False
    MONKEYS = []
    for line in lines:
        if line == '':
            current_monkey = False
            continue
        if line.startswith('Monkey') and not current_monkey:
            idx = int(line.split(' ')[1][:-1])
            if idx not in MONKEYS:
                current_monkey = Monkey()
                MONKEYS.append(current_monkey)
            current_monkey.idx = MONKEYS.index(current_monkey)
        elif current_monkey:
            if 'Starting items' in line:
                ids = line.split(': ')[1]
                current_monkey.items = deque([Item(int(x)) for x in ids.split(',')])
            elif 'Operation' in line:
                current_monkey.operation = line.split(': new = ')[1]
            elif 'Test' in line:
                current_monkey.test_divisible = int(line.split(' by ')[1])
                TOTAL_MODULOS *= current_monkey.test_divisible
            elif 'If true' in line:
                current_monkey.if_true = int(line.split('to monkey ')[1])
            elif 'If false' in line:
                current_monkey.if_false = int(line.split('to monkey ')[1])

    return MONKEYS


def simulate(part2=False):
    MONKEYS = read_and_process_monkeys()
    ITERATIONS = 10000 if part2 else 20
    for i in range(ITERATIONS):
        for monkey in MONKEYS:
            # print('Monkey: ', monkey.idx)
            while monkey.items:
                item, dest = monkey.throw(part2=part2)
                MONKEYS[dest].catch(item)

    inspects = sorted([m.inspected for m in MONKEYS], reverse=True)
    print(inspects)
    return inspects[0] * inspects[1]


def part1():
    print('Part 1: ', simulate())

def part2():
    print('Part 2: ', simulate(part2=True))


part1()
part2()
