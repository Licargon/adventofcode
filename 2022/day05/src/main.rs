use std::fs;
use std::env;

use regex::Regex;

fn main() {
    let args: Vec<String> = env::args().collect();
    let f = fs::read_to_string(&args[1]).expect("could not open input file");
    let lines: Vec<String> = f.lines().map(|s| s.to_string()).collect();

    // Test
    // let mut crates: Vec<Vec<char>> = Vec::new();
    // crates.push(vec!['Z', 'N']);
    // crates.push(vec!['M', 'C', 'D']);
    // crates.push(vec!['P']);

    // Input
    let mut crates: Vec<Vec<char>> = Vec::new();
    crates.push(vec!['N', 'B', 'D', 'T', 'V', 'G', 'Z', 'J']);
    crates.push(vec!['S', 'R', 'M', 'D', 'W', 'P', 'F']);
    crates.push(vec!['V', 'C', 'R', 'S', 'Z']);
    crates.push(vec!['R', 'T', 'J', 'Z', 'P', 'H', 'G']);
    crates.push(vec!['T', 'C', 'J', 'N', 'D', 'Z', 'Q', 'F']);
    crates.push(vec!['N', 'V', 'P', 'W', 'G', 'S', 'F', 'M']);
    crates.push(vec!['G', 'C', 'V', 'B', 'P', 'Q']);
    crates.push(vec!['Z', 'B', 'P', 'N']);
    crates.push(vec!['W', 'P', 'J']);
    let re = Regex::new(r"move ([0-9]+) from ([0-9]+) to ([0-9]+)").unwrap();

    let mut part1_crates = crates.clone();
    for line in &lines {
        let caps = re.captures(&line).unwrap();
        let amount: u32 = caps.get(1).unwrap().as_str().parse().unwrap();
        let mut source: u32 = caps.get(2).unwrap().as_str().parse().unwrap();
        source -= 1;
        let mut dest: u32 = caps.get(3).unwrap().as_str().parse().unwrap();
        dest -= 1;
        for _ in 0..amount {
            let item = part1_crates[source as usize].pop().unwrap();
            part1_crates[dest as usize].push(item)
        }
    }

    let mut part1: String = String::new();
    for idx in 0..part1_crates.len() as usize{
        part1.push(part1_crates[idx].last().copied().unwrap());
    }
    println!("Part 1: {}", part1);

    let mut part2_crates = crates.clone();
    for line in &lines {
        let caps = re.captures(&line).unwrap();
        let amount: usize = caps.get(1).unwrap().as_str().parse().unwrap();
        let mut source: usize = caps.get(2).unwrap().as_str().parse().unwrap();
        source -= 1;
        let mut dest: usize = caps.get(3).unwrap().as_str().parse().unwrap();
        dest -= 1;

        let length: usize = part2_crates[source].len();
        let cutoff: usize = length - amount;
        // Take the last 'amount' off of the source container
        let to_add = part2_crates[source].as_slice()[cutoff..].to_vec();
        // Pop them all
        for _ in 0..amount {
            part2_crates[source].pop();
        }
        // And push them in the correct order onto the destination
        for c in to_add {
            part2_crates[dest].push(c);
        }
    }
    let mut part2: String = String::new();
    for idx in 0..part2_crates.len() as usize{
        part2.push(part2_crates[idx].last().copied().unwrap());
    }
    println!("Part 2: {}", part2);
}