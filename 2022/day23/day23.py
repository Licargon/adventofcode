
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

with open('day23.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = defaultdict(lambda: '.')
amt = 0
for ir, row in enumerate(lines):
    for ic, col in enumerate(row):
        if col == '#':
            G[ir, ic] = col
            amt += 1


def print_grid(G):
    min_y = min(y for y, x in G if G[y, x] == '#')
    max_y = max(y for y, x in G if G[y, x] == '#')
    min_x = min(x for y, x in G if G[y, x] == '#')
    max_x = max(x for y, x in G if G[y, x] == '#')

    for r in range(min_y, max_y + 1):
        row = []
        for c in range(min_x, max_x + 1):
            row.append(G[r, c])
        print(row)


MOVES = [
    ((-1, 0), (-1, -1), (-1, +1), (-1, 0)),
    ((+1, 0), (+1, -1), (+1, +1), (+1, 0)),
    ((0, -1), (-1, -1), (+1, -1), (0, -1)),
    ((0, +1), (-1, +1), (+1, +1), (0, +1)),
]

part1 = None
part2 = None
for i in range(2000):
    proposed_movements = defaultdict(list)
    original_G = set(list(k for k in G.keys() if G[k] == '#'))
    for ey, ex in list(G.keys()):
        if G[ey, ex] != '#':
            continue
        elf_in_neighbours = False
        for ny in [-1, 0, 1]:
            for nx in [-1, 0, 1]:
                if (ny, nx) == (0,0):
                    continue
                if G[ey+ny, ex+nx] == '#':
                    elf_in_neighbours = True
                    break
        # Only move if we have any neighbours
        if elf_in_neighbours:
            # N, NE, NW
            for check in range(4):
                ((dy1, dx1), (dy2, dx2), (dy3, dx3), (my, mx)) = MOVES[(i+check) % 4]
                if G[ey+dy1, ex+dx1] == '.' and G[ey+dy2, ex+dx2] == '.' and G[ey+dy3, ex+dx3] == '.':
                    proposed_movements[ey+my, ex+mx].append((ey, ex))
                    break

    for dest, source in proposed_movements.items():
        if len(source) == 1:
            if source[0] == (0, 1):
                assert True
            G[source[0]] = '.'
            G[dest] = '#'

    # Confirm we still have the equal amount of elves
    assert amt == len([(y,x) for (y,x) in G if G[y,x] == '#'])
    if i == 9:
        min_y = min(y for y, x in G if G[y, x] == '#')
        max_y = max(y for y, x in G if G[y, x] == '#')
        min_x = min(x for y, x in G if G[y, x] == '#')
        max_x = max(x for y, x in G if G[y, x] == '#')

        part1 = 0
        for r in range(min_y, max_y + 1):
            for c in range(min_x, max_x + 1):
                if G[r, c] == '.':
                    part1 += 1
    if original_G == set(list(k for k in G.keys() if G[k] == '#')) and not part2:
        part2 = i+1
        break

print('Part 1: ', part1)
# 4056 too low
print('Part 2: ', part2)

