
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import re
import sys

with open('day22.in') as f:
    grid, instructions = ''.join(f.readlines()).split('\n\n')

# print(grid)
# print(instructions)

G = defaultdict(lambda: '')
for ir, row in enumerate(grid.split('\n')):
    # row = row.strip()
    for ic, col in enumerate(row):
        if col != ' ':
            G[(ir, ic)] = col

def get_amt_and_turn(instruction):
    if 'R' in instruction or 'L' in instruction:
        turn = instruction[-1]
        amt = int(instruction.replace(turn, ''))
    else:
        turn = None
        amt = int(instruction)
    return amt, turn

# R, D, L, U to facilitate left/right turn
DIR = [(0, 1), (1, 0), (0, -1), (-1, 0)]


def part1(G, instructions):
    y, x = 0,0
    d = 0

    while G[(y, x)] != '.':
        x += 1

    for ins in re.findall(r'\d+[RL]?', instructions):
        amt, turn = get_amt_and_turn(ins)
        dy, dx = DIR[d]
        for _ in range(amt):
            y += dy
            x += dx
            # Move one step back, illegal move
            if G[(y, x)] == '#':
                y -= dy
                x -= dx
                # Stop moving, blocked
                break
            # We ran off the board
            if G[(y, x)] == '':
                dest_y = y - dy
                dest_x = x - dx
                # Walk ALL THE WAY across the board untill we reach the other side
                while G[(dest_y, dest_x)] != '':
                    dest_y -= dy
                    dest_x -= dx
                # We are now one step beyond the board, step back
                dest_y += dy
                dest_x += dx
                # Check if there is a rock on the other side -> We cannot pass
                # Set y and x back to one step before the final move as we can't 'cross the border'
                if G[(dest_y, dest_x)] == '#':
                    y -= dy
                    x -= dx
                else:
                    y = dest_y
                    x = dest_x

        # TURN
        if turn == 'L':
            d = (d-1) % 4
        elif turn == 'R':
            d = (d+1) % 4
    # Index is 1 based!
    part1 = 1000 * (y + 1) + 4 * (x + 1) + d
    return part1

print('Part 1: ', part1(G, instructions))


def part2(grid, instructions):
    y, x = 0,0

    while G[(y, x)] != '.':
        x += 1

    dy, dx = 0, 1
    for ins in re.findall(r'\d+[RL]?', instructions):
        amt, turn = get_amt_and_turn(ins)
        for _ in range(amt):
            orig_dy = dy
            orig_dx = dx
            ny = y + dy
            nx = x + dx
            # Thank you hyper-neutrino ._. ( https://www.youtube.com/watch?v=kktpopXsX2E )
            if ny < 0 and 50 <= nx < 100 and dy == -1:
                dy, dx = 0, 1
                ny, nx = nx + 100, 0
            elif nx < 0 and 150 <= ny < 200 and dx == -1:
                dy, dx = 1, 0
                ny, nx = 0, ny - 100
            elif ny < 0 and 100 <= nx < 150 and dy == -1:
                ny, nx = 199, nx - 100
            elif ny >= 200 and 0 <= nx < 50 and dy == 1:
                ny, nx = 0, nx + 100
            elif nx >= 150 and 0 <= ny < 50 and dx == 1:
                dx = -1
                ny, nx = 149 - ny, 99
            elif nx == 100 and 100 <= ny < 150 and dx == 1:
                dx = -1
                ny, nx = 149 - ny, 149
            elif ny == 50 and 100 <= nx < 150 and dy == 1:
                dy, dx = 0, -1
                ny, nx = nx - 50, 99
            elif nx == 100 and 50 <= ny < 100 and dx == 1:
                dy, dx = -1, 0
                ny, nx = 49, ny + 50
            elif ny == 150 and 50 <= nx < 100 and dy == 1:
                dy, dx = 0, -1
                ny, nx = nx + 100, 49
            elif nx == 50 and 150 <= ny < 200 and dx == 1:
                dy, dx = -1, 0
                ny, nx = 149, ny - 100
            elif ny == 99 and 0 <= nx < 50 and dy == -1:
                dy, dx = 0, 1
                ny, nx = nx + 50, 50
            elif nx == 49 and 50 <= ny < 100 and dx == -1:
                dy, dx = 1, 0
                ny, nx = 100, ny - 50
            elif nx == 49 and 0 <= ny < 50 and dx == -1:
                dx = 1
                ny, nx = 149 - ny, 0
            elif nx < 0 and 100 <= ny < 150 and dx == -1:
                dx = 1
                ny, nx = 149 - ny, 50
            # Dont invert the movement if we cant move to the new face
            if grid[ny,nx] == "#":
                dy = orig_dy
                dx = orig_dx
                break
            y = ny
            x = nx
        if turn == "R":
            dy, dx = dx, -dy
        elif turn == "L":
            dy, dx = -dx, dy

    d = DIR.index((dy, dx))
    # Index is 1 based!
    part2 = 1000 * (y + 1) + 4 * (x + 1) + d
    return part2

print('Part 2: ', part2(G, instructions))
