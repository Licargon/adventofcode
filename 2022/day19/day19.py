
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import functools
import re
import sys

with open('day19.in') as f:
    lines = [line.strip() for line in f.readlines()]

blueprints = []
for line in lines:
    s = line.split('. ')
    ints = list(map(int, [x for x in re.findall(r'\d+', line)[1:]]))
    blueprints.append(ints)


def dfs(ore_cost, clay_cost, obsidian_ore_cost, obsidian_clay_cost, geode_ore_cost, geode_obsidian_cost, time_left, bots, resources):
    max_answer = 0

    Q = deque([(time_left, *bots, *resources)])
    SEEN = set()
    while Q:
        time_left, b_ore, b_clay, b_obsidian, b_geode, r_ore, r_clay, r_obsidian, r_geode = Q.pop()
        if time_left == 0:
            max_answer = max(max_answer, r_geode)
            continue

        if (time_left, b_ore, b_clay, b_obsidian, b_geode, r_ore, r_clay, r_obsidian, r_geode) in SEEN:
            continue

        # Heuristic. Given that we were to create a geode bot EVERY timestep from now one, can we still
        # reach a higher maximum?
        if max_answer > 0:
            # ((time_left-1)*time_left/2) AKA '1 geode robot will be produced every minute from now on'
            # b_geode * time_left = geodes we will be producing anyways with our current bots
            # r_geode = The amount of geodes we have right now
            if ((time_left-1)*time_left/2) + b_geode * time_left + r_geode < max_answer:
                # This means that we will NOT be able to reach a higher answer in the best case scenario: No need to
                # explore further
                continue

        SEEN.add((time_left, b_ore, b_clay, b_obsidian, b_geode, r_ore, r_clay, r_obsidian, r_geode))

        # Just produce ore
        Q.append((time_left-1,
                  b_ore, b_clay, b_obsidian, b_geode,
                  r_ore + b_ore, r_clay + b_clay, r_obsidian + b_obsidian, r_geode + b_geode))

        # Figure out which bots we can create, add them to the queue
        max_ore_cost = max([ore_cost, clay_cost, obsidian_ore_cost, geode_ore_cost])
        # Run some checks before adding stuff to the Q
        #   Dont create any bots if we already have enough to satisfy the highest cost
        #   Dont create any bots for a certain rock if we already have enough supply to last untill the end of time_left
        #   Prioritise more valuable rocks

        if r_ore >= geode_ore_cost and r_obsidian >= geode_obsidian_cost:
            Q.append((time_left - 1,
                      b_ore, b_clay, b_obsidian, b_geode + 1,
                      r_ore + b_ore - geode_ore_cost, r_clay + b_clay, r_obsidian + b_obsidian - geode_obsidian_cost, r_geode + b_geode))
        if r_ore >= obsidian_ore_cost and r_clay >= obsidian_clay_cost and b_obsidian < geode_obsidian_cost and b_obsidian * (time_left-1) + r_obsidian < geode_obsidian_cost * (time_left-1):
            Q.append((time_left - 1,
                      b_ore, b_clay, b_obsidian + 1, b_geode,
                      r_ore + b_ore - obsidian_ore_cost, r_clay + b_clay - obsidian_clay_cost, r_obsidian + b_obsidian, r_geode + b_geode))
        if r_ore >= clay_cost and b_clay < obsidian_clay_cost and b_clay * (time_left-1) + r_clay < obsidian_clay_cost * (time_left-1):
            Q.append((time_left - 1,
                      b_ore, b_clay + 1, b_obsidian, b_geode,
                      r_ore + b_ore - clay_cost, r_clay + b_clay, r_obsidian + b_obsidian, r_geode + b_geode))
        if r_ore >= ore_cost and b_ore < max_ore_cost and b_ore * (time_left-1) + r_ore < (time_left-1) * max_ore_cost:
            Q.append((time_left-1,
                      b_ore+1, b_clay, b_obsidian, b_geode,
                      r_ore + b_ore - ore_cost, r_clay + b_clay, r_obsidian + b_obsidian, r_geode + b_geode))

    return max_answer

from time import time
start = time()
total = 0
for i, bp in enumerate(blueprints):
    total += (i+1) * dfs(*bp, 24, [1, 0, 0, 0], [0,0,0,0])
end = time()


print('Part 1: ', total)
print('Runtime: ', end-start)
# 1506 too low

start = time()
part2 = 1
for i, bp in enumerate(blueprints[0:3]):
    part2 *= dfs(*bp, 32, [1, 0, 0, 0], [0,0,0,0])

print('Part 2: ', part2)
print('Runtime: ', time()-start)
