# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import functools
import re
import sys

with open('day21.in') as f:
    lines = [line.strip().replace(':', '=') for line in f.readlines()]

def part1(lines):
    root_found = False
    local_vars = {}
    while not root_found:
        for line in lines:
            try:
                exec(line, globals(), local_vars)
            except Exception as e:
                continue
            else:
                if line.split('=')[0] == 'root':
                    return local_vars['root'], local_vars

print('Part 1: ', int(part1(lines)[0]))

def run_with_humn_value(lines, val):
    # Create a new list of lines with humn set to our val and run it through part 1
    new_lines = []
    for line in lines:
        L, R = line.split('=')
        if L.strip() == 'humn':
            line = 'humn={}'.format(val)
        new_lines.append(line)

    return part1(new_lines)

root, local_vals = part1(lines)
# Run it once
# root will be equal to value1 + value2, these are our 2 targets which we want to be equal
for line in lines:
    if line.startswith('root'):
        target1, target2 = list(map(str.strip, line.split('=')[1].split(' + ')))
# target1 depends on humn, target2 doesnt, so we can set the TARGET fixed
TARGET = local_vals[target2]

# Binary search. humn is linear so we can just see if we end up higher or lower than target
# and adjust accordingly
lo = 0
hi = int(1e15)
while lo < hi:
    mid = (lo+hi)//2
    res, local_vals = run_with_humn_value(lines, mid)

    if TARGET == local_vals[target1]:
        print('Part 2: ', mid)
        break
    if local_vals[target1] - TARGET > 0:
        lo = mid
    else:
        hi = mid
