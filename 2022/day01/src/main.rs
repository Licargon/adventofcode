use std::fs;
use std::env;
use std::cmp;

fn main() {
    let args: Vec<String> = env::args().collect();
    let f = fs::read_to_string(&args[1]).expect("could not open input file");
    let lines: Vec<String> = f.lines().map(|s| s.to_string()).collect();

    let mut global_max: i32 = 0;
    let mut local_max: i32 = 0;

    let mut elves_calories : Vec<i32> = Vec::new();

    for line in lines {
        if line == ""{
            global_max = cmp::max(global_max, local_max);
            elves_calories.push(local_max);
            local_max = 0;

        } else {
            local_max += line.parse::<i32>().expect("Number expected");

        }
    }

    println!("Part 1: {}", global_max);

    elves_calories.sort();

    let part2: i32 = elves_calories.iter().rev().take(3).sum();
    println!("Part 2: {}", part2);
    
}
