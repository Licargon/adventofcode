# -*- coding: utf-8 -*-

with open('day06.in') as f:
    text = f.readlines()[0]

def part1(text):
    windows = [text[i:i+4] for i in range(len(text)-4)]

    for idx, window in enumerate(windows):
        if len(window) == len(set(window)):
            return idx + 4

def part2(text):
    windows = [text[i:i+14] for i in range(len(text)-14)]
    for idx, window in enumerate(windows):
        if len(window) == len(set(window)):
            return idx + 14

print('Part 1: ', part1(text))
print('Part 2: ', part2(text))

# Testcases
# print(part1('bvwbjplbgvbhsrlpgdmjqwftvncz'), 5)
# print(part1('nppdvjthqldpwncqszvftbrmjlhg'), 6)
# print(part1('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg'), 10)
# print(part1('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw'), 11)
#
# print(part2('mjqjpqmgbljsphdztnvjfqwrcgsmlb'), 19)
# print(part2('bvwbjplbgvbhsrlpgdmjqwftvncz'), 23)
# print(part2('nppdvjthqldpwncqszvftbrmjlhg'), 23)
# print(part2('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg'), 29)
