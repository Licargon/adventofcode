
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys


class LinkedList(object):
    def __init__(self):
        self.index = {}
        self.head = None
        self.tail = None

    def __getitem__(self, item):
        return self.index[item]

    def add(self, value):
        new_node = LinkedNode(value)

        if not self.head:
            self.head = new_node
            self.tail = new_node
            self.index[value] = self.head
        else:
            new_node.prev = self.tail
            self.tail.next = new_node
            self.tail = new_node

        self.index[value] = new_node

    def add_list(self, after, values):
        current = self.index[after]
        tmp = current.next
        for v in values:
            new_node = LinkedNode(v)
            new_node.prev = current
            current.next = new_node
            if current == self.tail: # We are adding after the tail
                self.tail = new_node
            current = new_node
            self.index[v] = new_node
        current.next = tmp
        tmp.prev = current

    def remove(self, value):
        node = self.index[value]
        if node.prev == None:
            assert True
        node.prev.next = node.next
        node.next.prev = node.prev
        if self.head == node:
            self.head = self.head.next
        self.index.pop(value)

    def print(self):
        l = []
        cur = self.head
        while cur.value != self.tail.value:
            l.append(cur.value[1])
            cur = cur.next
        l.append(cur.value[1])
        print(l)

    def size(self):
        return len(self.index)

    # Day 20 specific code
    def make_circular(self):
        self.head.prev = self.tail
        self.tail.next = self.head

    def mix(self, values, part2=False):
        for idx, val in enumerate(values):
            current = self[(idx, val)]
            if val == 0:
                continue
            # FIRST remove before iterating
            self.remove((idx, val))
            if val > 0:
                shift = val
                if part2:
                    shift = val % self.size()
                for _ in range(shift):
                    current = current.next
            else:
                shift = abs(val)
                if part2:
                    shift %= self.size()
                for _ in range(shift + 1):
                    current = current.prev
            # Add to correct position
            self.add_list(current.value, [(idx, val)])


class LinkedNode(object):
    def __init__(self, value):
        self.prev = None
        self.next = None
        self.value = value


with open('day20.in') as f:
    lines = list(map(int, [line.strip() for line in f.readlines()]))

print('Total lines: ', len(lines))


LL = LinkedList()
zero_idx = None
for idx, val in enumerate(lines):
    LL.add((idx, val))
    if val == 0:
        zero_idx = idx

LL.make_circular()
LL.mix(lines)

part1 = 0
cur = LL[(zero_idx, 0)]
for i in range(1, 3001):
    cur = cur.next
    if i % 1000 == 0:
        part1 += cur.value[1]

print('Part 1: ', part1)

LL = LinkedList()
zero_idx = None
lines = [x * 811589153 for x in lines]
for idx, val in enumerate(lines):
    LL.add((idx, val))
    if val == 0:
        zero_idx = idx

LL.make_circular()
for i in range(10):
    LL.mix(lines, part2=True)

part2 = 0
cur = LL[(zero_idx, 0)]
for i in range(1, 3001):
    cur = cur.next
    if i % 1000 == 0:
        print('Grove coordinate: ', cur.value)
        part2 += cur.value[1]

print('Part 2: ', part2)