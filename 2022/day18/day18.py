# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

with open('day18.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = set()
for line in lines:
    coords = [int(s) for s in line.split(',')]
    G.add(tuple(coords))

def get_neighbours(cube):
    return [
        tuple([cube[0]+1, cube[1], cube[2]]),
        tuple([cube[0]-1, cube[1], cube[2]]),
        tuple([cube[0], cube[1]+1, cube[2]]),
        tuple([cube[0], cube[1]-1, cube[2]]),
        tuple([cube[0], cube[1], cube[2]+1]),
        tuple([cube[0], cube[1], cube[2]-1]),
    ]

def get_matches(cube, G):
    matches = 0
    for neighbour in get_neighbours(cube):
        if neighbour in G:
            matches += 1
    return matches

# 6 sides - 1 for every neighbouring cube
part1 = len(G) * 6
for cube in G:
    part1 -= get_matches(cube, G)
print('Part 1:', part1)


def get_inside_cubes(G):
    """
    Returns all the inside cubes that cant be reached from the outside.
    """
    min_x = min(c[0] for c in G)
    max_x = max(c[0] for c in G)
    min_y = min(c[1] for c in G)
    max_y = max(c[1] for c in G)
    min_z = min(c[2] for c in G)
    max_z = max(c[2] for c in G)
    all_cubes = set()
    # Minimal bounding box for our lava drop
    for x in range(min_x-1, max_x+2):
        for y in range(min_y-1, max_y+2):
            for z in range(min_z-1, max_z+2):
                all_cubes.add((x,y,z))

    # Remove all current cubes as we dont want to regard those
    empty_cubes = all_cubes - G

    # Start floodfill outside the box. Remove all reached cubes from empty_cubes. The end result will be the cubes that
    # couldnt be reached
    start = (min_x-2, min_y-2, min_z-2)
    Q = deque([start])
    SEEN = set()
    SEEN.add(start)
    assert start not in empty_cubes
    while Q:
        cur = Q.pop()
        if cur in empty_cubes:
            empty_cubes.remove(cur)

        for neighbour in get_neighbours(cur):
            # Only continue with neighbours if they're new and they fit inside our bounding box
            # If neighbour is in G we dont want to traverse through known lava
            if neighbour not in SEEN and neighbour not in G:
                if (min_x-2 <= neighbour[0] <= max_x+2 and min_y-2 <= neighbour[1] <= max_y+2 and min_z-2 <= neighbour[2] <= max_z+2):
                    Q.append(neighbour)
                    SEEN.add(neighbour)

    return empty_cubes

# Answer = surface area outside - surface area inside
inside_cubes = get_inside_cubes(G)
part2 = len(inside_cubes) * 6
for cube in inside_cubes:
    part2 -= get_matches(cube, inside_cubes)
print('Part 2:', part1-part2)
