
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys


class Grid:

    def __init__(self, inp_file):
        self._parse_grid(inp_file)

    def _parse_grid(self, inp_file):
        with open(inp_file) as f:
            lines = [line.strip() for line in f.readlines()]
        self.G = defaultdict(lambda: '.')
        for line in lines:
            points = line.split(' -> ')
            for p1, p2 in zip(points, points[1:]):
                x1, y1 = map(int, p1.split(','))
                x2, y2 = map(int, p2.split(','))

                for r in range(min(y1, y2), max(y1, y2) + 1):
                    for c in range(min(x1, x2), max(x1, x2) + 1):
                        self.G[(r, c)] = '#'

    def neighbours_orth(self, cur):
        neighbours = set()
        for dir_y in [-1, 1]:
            if (cur[0] + dir_y, cur[1]) in self.G:
                neighbours.add((cur[0] + dir_y, cur[1]))
        for dir_x in [-1, 1]:
            if (cur[0], cur[1]+dir_x) in self.G:
                neighbours.add((cur[0], cur[1]+dir_x))
        return neighbours

    def print(self):
        min_x = min([key[1] for key in self.G.keys()])
        max_x = max([key[1] for key in self.G.keys()])
        min_y = min([key[0] for key in self.G.keys()])
        max_y = max([key[0] for key in self.G.keys()])
        for i in range(min_y, max_y+1):
            row = str(i)
            for j in range(min_x, max_x+1):
                row += self.G[(i,j)]
            print(row)

        print()

    def get(self, y, x):
        return self.G[(y, x)]

    def is_empty(self, y, x):
        return self.G[(y, x)] == '.'

    def count_sand(self):
        return sum([1 for v in self.G.values() if v == 'o'])

    def drop_sand(self, part2=False):
        cur_pos = [0, 500]

        if not G.is_empty(*cur_pos):
            return False
        # Default starting position
        while True:
            test_pos = [cur_pos[0] + 1, cur_pos[1]]
            if not part2 and test_pos[0] > MAX_Y:  # Falling into the abyss -> Stop
                return False
            if G.is_empty(*test_pos):
                # Falling down
                cur_pos = test_pos
                continue
            else:
                # Try diagonal down to the left
                if G.is_empty(test_pos[0], test_pos[1] - 1):
                    cur_pos = [test_pos[0], test_pos[1] - 1]
                elif G.is_empty(test_pos[0], test_pos[1] + 1):
                    cur_pos = [test_pos[0], test_pos[1] + 1]
                else:
                    # We can not move. Settle here
                    self.G[tuple(cur_pos)] = 'o'
                    return True
G = Grid('day14.in')

# Lowest point in this example
MAX_Y = max([key[0] for key in G.G.keys()])
print('Max y: ', MAX_Y)

while G.drop_sand():
    # G.print()
    pass

print('Part 1: ', G.count_sand())

G = Grid('day14.in')

# Add an infinite floor at MAX_Y+2
MAX_Y = max([key[0] for key in G.G.keys()])
MIN_X = min([key[1] for key in G.G.keys()])
MAX_X = max([key[1] for key in G.G.keys()])

for x in range(MIN_X-250, MAX_X+250):
    G.G[(MAX_Y+2, x)] = '#'

cnt = 0
while G.drop_sand(part2=True):
    pass

G.print()
print('Part 2: ', G.count_sand())







