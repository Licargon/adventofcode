use std::fs;
use std::env;

fn is_full_overlap(left: &Vec<u32>, right: &Vec<u32>) -> bool {
    (left[0] >= right[0]) && (left[1] <= right[1])
}

fn is_partial_overlap(left: &Vec<u32>, right: &Vec<u32>) -> bool {
    (right[0] <= left[0] && left[0] <= right[1]) || (left[1] <= right[1] && left[0] >= right[1])
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let f = fs::read_to_string(&args[1]).expect("could not open input file");
    let lines: Vec<String> = f.lines().map(|s| s.to_string()).collect();

    let mut part1: u32 = 0;
    let mut part2: u32 = 0;

    for line in &lines {
        let i = line.find(',').expect("Invalid line!");
        let left: Vec<u32> = line[0..i].split('-').map(|s| s.parse().expect("Invalid number!")).collect();
        let right: Vec<u32> = line[i+1..].split('-').map(|s| s.parse().expect("Invalid number!")).collect();
        
        if is_full_overlap(&left, &right) || is_full_overlap(&right, &left){
            part1 += 1;
        }
        if is_partial_overlap(&left, &right) || is_partial_overlap(&right, &left){
            part2 += 1;
        }
    }
    println!("Part 1: {}", part1);
    println!("Part 2: {}", part2);
}
