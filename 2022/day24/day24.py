
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

with open('day24.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = {}
amt = 0
initial_blizzard_cells = set()
for ir, row in enumerate(lines):
    for ic, col in enumerate(row):
        G[ir, ic] = col
        if col in ['>', '<', '^', 'v']:
            initial_blizzard_cells.add((ir, ic))

R = len(lines)
C = len(lines[0])

start_y, start_x = 0, 0
while G[start_y, start_x] == '#':
    start_x += 1

end_y, end_x = max(y for y, x in G), 0
while G[end_y, end_x] == '#':
    end_x += 1


def print_grid(blizzard_cells):
    print(['#'] * C)
    for r in range(1, R-1):
        row = ['#']
        for c in range(1, C-1):
            if (r, c) in blizzard_cells:
                row.append('x')
            else:
                row.append('.')
        row.append('#')
        print(row)
    print(['#'] * C)
    print()

# time -> map of blizzards at time
BLIZZARD_MAPS = {}
for i in range(0, 5000):
    BLIZZARD_CELLS = set()
    # Check where each cell ends up after i timesteps
    # Need to add and subtract 1 to make it shift to the inner cells properly
    for (y, x), c in G.items():
        if c == '>':
            BLIZZARD_CELLS.add((y, 1+(x + i - 1) % (C-2)))
        elif c == '<':
            BLIZZARD_CELLS.add((y, 1 + (x - i - 1) % (C - 2)))
        elif c == '^':
            BLIZZARD_CELLS.add((1 + ((y - i - 1)) % (R-2), x))
        elif c == 'v':
            BLIZZARD_CELLS.add((1 + ((y + i - 1)) % (R-2), x))
    BLIZZARD_MAPS[i] = BLIZZARD_CELLS

# y, x, time, start_found, end_found
part1_found = False
Q = deque([(start_y, start_x, 0, False, False)])
SEEN = set()
while Q:
    y, x, t, start_found, end_found = Q.popleft()
    if (y, x, t, start_found, end_found) in SEEN:
        continue
    if (y, x) == (end_y, end_x) and not end_found:
        end_found = True
        if not part1_found:
            print('Part 1: ', t)
            part1_found = True

    if (y, x) == (start_y, start_x) and end_found:
        # We arrived back at the start
        start_found = True

    if (y, x) == (end_y, end_x) and start_found and end_found:
        print('Part 2: ', t)
        break

    SEEN.add((y, x, t, start_found, end_found))

    # Simulate waiting in place
    if (y, x) not in BLIZZARD_MAPS[t + 1]:
        Q.append((y, x, t + 1, start_found, end_found))
    # Check every direction, see if a blizzard will be there in the NEXT step
    for dy, dx in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
        ny, nx = y+dy, x+dx
        if 0 <= ny < R and 0 <= nx < C and G[ny, nx] != '#':
            if (ny, nx) not in BLIZZARD_MAPS[t+1]: # Safe step in this timeslice
                Q.append((ny, nx, t+1, start_found, end_found))

