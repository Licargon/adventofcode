
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import re
from pprint import pprint
import sys

with open('day16.in') as f:
    lines = [line.strip() for line in f.readlines()]

VALVES = {}

regex = re.compile(r'Valve (\w+) has flow rate=(\d+); tunnel[s]? lead[s]? to valve[s]? ([A-Z, ]*)')
for line in lines:
    groups = regex.match(line).groups()
    VALVES[groups[0]] = (int(groups[1]), set(groups[2].split(', ')))


# Shorted distance from any valve to any valve
VALVE_DISTANCES = {}
for v, data in VALVES.items():
    # BFS from every valve to every valve to find the shortest distance
    for v2, data2 in VALVES.items():
        Q = deque([(v, 0)])
        SEEN = set()
        while Q:
            cur, d = Q.popleft()
            if cur == v2 and (v, v2) not in VALVE_DISTANCES:
                VALVE_DISTANCES[(v, v2)] = d
                break
            SEEN.add(cur)
            for connected in VALVES[cur][1]:
                if connected not in SEEN:
                    Q.append((connected, d+1))

def dfs(V, VD, start_time):
    Q = deque([('AA', start_time, set(), 0, [])])
    max_score = 0
    all_paths = {}
    while Q:
        cur, time_left, opened, score, ordered_opens = Q.pop()
        max_score = max(score, max_score)
        # Keep track of the best score for every path for part 2
        if tuple(ordered_opens) not in all_paths or all_paths[tuple(ordered_opens)] < score:
            all_paths[tuple(ordered_opens)] = score

        for valve in V:
            # Dont try to reopen valves, dont go for valves without flow_rate
            if valve in opened or V[valve][0] == 0:
                continue
            # Only go to a valve if we can actually reach it, dont 'move' to ourselves
            d = VD[(cur, valve)]
            if d < time_left-1 and valve != cur:
                new_score = V[valve][0] * (time_left-d-1) + score
                # Move and open takes time d+1
                Q.append((valve, time_left-d-1, opened.copy() | {valve}, new_score,
                          ordered_opens[:] + [(valve, time_left-d-1)]))

    return max_score, all_paths


part1, _ = dfs(VALVES, VALVE_DISTANCES, 30)
print('Part 1: ', part1)

"""
all_paths will contain all final paths with their score. 

We search for all combinations of 2 paths who dont share any valves (meaning player and elephant did totally different things),
we calculate the score for the combined path and find the maximum
"""
score, all_paths = dfs(VALVES, VALVE_DISTANCES, 26)
l = [(v, k) for k,v in all_paths.items()]
# Guess: Best combination of paths will be in the top 500 paths. Timesave.
sorted_l = sorted(l, key=lambda t: t[0], reverse=True)[0:500]
cur_max = 0
for idx, l1 in enumerate(sorted_l):
    for l2 in sorted_l[idx:]:
        if l1[0] + l2[0] < part1: # No need to bother:
            continue
        s1 = {t[0] for t in l1[1]}
        s2 = {t[0] for t in l2[1]}
        if s1 & s2: # There are overlaps, not possible, skip.
            continue
        else:
            # Combine the score
            score = 0
            for t in l1[1]:
                score += VALVES[t[0]][0] * t[1]
            for t in l2[1]:
                score += VALVES[t[0]][0] * t[1]
            cur_max = max(cur_max, score)

print('Part 2: ', cur_max)
