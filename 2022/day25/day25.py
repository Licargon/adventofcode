# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

with open('day25.in') as f:
    lines = [line.strip() for line in f.readlines()]

CONV = {'2': 2, '1': 1, '0': 0, '-': -1, '=': -2}
CONV_INV = {2: '2', 1: '1', 0: '0', -1: '-', -2: '='}

total = 0
for line in lines:
    cur = 0
    d = 1
    for c in reversed(line):
        cur += CONV[c] * d
        d *= 5

    total += cur

print('Total: ', total)

snafu = []
while total > 0:
    snafu.append(total % 5)
    total //= 5
snafu = list(reversed(snafu))
# Sample: [1, 2, 4, 0, 3, 0]
# Correct: [2, =, -, 1, =, 0]

# Convert from base10 to snafu
while any(x >= 3 for x in snafu):
    for i, n in enumerate(snafu):
        if n >= 3:
            snafu[i-1] += 1
            snafu[i] -= 5

part1 = []
for x in snafu:
    part1.append(CONV_INV[x])
print('Part 1: ', ''.join(part1))
