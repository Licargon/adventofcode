# -*- coding: utf-8 -*-

from collections import defaultdict

with open('day07.in') as f:
    lines = [line.strip() for line in f.readlines()]


def solve(lines):
    fs = defaultdict(int)
    cur_path = ['/']
    for line in lines:
        parts = line.split(' ')
        if line == '$ cd /':
            cur_path = ['/']
        elif parts[1] == 'ls':
            continue
        elif parts[1] == 'cd':
            dest = parts[2]
            if dest == '..':
                cur_path.pop()
            else:
                cur_path.append(dest)
        elif parts[0] == 'dir':
            continue
        else:
            filesize, filename = int(parts[0]), parts[1]
            new_path = cur_path[:] + [filename]
            # Recursively add the filesize to all folders above
            for i in range(len(new_path)):
                fs[tuple(new_path[:i])] += filesize

    part1 = 0
    part2 = 1e12

    # Max space to be used is 40000000
    space_to_free = fs[tuple(['/'])] - 40000000

    for key, value in fs.items():
        if value <= 100000:
            part1 += value
        if value >= space_to_free:
            part2 = min(part2, value)
    print('Part 1: ', part1)
    print('Part 2: ', part2)

solve(lines)
