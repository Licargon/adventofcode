
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import math
import re
import sys

def dist(first, second):
    # Manhattan
    return abs(first[0]-second[0]) + abs(first[1]-second[1])

with open('day15.in') as f:
    lines = [line.strip() for line in f.readlines()]

SENSORS = set()
BEACONS = set()
regex = re.compile(r'Sensor at x=(\d+), y=(\d+): closest beacon is at x=([-]*\d+), y=([-]*\d+)')
for line in lines:
    match = list(map(int, list(regex.match(line).groups())))
    sensor = (match[0], match[1])
    beacon = (match[2], match[3])
    d = dist(sensor, beacon)
    SENSORS.add((sensor, d))
    BEACONS.add(beacon)

# print(SENSORS)
# print(BEACONS)
# print(len(BEACONS))

def find_sensor_for_point(x, y):
    # Finds the sensor that can see this point, else None
    for sensor, min_dist in SENSORS:
        d = dist(sensor, (x, y))
        if d <= min_dist:
            return sensor, min_dist

    return None, None

def part1():
    y = 2000000
    ### Naive and slow.
    # for x in range(int(-1e7), int(1e7)):
    #     # Check if x,Y is an invalid beacon position
    #     # position is invalid if it isnt a beacon already, and its closer to the assigned beacon for any sensor
    #     if (x, y) in BEACONS:
    #         continue
    #     valid = True
    #     for S, min_dist in SENSORS:
    #         d1 = dist(S, (x, y))
    #         # Its supposedly closer to the closest beacon: Can't be a beacon
    #         if d1 <= min_dist:
    #             valid = False
    #             break
    #
    #     if not valid:
    #         part1 += 1
    #
    # print('Part 1: ', part1)

    invalid_points = set()
    for sensor, min_dist in SENSORS:
        # Check if the sensor and y overlap
        if sensor[1] - min_dist <= y <= sensor[1] + min_dist:
            # Figure out how much of y crosses with the diamond.
            y_diff = abs(y - sensor[1])
            # Because manhattan distance we can still move up to min_dist-y_diff in both directions
            remaining = min_dist - y_diff
            x = sensor[0]
            for i in range(-remaining, remaining+1):
                invalid_points.add((x+i, y))
            invalid_points.add((x, y))
    # Remove any existing beacons
    for beacon in BEACONS:
        if beacon in invalid_points:
            invalid_points.remove(beacon)
    print('Part 1: ', len(invalid_points))

def part2():
    MIN = 0
    MAX = 4_000_000
    for y in range(MIN, MAX+1):
        x = 0
        while x <= MAX:
            s, min_dist = find_sensor_for_point(x, y)
            if not s:
                print('Point: ', x, y)
                print('Part 2: ', 4_000_000*x + y)
                return
            else:
                # Find where x ends up AFTER the sensor, AKA manhattan distance equal to the beacon PLUS ONE
                x = s[0]
                y_diff = abs(y-s[1])
                x += (min_dist - y_diff) + 1 # +1 to break _JUST_ free of the sensor
                # X is now just past the edge of the diamond and we can see which sensor picks it up now

part1()
part2()
