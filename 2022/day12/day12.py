# -*- coding: utf-8 -*-

from collections import deque


class Grid:

    def __init__(self, inp_file):
        self._parse_grid(inp_file)

    def _parse_grid(self, inp_file):
        with open(inp_file) as f:
            lines = [line.strip() for line in f.readlines()]
        self.G = {}
        for ir, line in enumerate(lines):
            for ic, col in enumerate(line):
                self.G[(ir, ic)] = col

    def neighbours_orth(self, cur):
        neighbours = set()
        for dir_y in [-1, 1]:
            if (cur[0] + dir_y, cur[1]) in self.G:
                neighbours.add((cur[0] + dir_y, cur[1]))
        for dir_x in [-1, 1]:
            if (cur[0], cur[1]+dir_x) in self.G:
                neighbours.add((cur[0], cur[1]+dir_x))
        return neighbours

    def print(self):
        min_x = min([key[1] for key in self.G.keys()])
        max_x = max([key[1] for key in self.G.keys()])
        min_y = min([key[0] for key in self.G.keys()])
        max_y = max([key[0] for key in self.G.keys()])
        for i in range(min_y, max_y+1):
            row = ''
            for j in range(min_x, max_x+1):
                row += self.G[(i,j)]
            print(row)

        print('Width: ', max_x-min_x)
        print('Height: ', max_y-min_y)

    def solve(self, part):
        Q = deque([])
        for k, v in self.G.items():
            if part == 1 and v == 'S':
                Q.append((k, []))
                break
            elif part == 2 and v == 'a':
                Q.append((k, []))
        cur_min_path_length = 1e9
        SEEN = set()
        cnt = 0
        while Q:
            cur, path = Q.popleft()
            if cur in SEEN:
                continue
            SEEN.add(cur)
            cnt += 1
            neighbours = self.neighbours_orth(cur)
            for neighbour in neighbours:
                dist_max_one = (ord(self.G[neighbour]) - ord(self.G[cur]) <= 1)
                if self.G[neighbour] == 'z' and dist_max_one: # done
                    cur_min_path_length = min(cur_min_path_length, len(path))
                elif self.G[cur] == 'S' or dist_max_one:
                    new_path = path[:] + [self.G[neighbour]]
                    new_seen = SEEN.copy()
                    new_seen.add(neighbour)
                    Q.append((neighbour, new_path))
        return cur_min_path_length


grid = Grid('day12.in')
# grid.print()
print('Part 1: ', grid.solve(1))
print('Part 2: ', grid.solve(2))
