use std::fs;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let f = fs::read_to_string(&args[1]).expect("could not open input file");
    let lines: Vec<String> = f.lines().map(|s| s.to_string()).collect();

    let mut part1: u32 = 0;

    for line in &lines {
        
        let mut chars = line.chars();
        let mut opp = chars.nth(0).unwrap();
        let mut us = chars.nth(1).unwrap();
        /*
         * A/X = Rock 
         * B/Y = Papers 
         * C/Z = Scissors
         * 
         * Rock = 1
         * Papers = 2
         * Scissors = 3
         * Win = +6, Draw = +3, Lose = +0
         */
        part1 += match opp {
            'A' => match us {
                'X' => 4,
                'Y' => 8,
                'Z' => 3,
                _ => {panic!("Unexpected value");}
            },
            'B' => match us {
                'X' => 1,
                'Y' => 5,
                'Z' => 9,
                _ => {panic!("Unexpected value");}

            },
            'C' => match us {
                'X' => 7,
                'Y' => 2,
                'Z' => 6,
                _ => {panic!("Unexpected value");}

            },
            _ => {panic!();}
        };
    }
    println!("Part 1: {}", part1);

    let mut part2: u32 = 0;

    for line in &lines {
        
        let mut chars = line.chars();
        let mut opp = chars.nth(0).unwrap();
        let mut us = chars.nth(1).unwrap();
        /*
         * A/X = Rock 
         * B/Y = Papers 
         * C/Z = Scissors
         * 
         * Rock = 1
         * Papers = 2
         * Scissors = 3
         * Win = +6, Draw = +3, Lose = +0
         */
        part2 += match opp {
            'A' => match us {
                'X' => 3,
                'Y' => 4,
                'Z' => 8,
                _ => {panic!("Unexpected value");}
            },
            'B' => match us {
                'X' => 1,
                'Y' => 5,
                'Z' => 9,
                _ => {panic!("Unexpected value");}

            },
            'C' => match us {
                'X' => 2,
                'Y' => 6,
                'Z' => 7,
                _ => {panic!("Unexpected value");}

            },
            _ => {panic!();}
        };
    }
    println!("Part 2: {}", part2);
}
