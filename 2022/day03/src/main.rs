use std::fs;
use std::env;
use std::collections::HashSet;

fn get_element_prio(elem: char) -> u32{
    let mut elem_ord: u32 = elem.into();
    if elem.is_lowercase() {
        elem_ord -= 96;
    } else if elem.is_uppercase() {
        elem_ord -= 38; // -64 + 26
    }
    return elem_ord;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let f = fs::read_to_string(&args[1]).expect("Could not open input file");
    let lines: Vec<String> = f.lines().map(|s| s.to_string()).collect();

    let mut part1: u32 = 0;
    for line in &lines{
        let (first, last) = line.split_at(line.len()/2);
        let mut left: HashSet<char> = HashSet::new();
        let mut right: HashSet<char> = HashSet::new();
        for c in first.chars(){
            left.insert(c);
        }
        for c in last.chars(){
            right.insert(c);
        }
        let intersect: HashSet<char> = left.intersection(&right).copied().collect();
        let intersect_vector: Vec<char> = intersect.into_iter().collect();
        if intersect_vector.len() == 1 {
            let elem: char = intersect_vector[0];
            let prio: u32 = get_element_prio(elem);
            
            // println!("Elem {} has prio {}", elem, prio);
            part1 += prio;
        }
        
    }
    println!("Part 1: {}", part1);

    /*
    Get the common element between all groups of 3 elves and find the prio of that element
     */
    let mut part2: u32 = 0;
    for chunk in lines.chunks(3) {
        let mut first: HashSet<char> = HashSet::new();
        for c in chunk[0].chars() {
            first.insert(c);
        }

        let mut second: HashSet<char> = HashSet::new();
        for c in chunk[1].chars() {
            second.insert(c);
        }

        let mut third: HashSet<char> = HashSet::new();
        for c in chunk[2].chars() {
            third.insert(c);
        }
        let intersect: HashSet<char> = first.intersection(&second).copied().collect();
        let intersect: HashSet<char> = intersect.intersection(&third).copied().collect();
        let intersect_vector: Vec<char> = intersect.into_iter().collect();

        // println!("Elem {} has prio {}", intersect_vector[0], get_element_prio(intersect_vector[0]));
        
        part2 += get_element_prio(intersect_vector[0]);

    }
    println!("Part 2: {}", part2);
}
