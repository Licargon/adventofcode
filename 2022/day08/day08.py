# -*- coding: utf-8 -*-

from collections import defaultdict
from pprint import pprint

with open('day08.in') as f:
    lines = [line.strip() for line in f.readlines()]

GRID = {}
for ri, row in enumerate(lines):
    for ci, col in enumerate(row):
        GRID[(ri, ci)] = int(col)

HEIGHT = len(lines)
WIDTH = len(lines[0])

# Left, right, down, up
DIR = [(0, -1), (0, 1), (1, 0), (-1, 0)]


def part1():
    # Check which trees are visible by checking if there is something higher in any director
    # Outside trees are always visible, only check the internal ones
    part1 = 2 * HEIGHT + 2 * (WIDTH-2) # Avoid double counting corners
    for row in range(1, HEIGHT-1):
        for col in range(1, WIDTH-1):
            # Avoid double counting trees
            tree_added = False
            # print(row, col, GRID[(row, col)])
            for (dy, dx) in DIR:
                visible = True
                cur_row = row
                cur_col = col
                while True:
                    cur_col += dx
                    cur_row += dy
                    if not (0 <= cur_row < HEIGHT and 0 <= cur_col < WIDTH):
                        break
                    if GRID[(cur_row, cur_col)] >= GRID[(row, col)]:
                        visible = False

                if visible and not tree_added:
                    if not tree_added:
                        part1 += 1
                        tree_added = True
                        break

    return part1


def part2():
    part2 = 1
    # Consider all trees in part 2
    for row in range(HEIGHT):
        for col in range(WIDTH):
            scenic_score = 1
            for (dy, dx) in DIR:
                cur_row = row
                cur_col = col
                dist = 1
                while True:
                    cur_col += dx
                    cur_row += dy
                    if not (0 <= cur_row < HEIGHT and 0 <= cur_col < WIDTH):
                        # Make sure we dont overcount if we leave the edge
                        dist -= 1
                        break
                    if GRID[(cur_row, cur_col)] >= GRID[(row, col)]:
                        break
                    else:
                        dist += 1
                scenic_score *= dist
            part2 = max(part2, scenic_score)
    return part2

print('Part 1: ', part1())
print('Part 2: ', part2())