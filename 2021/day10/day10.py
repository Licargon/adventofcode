# -*- coding: utf-8 -*-

from collections import deque

with open('day10.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

part1 = 0
part2 = []
incomplete_lines = []
for line in lines:
    stack = []
    corrupt = False
    for char in line:
        if char in '{(<[':
            stack.append(char)
        if char in '})>]':
            peek = stack.pop()
            if char == ')' and peek != '(':
                part1 += 3
                corrupt = True
                break
            elif char == ']' and peek != '[':
                part1 += 57
                corrupt = True
                break
            elif char == '}' and peek != '{':
                part1 += 1197
                corrupt = True
                break
            elif char == '>' and peek != '<':
                part1 += 25137
                corrupt = True
                break
    if not corrupt:
        incomplete_lines.append(line)
        # Complete the string based on whats left on the stack.
        if len(stack):
            completion_score = 0
            scores = {
                '(': 1,
                '[': 2,
                '{': 3,
                '<': 4
            }
            for char in reversed(stack):
                completion_score *= 5
                completion_score += scores[char]
            part2.append(completion_score)


print('Part 1: ', part1)
# Part 2 answer is the middle score
part2.sort()
print('Part 2: ', part2[len(part2)//2])




