# -*- coding: utf-8 -*-

from collections import Counter
from copy import deepcopy

with open('day6.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))[0]

fishes_input = list(map(int, lines.split(',')))
fishes_input = Counter(fishes_input) #
# Fill up the counter with all values from 0 to 8 to prevent 'dict size changed during iteration'
# errors
for i in range(9):
    if i not in fishes_input:
        fishes_input[i] = 0

def tick(fishes):
    # 0 days left: Spawn X fish at day 6 and X at day 8
    # Subtract where needed
    # Loop in reversed order (8 days left -> 0 days left)
    for days_left, amount in sorted(list(fishes.items()), key=lambda t: -t[0]):
        if days_left > 0:
            fishes[days_left] -= amount
            fishes[days_left-1] += amount
        else:
            fishes[8] += amount
            fishes[6] += amount
            fishes[0] -= amount
    return fishes

fishes = deepcopy(fishes_input)
for i in range(80):
    fishes = tick(fishes)

print('Part 1: ', sum(fishes.values()))

fishes = deepcopy(fishes_input)
for i in range(256):
    fishes = tick(fishes)

print('Part 2: ', sum(fishes.values()))
