# -*- coding: utf-8 -*-
from copy import deepcopy

import heapq
import math

with open('day15.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

grid = {}
for rr, row in enumerate(lines):
    for cc, col in enumerate(row):
        grid[(rr, cc)] = [int(col), math.inf]


def find_lowest_risk_path(grid):
    # Mark the starting point as cost 0
    grid[(0, 0)][1] = 0

    Q = []
    SEEN = {(0, 0)}
    # Tuple = (current_cost, current_coord)
    # Use a heap as a PriorityQueue
    heapq.heappush(Q, (0, (0,0)))
    while Q:
        state = heapq.heappop(Q)
        SEEN.add(state[0])

        row, col = state[1]
        cost = state[0]
        for dy, dx in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            new_coords = (row+dy, col+dx)
            if new_coords in grid and new_coords not in SEEN:
                risk_level, current_lowest_cost = grid[new_coords]
                # Compare current cost + cost of new cell compared to current lowest registered
                # cost to this coordinate thusfar, replace if needed
                if cost + risk_level < current_lowest_cost:
                    current_lowest_cost = cost + risk_level

                # Record it for the future
                grid[new_coords][1] = current_lowest_cost
                heapq.heappush(Q, (current_lowest_cost, new_coords))
                SEEN.add(new_coords)

    # Print the cost of the bottom right cell
    max_y = max([key[0] for key in grid.keys()])
    max_x = max([key[1] for key in grid.keys()])
    return grid[(max_y, max_x)][1]


print('Part 1: ', find_lowest_risk_path(deepcopy(grid)))


grid_dimension = len(lines)
new_grid = {}
# Construct grid for part 2
for row in range(5):
    for col in range(5):
        for key, value in grid.items():
            new_key = (key[0] + row * grid_dimension, key[1] + col * grid_dimension)
            # New value is basically old value + Manhattan distance to this point
            new_value = value[0] + row + col
            # Can use MOD because we need to loop back to 1, not 0.
            if new_value > 9:
                new_value -= 9
            new_grid[new_key] = [new_value, math.inf]

print('Part 2: ', find_lowest_risk_path(deepcopy(new_grid)))
