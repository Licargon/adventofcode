# -*- coding: utf-8 -*-
from copy import deepcopy

with open('day25.in') as f:
    lines = f.readlines()
grid = []
for line in lines:
    tmp_row = []
    for char in line.strip('\n'):
        tmp_row.append(char)

    grid.append(tmp_row)


def step(grid, to_move):
    offsets = {
        '>': (0, 1),
        'v': (1, 0)
    }
    moved = False

    new_grid = deepcopy(grid)
    no_rows = len(grid)
    no_cols = len(grid[0])
    for ridx, row in enumerate(grid):
        for cidx, col in enumerate(row):
            if col != to_move:
                continue
            else:
                dy, dx = offsets[col]
                check_row, check_col = (ridx+dy) % no_rows, (cidx+dx) % no_cols
                if grid[check_row][check_col] == '.':
                    moved = True
                    new_grid[ridx][cidx] = '.'
                    new_grid[check_row][check_col] = col

    return moved, new_grid


def print_grid(grid):
    for row in grid:
        print(row)
    print()


def part1(grid):
    have_moved1 = have_moved2 = True
    cnt = 0
    while have_moved1 or have_moved2:
        # print_grid(grid)
        have_moved1, grid = step(grid, '>')
        have_moved2, grid = step(grid, 'v')
        cnt += 1

    print('Part 1: ', cnt)


if __name__ == "__main__":
    part1(grid)
