# -*- coding: utf-8 -*-

from collections import defaultdict

with open('day5.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

coordinate_pairs = []
for line in lines:
    pair1, pair2 = line.split(' -> ')
    coordinate_pairs.append((list(map(int, pair1.split(','))), list(map(int,pair2.split(',')))))

def get_points_between_straight(x1, y1, x2, y2):
    if x1 == x2:
        return [(x1, inter_y) for inter_y in range(min(y1, y2), max(y1, y2)+1)]
    elif y1 == y2:
        return [(inter_x, y1) for inter_x in range(min(x1, x2), max(x1, x2)+1)]
    return []

def get_points_between_diagonal(x1, y1, x2, y2):
    if x1 == x2 or y1 == y2:
        # Covered in the function above.
        return []
    slope = (y2-y1)/abs(x2-x1)
    points = []
    step = 1 if x1 < x2 else -1
    for idx, x in enumerate(range(x1, x2+step, step)):
        points.append((x, int(y1+slope*idx)))
    return points

points_reached = defaultdict(int)
points_reached_inc_diag = defaultdict(int)
for coordinate_pair in coordinate_pairs:
    for point in get_points_between_straight(*coordinate_pair[0], *coordinate_pair[1]):
        points_reached[point] += 1
        points_reached_inc_diag[point] += 1
    for point in get_points_between_diagonal(*coordinate_pair[0], *coordinate_pair[1]):
        points_reached_inc_diag[point] += 1

part1 = 0
for k, v in points_reached.items():
    if v > 1:
        part1 += 1

print('Part 1: ', part1)

part2 = 0
for k, v in points_reached_inc_diag.items():
    if v > 1:
        part2 += 1
print('Part 2: ', part2)