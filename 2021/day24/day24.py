# -*- coding: utf-8 -*-
from collections import defaultdict, deque, Counter
from itertools import permutations, combinations_with_replacement
from pprint import pprint
from copy import deepcopy
import functools
import math
import re

with open('day24.in') as f:
    lines = [line.strip() for line in f.readlines()]

def get_arguments(args):
    ret = [args[0]]
    try:
        arg2 = int(args[1])
        ret.append(arg2)
    except ValueError: # not an int
        ret.append(args[1])
    except IndexError:
        print('Huh?')
        raise
    return tuple(ret)

instructions = []
for line in lines:
    if line.startswith('inp'):
        instructions.append(('inp', line.split(' ')[1]))
    else:
        s = line.split(' ')
        instructions.append((line[0:3], get_arguments(line[3:].strip().split(' '))))

print(instructions)


class ALU(object):
    def __init__(self, instructions, input_Q=None):
        self.REGISTERS = {
            'w': 0,
            'x': 0,
            'y': 0,
            'z': 0,
        }
        self.instructions = instructions
        self.Q = input_Q
        self.IP = 0

    def get_input(self):
        if self.Q:
            val = self.Q.popleft()
        else:
            val = input('>> ')
        return val

    def process(self):
        last_z = -1
        while True and self.IP < len(self.instructions):
            op, args = self.instructions[self.IP]
            if op == 'inp':
                if last_z > self.REGISTERS['z']:
                    print('Decreased!')
                last_z = self.REGISTERS['z']
                val = self.get_input()
                self.REGISTERS[args[0]] = val
            elif op == 'add':
                arg1 = self.REGISTERS[args[0]]
                if isinstance(args[1], int):
                    arg2 = args[1]
                else:
                    arg2 = self.REGISTERS[args[1]]
                self.REGISTERS[args[0]] = arg1 + arg2
            elif op == 'mod':
                arg1 = self.REGISTERS[args[0]]
                if isinstance(args[1], int):
                    arg2 = args[1]
                else:
                    arg2 = self.REGISTERS[args[1]]
                self.REGISTERS[args[0]] = arg1 % arg2
            elif op == 'mul':
                arg1 = self.REGISTERS[args[0]]
                if isinstance(args[1], int):
                    arg2 = args[1]
                else:
                    arg2 = self.REGISTERS[args[1]]
                self.REGISTERS[args[0]] = arg1 * arg2
            elif op == 'div':
                arg1 = self.REGISTERS[args[0]]
                if isinstance(args[1], int):
                    arg2 = args[1]
                else:
                    arg2 = self.REGISTERS[args[1]]
                self.REGISTERS[args[0]] = arg1 // arg2
            elif op == 'eql':
                arg1 = self.REGISTERS[args[0]]
                if isinstance(args[1], int):
                    arg2 = args[1]
                else:
                    arg2 = self.REGISTERS[args[1]]
                self.REGISTERS[args[0]] = int(arg1 == arg2)

            self.IP += 1


# Not needed anymore due to manual analysis.
def solve(instructions):
    cnt = 0
    numbers = [int(x) for x in list('987654321')]
    combinations = combinations_with_replacement(numbers, 14)
    cur_highest_model_number = -1e20
    for c in combinations:
        cnt += 1
        if cnt % 100000 == 0:
            print(f'Step {cnt}')
        Q = deque([x for x in c])
        alu = ALU(instructions, Q)
        alu.process()
        if alu.REGISTERS['z'] == 0:
            print('Success')
            model_number = int(''.join([str(x) for x in list(c)]))
            cur_highest_model_number = max(cur_highest_model_number, model_number)
            return model_number
    print('List exhausted after cnt? ', cnt)
    return cur_highest_model_number


print('Part 1: ', solve(instructions[:]))

