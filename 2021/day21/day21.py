# -*- coding: utf-8 -*-
from collections import defaultdict
from copy import deepcopy
import functools
import re

with open('day21.in') as f:
    lines = f.readlines()

p1_line = lines[0]
p2_line = lines[1]

regex = re.compile(r'Player [12]+ starting position: ([0-9]+)')
match = regex.match(p1_line)
p1_pos = match.groups()[0]
match = regex.match(p2_line)
p2_pos = match.groups()[0]

positions = [int(p1_pos), int(p2_pos)]
scores = [0, 0]


class Dice(object):
    def __init__(self, max_roll):
        self.rolls = 0
        self.current = 0
        self.max = max_roll

    def roll(self):
        self.current += 1
        if self.current > self.max:
            self.current -= self.max
        self.rolls += 1
        return self.current


def part1():
    d = Dice(100)
    turn = 0
    done = False
    while not done:
        score = 0
        for _ in range(3):
            score += d.roll()
        new_pos = positions[turn] + score
        while new_pos > 10:
            new_pos -= 10
        positions[turn] = new_pos
        scores[turn] += new_pos
        if scores[turn] >= 1000:
            done = True
            break
        turn = 1-turn
    return min(scores) * d.rolls

print('Part 1: ', part1())

# Distribution for how many possibilities there are per roll of 3 dice
distrib = defaultdict(lambda: 0)
for d1 in range(1, 4):
    for d2 in range(1, 4):
        for d3 in range(1, 4):
            distrib[d1+d2+d3] += 1


@functools.lru_cache(maxsize=None)
def recursive_play(turn, pos1, score1, pos2, score2):
    if score1 >= 21:
        return (1, 0)
    elif score2 >= 21:
        return (0, 1)

    # Roll the THREE dice
    game_result = (0, 0)
    # 3 dice [1,2,3] -> results lie in [1, 10[
    for roll in range(3, 10):
        if turn == 0:
            new_pos = pos1 + roll
            while new_pos > 10:
                new_pos -= 10
            win = recursive_play(1, new_pos, score1+new_pos, pos2, score2)
        else:
            new_pos = pos2 + roll
            while new_pos > 10:
                new_pos -= 10
            win = recursive_play(0, pos1, score1, new_pos, score2+new_pos)

        # Multiply by the distribution to see how many times they actually won with that roll
        game_result = (game_result[0] + distrib[roll] * win[0], game_result[1] + distrib[roll] * win[1])

    return game_result

from time import time
start = time()
result = recursive_play(0, int(p1_pos), 0, int(p2_pos), 0)
print('Part 2: ', max(result))
print('Done in {} seconds'.format(time()-start))
