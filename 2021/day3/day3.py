# -*- coding: utf-8 -*-

with open('day3.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

def part1(lines):
    line_length = len(lines[0])
    gamma = ''
    epsilon = ''

    for i in range(line_length):
        zeroes = ones = 0
        for line in lines:
            if line[i] == '0':
                zeroes += 1
            else:
                ones += 1

        if zeroes > ones:
            gamma += '0'
            epsilon += '1'
        else:
            gamma += '1'
            epsilon += '0'
    return int(gamma, 2) * int(epsilon, 2)


def part2(lines, mode):
    line_length = len(lines[0])
    for i in range(line_length):
        if len(lines) == 1:
            break

        zeroes = ones = 0
        for line in lines:
            if line[i] == '0':
                zeroes += 1
            else:
                ones += 1
        if zeroes == ones:
            filter_value = '1'
        elif zeroes > ones:
            filter_value = '0'
        else:
            filter_value = '1'

        # For co2, flip the filter.
        if mode == 'co2':
            filter_value = '0' if filter_value == '1' else '1'

        lines = [line for line in lines if line[i] == filter_value]

    return int(lines[0], 2)


print('Part 1: ', part1(lines))
print('Part 2: ', part2(lines, 'oxygen') * part2(lines, 'co2'))