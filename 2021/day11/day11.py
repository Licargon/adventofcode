# -*- coding: utf-8 -*-
from copy import deepcopy

with open('day11.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

grid = {}
for row, line in enumerate(lines):
    for col, level in enumerate(line):
        grid[(row, col)] = int(level)


def step_increase_levels(grid):
    # Increase all levels by 1
    for coord in grid.keys():
        grid[coord] += 1


def flash(grid, already_flashed):
    # Flash all octopi once, if they havent already this step.
    flashed = set()
    for coord, level in grid.items():
        if level > 9 and coord not in already_flashed and coord not in flashed:
            flashed.add(coord)
            y, x = coord
            # Increase all neighbours by 1, also diagonal
            for dx in [-1, 0, 1]:
                for dy in [-1, 0, 1]:
                    if dx == 0 and dy == 0:
                        continue
                    if (y+dy, x+dx) in grid:
                        grid[(y+dy, x+dx)] += 1

    return flashed


def reset_flashed_octopi(grid, flashed):
    for coord in flashed:
        grid[coord] = 0


def print_grid(grid):
    row = []
    cur_row = 0
    for coord, value in grid.items():
        if coord[0] != cur_row:
            print(row)
            row = [value]
            cur_row += 1
        else:
            row.append(value)
    print(row)
    print()

def step_grid(grid):
    # Do all the grid operations and return how many octopi have flashed
    step_increase_levels(grid)

    flashed_octopi = set()
    while True:
        # Keep track of all octopi that have flashed, because they can only flash once per step
        flashed = flash(grid, flashed_octopi)
        if not flashed:
            break
        flashed_octopi |= flashed
    reset_flashed_octopi(grid, flashed_octopi)
    return len(flashed_octopi)

def part1(grid):
    steps = 100
    part1 = 0
    for step in range(steps):
        part1 += step_grid(grid)
    return part1

def part2(grid):
    # Figure out when all octopi flash at once
    grid_size = len(grid.keys())
    cnt = 0
    while True:
        flashed = step_grid(grid)
        cnt += 1
        if flashed == grid_size:
            return cnt


print('Part 1: ', part1(deepcopy(grid)))
print('Part 2: ', part2(deepcopy(grid)))
