# -*- coding: utf-8 -*-

with open('day2.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

def part1(lines):
    x = 0
    y = 0
    for line in lines:
        direction, magnitude = line.split(' ')
        magnitude = int(magnitude)
        if direction == 'forward':
            x += magnitude
        if direction == 'down':
            y += magnitude
        if direction == 'up':
            y -= magnitude
    return x * y

def part2(lines):
    x = 0
    y = 0
    aim = 0
    for line in lines:
        direction, magnitude = line.split(' ')
        magnitude = int(magnitude)
        if direction == 'forward':
            x += magnitude
            y += aim * magnitude
        if direction == 'down':
            aim += magnitude
        if direction == 'up':
            aim -= magnitude
    return x * y

print('Part 1: ', part1(lines))
print('Part 2: ', part2(lines))