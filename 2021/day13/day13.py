# -*- coding: utf-8 -*-
from collections import defaultdict, Counter
import re

with open('day13.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

coords = set()
for idx, line in enumerate(lines):
    if line == '':
        break
    x, y = line.split(',')
    coords.add((int(y), int(x)))

folds = []
for idx2 in range(idx+1, len(lines)):
    match = re.findall(r'fold along ([xy])=([\d]+)', lines[idx2])
    if match:
        folds.append((match[0][0], int(match[0][1])))


def fold_y(coords, line_location):
    new_coords = set()
    for coord in coords:
        if coord[0] < line_location:
            new_coords.add(coord)
            continue
        dist = coord[0] - line_location
        new_coords.add((line_location - dist, coord[1]))
    return new_coords


def fold_x(coords, line_location):
    new_coords = set()
    for coord in coords:
        if coord[1] < line_location:
            new_coords.add(coord)
            continue
        dist = line_location - coord[1]
        new_coords.add((coord[0], line_location + dist))
    return new_coords


def draw(coords):
    # Create the grid and draw it out
    min_x = min_y = 9999999
    max_x = max_y = 0
    for coord in coords:
        min_x = min(min_x, coord[1])
        max_x = max(max_x, coord[1])
        min_y = min(min_y, coord[0])
        max_y = max(max_y, coord[0])

    grid = []
    for y in range(max_y+1):
        grid.append([' . '] * (max_x+1))

    for coord in coords:
        grid[coord[0]][coord[1]] = ' # '

    for line in grid:
        print(''.join(line))

# Part 1, do just the first fold
coords = fold_x(coords, folds[0][1])

print('Part 1: ', len(coords))

for fold_dir, fold_line in folds[1:]:
    if fold_dir == 'x':
        coords = fold_x(coords, fold_line)
    else:
        coords = fold_y(coords, fold_line)

print('Part 2: ')
draw(coords)