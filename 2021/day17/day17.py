# -*- coding: utf-8 -*-
from copy import deepcopy

import math

from typing import Tuple

with open('day17.in') as f:
    input = list(map(lambda line: line.rstrip('\n'), f.readlines()))[0]

# Fuck regex. Fuck it.
coords_part = input.split(': ')[1]
x_part, y_part = coords_part.split(', ')
x_part = x_part[2:]
y_part = y_part[2:]
x0 = int(x_part[:x_part.find('.')])
x1 = int(x_part[x_part.find('.')+2:])
y0 = int(y_part[:y_part.find('.')])
y1 = int(y_part[y_part.find('.')+2:])

print('Target at: ', x0, x1, y0, y1)


class TargetArea(object):
    def __init__(self, x0: int, x1: int, y0: int, y1: int):
        self.x0 = x0
        self.x1 = x1
        self.y0 = y0
        self.y1 = y1

    def hit(self, x: int , y: int) -> bool:
        if self.x0 <= x <= self.x1 and self.y0 <= y <= self.y1:
            return True
        return False


class Probe(object):
    start_x = 0
    start_y = 0

    def __init__(self, x_vel, y_vel):
        self.cur_x = self.cur_y = 0
        self.x_vel = x_vel
        self.y_vel = y_vel

    def trace(self, steps: int) -> Tuple[int, int]:
        for _ in range(steps):
            self.cur_x += self.x_vel
            self.cur_y += self.y_vel

            # Drag towards 0
            if self.x_vel > 0:
                self.x_vel -= 1
            elif self.x_vel < 0:
                self.x_vel += 1
            self.y_vel -= 1

            yield self.cur_x, self.cur_y


def part1():
    target = TargetArea(x0, x1, y0, y1)

    max_y = 0
    best_velocity = (0,0)
    successes = 0
    for start_x in range(-10, 200):
        for start_y in range(-1000, 1000):
            new_max_y = 0
            probe = Probe(start_x, start_y)
            for point in probe.trace(1000):
                # Break if we end up to the right of the target
                if point[0] > x1:
                    break
                # Break if we end up below the target.
                if point[1] < min(y0, y1):
                    break
                new_max_y = max(new_max_y, point[1])
                if target.hit(*point):
                    successes += 1
                    if new_max_y > max_y:
                        # Record this as the highest valid max y
                        max_y = new_max_y
                        best_velocity = (start_x, start_y)
                    break
    print(f'Part 1: max height {max_y} at velocity {best_velocity}')
    print(f'Part 2: total successes: {successes}')


if __name__ == "__main__":
    part1()
