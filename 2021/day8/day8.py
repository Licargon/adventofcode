# -*- coding: utf-8 -*-

with open('day8.in') as f:
    in_lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

lines = []
for line in in_lines:
    split = line.split(' | ')
    lines.append((
        split[0].split(' '), split[1].split(' ')
    ))

def deduce_display_numbers(line):
    # 1, 4, 7, 8 use unique mapping, so we can deduce those based on their length
    input = line[0]
    output = line[1]
    one = list(filter(lambda el: len(el) == 2, input))[0]
    four = list(filter(lambda el: len(el) == 4, input))[0]
    seven = list(filter(lambda el: len(el) == 3, input))[0]
    eight = list(filter(lambda el: len(el) == 7, input))[0]
    top_part = list(set(seven) - set(one))
    assert len(top_part) == 1

    # 3 has 3 segments in common with 7, so we can figure out which display represents 3
    for display in input:
        if len(display) == 5 and len(set(display) & set(seven)) == len(set(seven)):
            three = display
            break

    # If we know 3, we can use 3, 4 and 7 to deduce which one the middle line is between those sets
    middle_part = list((set(three) & set(four)) - set(seven))
    assert len(middle_part) == 1
    # Bottom line: Same shit. 3 - 7 - the middle line = the bottom line
    bottom_part = list(set(three) - set(seven) - set(middle_part))
    assert len(bottom_part) == 1

    # To get 0, we look for a display with 6 parts which does not contain the middle line
    for display in input:
        if len(display) == 6 and middle_part[0] not in display:
            zero = display
            break
    bottom_left = list(set(zero) - set(four) - set(three))
    assert len(bottom_left) == 1

    # 2 and 5 can be deduced by looking for displays of length 5 that do, or do not contain the
    # bottom left part
    for display in input:
        if display == three:
            continue
        if len(display) == 5 and bottom_left[0] not in display:
            five = display
        elif len(display) == 5 and bottom_left[0] in display:
            two = display

    # Top left line can be deduced by subtracting the part of 5 and 3
    top_left = list(set(five) - set(three))
    assert len(top_left) == 1

    # 'Flipped 3' has 5 lines in common with 6
    inverted_three = set([top_left[0], top_part[0], middle_part[0], bottom_left[0], bottom_part[0]])
    for display in input:
        if len(display) == 6:
            if len(set(inverted_three) & set(display)) == 5:
                six = display
                break

    # By definition we just need to find 9 now
    for i in input:
        if i in [zero, one, two, three, four, five, six, seven, eight]:
            continue
        nine = i
        break

    # Create a mapping from our deductions to actual numbers
    mapping = {}
    mapping[''.join(sorted(zero))] = 0
    mapping[''.join(sorted(one))] = 1
    mapping[''.join(sorted(two))] = 2
    mapping[''.join(sorted(three))] = 3
    mapping[''.join(sorted(four))] = 4
    mapping[''.join(sorted(five))] = 5
    mapping[''.join(sorted(six))] = 6
    mapping[''.join(sorted(seven))] = 7
    mapping[''.join(sorted(eight))] = 8
    mapping[''.join(sorted(nine))] = 9

    # Convert the given output to the correct number representation
    number = ''
    for digit in output:
        number += str(mapping[''.join(sorted(digit))])
    return number

numbers = []
for line in lines:
    numbers.append(deduce_display_numbers(line))

part1 = 0
part2 = 0
for number in numbers:
    part1 += number.count('1') + number.count('4') + number.count('7') + number.count('8')
    part2 += int(number)
print('Part 1: ', part1)
print('Part 2: ', part2)

