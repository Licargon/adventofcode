# -*- coding: utf-8 -*-

with open('day7.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))[0]

crabs = list(map(int, lines.split(',')))

possible_positions = [-1 for _ in range(max(crabs))]

# For every possible possible, sum up all the movements every crab has to make to get there
for idx in range(len(possible_positions)):
    combined_dist = 0
    for crab in crabs:
        combined_dist += abs(crab-idx)
    possible_positions[idx] = combined_dist

print('Part 1: ', min(possible_positions))

possible_positions = [-1 for _ in range(max(crabs))]

# Other distance calculation (1 step = 1, 2 steps = 1+2, 3 steps = 1+2+3, ...)
# Sum of 1->n = n*(n+1)/2
for idx in range(len(possible_positions)):
    combined_dist = 0
    for crab in crabs:
        gap = abs(crab-idx)
        combined_dist += gap * (gap+1) / 2
    possible_positions[idx] = combined_dist

print('Part 2: ', int(min(possible_positions)))