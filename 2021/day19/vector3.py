import math

class Vector3(object):
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __sub__(self, other):
        return Vector3(self.x-other.x, self.y-other.y, self.z-other.z)

    def __add__(self, other):
        return Vector3(self.x+other.x, self.y+other.y, self.z+other.z)

    def __mul__(self, other):
        return Vector3(self.x*other, self.y*other, self.z*other)

    __radd__ = __add__

    def as_list(self):
        return [self.x, self.y, self.z]

    def as_frozenset(self):
        return frozenset([self.x, self.y, self.z])

    def as_tuple(self):
        return tuple([self.x, self.y, self.z])

    def rot_x(self):
        return Vector3(*[int(x) for x in R.from_euler('x', 90, degrees=True).apply(self.as_list())])

    def rot_y(self):
        return Vector3(*[int(x) for x in R.from_euler('y', 90, degrees=True).apply(self.as_list())])

    def rot_z(self):
        return Vector3(*[int(x) for x in R.from_euler('z', 90, degrees=True).apply(self.as_list())])

    def translate(self, x, y, z):
        return Vector3(self.x+x, self.y+y, self.z+z)

    def __str__(self):
        return '<{}, {}, {}>'.format(self.x, self.y, self.z)

    def __eq__(self, other):
        return (self.x == other.x) and (self.y == other.y) and (self.z == other.z)

    def dist(self, other):
        return math.sqrt((other.x-self.x)**2 + (other.y-self.y)**2 + (other.z-self.z)**2)
