# -*- coding: utf-8 -*-
from copy import deepcopy

from collections import defaultdict, Counter
from itertools import permutations

import re

import numpy as np


import vector3
Vector3 = vector3.Vector3

# Get all 24 possible rotations. Thanks Reddit.
rot = []
for x in [-1, 1]:
    for y in [-1, 1]:
        for z in [-1, 1]:
            for q in permutations([[x,0,0], [0,y,0], [0,0,z]]):
                m = np.array(q)
                if np.linalg.det(m) == 1:
                    rot.append(m)

with open('day19.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

beacons = []
scanner = None
scanner_to_beacons = defaultdict(list)
scanning_beacons = False
cur_idx = 0
while cur_idx < len(lines):
    if lines[cur_idx] == '':
        scanning_beacons = False
        cur_idx += 1
        continue
    if not scanning_beacons:
        # Read a scanner ID
        regex = re.compile(r'--- scanner ([0-9]+) ---')
        match = regex.match(lines[cur_idx])
        scanner = int(match.groups()[0])
        cur_idx += 1
        scanning_beacons = True
    else:
        coords = [int(x) for x in lines[cur_idx].split(',')]
        scanner_to_beacons[scanner].append(coords)
        cur_idx += 1


# Assume that scanner 0 is at absolute pos 0
scanner_positions = {0: (0,0,0)}

unique_beacons = set()
for beacon in scanner_to_beacons[0]:
    unique_beacons.add(tuple(beacon))


# Keep scanning untill we figure out the positions of all scanners.
# Depending on the order of the input file we may not have a logical order, requiring multiple passes
while len(scanner_positions.keys()) != len(scanner_to_beacons.keys()):
    for scanner_id in scanner_to_beacons.keys():
        for scanner_id2 in scanner_to_beacons.keys():
            # Dont compare the same scanners
            if scanner_id2 == scanner_id:
                continue

            # If we dont know the position of our 'source' yet, skip
            if scanner_id not in scanner_positions:
                continue

            # If we already calculated the position of this one, skip.
            if scanner_id2 in scanner_positions:
                continue

            beacons = scanner_to_beacons[scanner_id2]

            for rotation in rot:
                rotated_beacons = [rotation.dot(beacon) for beacon in scanner_to_beacons[scanner_id2]]

                # Calculate vector between all pairs of beacons between the rotated one, and the original one
                # This represents the distance between them as a vector (and less computations than calculating the actual distance..)
                ctr = Counter()
                for beacon1 in rotated_beacons:
                    for beacon2 in scanner_to_beacons[scanner_id]:
                        ctr[(Vector3(*beacon2)-Vector3(*beacon1)).as_tuple()] += 1

                # If we find at least 12 matching inter-beacon-distances,
                #   we know we have an overlap (distance is invariant to rotation)
                translation_vector, no_of_matches = ctr.most_common(1)[0]
                if no_of_matches >= 12:
                    for beacon in rotated_beacons:
                        unique_beacons.add((Vector3(*beacon) + Vector3(*translation_vector) + Vector3(*scanner_positions[scanner_id])).as_tuple())
                    # IMPORTANT. This is now the 'correct' orientation for this set -> use it in the future
                    # Makes it so vector from 0->1, 1->2, ... works properly when adding multiple vectors down the line.
                    scanner_to_beacons[scanner_id2] = rotated_beacons
                    scanner_positions[scanner_id2] = (Vector3(*scanner_positions[scanner_id]) + Vector3(*translation_vector)).as_list()
                    break


print('Part 1: ', len(unique_beacons))

# Figure out the max manhattan distance between scanners
cur_max = -1e9
scanners = list(scanner_positions.keys())
for scanner1 in scanner_positions.values():
    for scanner2 in scanner_positions.values():
        manh_dist = sum(abs(val2-val1) for val1, val2 in zip(scanner1, scanner2))
        cur_max = max(cur_max, manh_dist)

print('Part 2: ', cur_max)