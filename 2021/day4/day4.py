# -*- coding: utf-8 -*-

import re
from copy import deepcopy

with open('day4.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))
    # Empty string to ease parsing
    lines.append('')

# Extract all number on the first line and convert to int
drawn_numbers = list(map(int, re.findall('(\d+)', lines[0])))

bingo_boards = []
cur_board = []
for line in lines[2:]:
    if line == '':
        bingo_boards.append(cur_board)
        cur_board = []
    else:
        values = list(map(int, re.findall('(\d+)', line)))
        cur_board.append(values)

def is_row_winning(row):
    return all([element == None for element in row])

def mark_off_number(board, number) -> bool:
    # Mark off a number on the board and return True if this causes the board to become winning
    for idx_row, row in enumerate(board):
        for idx_col, col in enumerate(row):
            if col == number:
                board[idx_row][idx_col] = None
                col_list = [board[rr][idx_col] for rr in range(len(board))]
                if is_row_winning(row) or is_row_winning(col_list):
                    return True
    return False

def calculate_score(board, last_drawn_number):
    total = 0
    for row in board:
        for col in row:
            if col:
                total += col
    return total * last_drawn_number

def play_bingo(boards, drawn_numbers):
    # Figure out the first board to win
    for number in drawn_numbers:
        for board in boards:
            win = mark_off_number(board, number)
            if win:
                return calculate_score(board, number)

def play_bingo_part2(boards, drawn_numbers):
    # Figure out the last board to win
    last_winning_board = None
    last_winning_number = None
    won_boards = set()
    for number in drawn_numbers:
        for board_idx, board in enumerate(boards):
            # Board has already won. Skip it.
            if board_idx in won_boards:
                continue
            win = mark_off_number(board, number)
            if win:
                won_boards.add(board_idx)
                if len(won_boards) == len(boards):
                    # Final board to win has been found
                    return calculate_score(board, number)
    return -1

print('Part 1: ', play_bingo(deepcopy(bingo_boards), drawn_numbers))
print('Part 2: ', play_bingo_part2(deepcopy(bingo_boards), drawn_numbers))
