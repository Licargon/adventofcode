class Test(object):
    def find_possible_routes(self, amphipod, location):
        """
        DFS. Find possible ending locations for our amphipod to end up in

        We cant stop in a FORBIDDEN square, we can't walk into a pocket that we dont belong in.
        """
        row, col = location
        locations = set()
        if row > 2 and self.grid[(row-1, col)] != '.':
            # Something is above us.
            return []
        elif row == 1:
            # Amphipod has already moved, see if we can reach an end square
            can_reach_goal = self.amphipod_has_route_to_goal(amphipod, location)
            if can_reach_goal[1]:
                # Return move to the finish line
                return [can_reach_goal[1]]
        # Dont move up in the room, just go into the hallway immediately if the path is clear
        # elif row == 2 or (row == 3 and self.grid[(2, col)] == '.'):
        else:
            for cc in range(col, 12):
                if self.grid[(1, cc)] != '.': # Something in the way
                    break
                # Cant stop above a pocket
                if cc in self.goal_columns:
                    continue
                locations.add((1, cc))

            for cc in range(col, 0, -1):
                if self.grid[(1, cc)] != '.': # Something in the way
                    break
                # Cant stop above a pocket
                if cc in self.goal_columns:
                    continue
                locations.add((1, cc))
        return locations