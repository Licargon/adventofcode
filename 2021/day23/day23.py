# -*- coding: utf-8 -*-
from collections import defaultdict, deque, Counter
from pprint import pprint
from copy import deepcopy
import functools
import math
import re

def read_file(filename):
    with open(filename) as f:
        lines = f.readlines()
    grid = {}
    amphipods = defaultdict(set)
    # Goals for the amphipods in coords
    for row, line in enumerate(lines):
        for col, char in enumerate(line.strip('\n')):
            grid[(row, col)] = char
            if char in 'ABCD':
                amphipods[char].add((row, col))

    return grid, amphipods

energy = {'A': 1, 'B': 10, 'C': 100, 'D': 1000}
goal_columns = [3,5,7,9]
DEST = {amphipod: col for (amphipod, col) in zip('ABCD', sorted(list(goal_columns)))}


class Grid(object):
    def __init__(self, grid, goal_columns, amphipods, DEST, goal_height):
        self.grid = grid
        self.goal_columns = goal_columns
        self.amphipods = amphipods
        self.DEST = DEST
        # Size of the pockets they have to go in
        self.goal_height = goal_height

    def print(self):
        print_row = ''
        cur_row = 0
        for coord, char in self.grid.items():
            row, col = coord
            if row != cur_row:
                print(print_row)
                print_row = char
                cur_row = row
            else:
                print_row += char
        print(print_row)

    def copy(self):
        return Grid(deepcopy(self.grid), self.goal_columns, deepcopy(self.amphipods),
                    self.DEST, self.goal_height)

    def done(self) -> bool:
        # Check if all pockets are filled with the correct amphipod
        for amphipod, locations in self.amphipods.items():
            for location in locations:
                if location[1] != self.DEST[amphipod]:
                    return False

        return True

    def amphipod_has_route_to_goal(self, amphipod, cur_pos):
        """
        Amphipod has a clear route to the goal. Meaning the pocket is empty, or there are only the same amphipods there
        As amphipods cant move into pockets otherwise, this is always the preferred route to take

        Returns energy usage if moved and the new position, else -1, None
        """
        pocket = self.DEST[amphipod]
        in_pocket = self.amphipods_in_pocket(pocket)
        # Full, or something else in the pocket
        # if len(in_pocket) == self.goal_height or (len(in_pocket) and in_pocket[0] != amphipod):
        if len(in_pocket) == self.goal_height or (len(in_pocket) and not all([ip == amphipod for ip in in_pocket])):
            # print(amphipod, in_pocket, self.goal_height)
            return -1, None

        # Check if dont encounter anything on the way there
        row, col = cur_pos
        for cc in range(min(col, pocket)+1, max(col, pocket)):
            if self.grid[(row, cc)] != '.':
                # Something else is blocking us
                return -1, None
        # Return amount of moves + (goal_height-total amphipods already there) * energy cost
        # and the new position
        new_pos = (self.goal_height+1-len(in_pocket), self.DEST[amphipod])
        return (abs(col-pocket) + self.goal_height-len(in_pocket)) * energy[amphipod], new_pos

    def amphipods_in_pocket(self, pocket_id):
        # Return all amphipods in a pocket, if any
        amphipods = []
        for row in range(2, 2+self.goal_height):
            if self.grid[(row, pocket_id)] not in '.#':
                amphipods.append(self.grid[(row, pocket_id)])
        return amphipods

    def find_possible_routes(self, amphipod, location):
        """
        DFS. Find possible ending locations for our amphipod to end up in

        """
        row, col = location
        locations = set()
        if row == 1:
            # Amphipod has already moved, see if we can reach an end square
            can_reach_goal = self.amphipod_has_route_to_goal(amphipod, location)
            if can_reach_goal[1]:
                # Return move to the finish line
                return [can_reach_goal[1]]

        # Dont move up in the room, just go into the hallway immediately if the path is clear
        elif row == 2 or (row == 3 and self.grid[(2, col)] == '.') or (row == 4 and self.grid[(3, col)] == '.') or (row == 5 and self.grid[(4, col)] == '.'):
            for cc in range(col, 12):
                if self.grid[(1, cc)] != '.': # Something in the way
                    break
                # Cant stop above a pocket
                if cc in self.goal_columns:
                    continue
                locations.add((1, cc))

            for cc in range(col, 0, -1):
                if self.grid[(1, cc)] != '.': # Something in the way
                    break
                # Cant stop above a pocket
                if cc in self.goal_columns:
                    continue
                locations.add((1, cc))
        return locations

    def validate(self):
        ctr = Counter(self.grid.values())
        assert ctr['A'] == self.goal_height
        assert ctr['B'] == self.goal_height
        assert ctr['C'] == self.goal_height
        assert ctr['D'] == self.goal_height

    def memo_key(self):
        memo_key = ''
        for amphipod in sorted(self.amphipods):
            memo_key += amphipod
            for pos in sorted(self.amphipods[amphipod]):
                memo_key += '{},{};'.format(pos[0], pos[1])
        return memo_key

    def solve(self):
        Q = deque([self])
        # Memo maps board state to least known cost for state.
        # memo key is a sorted list of all amphipod locations in string format
        memo = {self.memo_key(): 0}
        while Q:
            G = Q.popleft()

            # if G.done():
            #     # We're done here.
            #     return memo

            for amphipod, positions in G.amphipods.items():
                for position in positions:
                    # Find all possible new locations we can reach from here, keeping in mind that we cant move through
                    # stuff
                    locations = sorted(G.find_possible_routes(amphipod, position))
                    for possible_location in locations:
                        # Move the amphipod in the new grid
                        new_G = G.copy()
                        new_G.grid[position] = '.'
                        new_G.grid[possible_location] = amphipod
                        new_G.amphipods[amphipod].remove(position)
                        new_G.amphipods[amphipod].add(possible_location)
                        spaces_moved = abs(position[0] - possible_location[0]) + abs(position[1]-possible_location[1])
                        # new_energy is the previous lowest cost of the board state + the energy we just expended
                        new_energy = memo[G.memo_key()] + (spaces_moved * energy[amphipod])
                        try:
                            new_G.validate()
                        except Exception as e:
                            # Debug reasons
                            print('ERROR')
                            locations = sorted(G.find_possible_routes(amphipod, position))
                            raise

                        # If we end up in the new state with a lower energy expenditure, we mark it in the memo and go further from here
                        # If not, we ignore the state as its less feasible than a previous state we've already found and explored
                        if new_energy < memo.get(new_G.memo_key(), 1e20):
                            memo[new_G.memo_key()] = new_energy
                            Q.append(new_G)

        return memo

import time

start = time.time()

grid, amphipods = read_file('day23.in')
G = Grid(grid, goal_columns, amphipods, DEST, 2)
G.print()
memo = G.solve()
goal_state = 'A2,3;3,3;B2,5;3,5;C2,7;3,7;D2,9;3,9;'
print('Part 1: ', memo[goal_state])
print('Part 1 done after ', time.time()-start)

start = time.time()

grid, amphipods = read_file('day23.in2')
G = Grid(grid, goal_columns, amphipods, DEST, 4)
G.print()
memo = G.solve()
goal_state = 'A2,3;3,3;4,3;5,3;B2,5;3,5;4,5;5,5;C2,7;3,7;4,7;5,7;D2,9;3,9;4,9;5,9;'
print('Part 2: ', memo[goal_state])
print('Part 2 done after ', time.time()-start)