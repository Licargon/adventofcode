# -*- coding: utf-8 -*-
from copy import deepcopy

import ast
import math

with open('day18.in') as f:
    input = list(map(lambda line: line.rstrip('\n'), f.readlines()))

numbers = []
for line in input:
    numbers.append(ast.literal_eval(line))


class SnailfishNumber(object):
    def __init__(self, parent=None):
        self.parent = parent

    def reduce(self):
        # This makes sure we first try an explode, then a split in every step.
        while self.explode() or self.split():
            pass

    # Base interface
    def explode(self):
        return False

    def split(self, *args):
        return False


class RegularNumber(SnailfishNumber):
    def __init__(self, val, parent=None):
        super(RegularNumber, self).__init__(parent)
        self.val = val

    def split(self, side):
        """
        Side = 0: left
        Side = 1: right

        To determine which part of the parent we have to append the split number to
        """
        if self.val >= 10:
            new_pair = PairNumber(parent=self.parent)
            new_pair.set_left(RegularNumber(self.val // 2, parent=new_pair))
            new_pair.set_right(RegularNumber(math.ceil(self.val/2), parent=new_pair))
            if side == 0:
                self.parent.left_child = new_pair
            else:
                self.parent.right_child = new_pair
            return True
        return False

    def explode(self):
        # Regular number can't explode
        return False

    def print(self):
        return str(self.val)

    def in_order(self):
        return [(self, self.val)]

    def magnitude(self):
        return self.val


class PairNumber(SnailfishNumber):
    def __init__(self, parent=None):
        super(PairNumber, self).__init__(parent)
        self.left_child = None
        self.right_child = None

    @property
    def depth(self):
        depth = 0
        parent = self.parent
        while parent:
            depth += 1
            parent = parent.parent
        return depth

    def set_left(self, number):
        self.left_child = number
        self.left_child.parent = self

    def set_right(self, number):
        self.right_child = number
        self.right_child.parent = self

    def add_number(self, right):
        # A + B -> [A, B]
        new_root = PairNumber()
        new_root.left_child = self
        new_root.right_child = self.parse_number(right)
        new_root.left_child.parent = new_root
        new_root.right_child.parent = new_root
        return new_root

    def in_order(self):
        # In order traverse the tree
        res = []
        if self.left_child:
            res = self.left_child.in_order()
        if self.right_child:
            res += self.right_child.in_order()
        return res

    def explode(self):
        if self.depth == 4:
            # Exploding pair will ALWAYS be 2 regular vals, per assignment
            root = self
            while root.parent:
                root = root.parent

            """
            Figure out which regular numbers comes right before and after the pair we are exploding.
            
            Lazy way: find the root, in_order traverse the tree and fetch the nodes right before and after the pair
            """
            in_order = root.in_order()
            node_index = in_order.index((self.left_child, self.left_child.val))

            before = node_index - 1
            if before >= 0:
                in_order[before][0].val += self.left_child.val
            # Move up 2 because the node_index points to the left right and we need the one to the right of the RIGHT child
            after = node_index + 2
            if after < len(in_order):
                in_order[after][0].val += self.right_child.val

            # Replace self with regular number 0
            new_node = RegularNumber(0, parent=self.parent)
            # Replace the correct link of the parent
            if self.parent.left_child == self:
                self.parent.left_child = new_node
            elif self.parent.right_child == self:
                self.parent.right_child = new_node

            return True

        elif self.left_child or self.right_child:
            # Recurse down
            return self.left_child.explode() or self.right_child.explode()

        return False

    def split(self, *args):
        # Cant split pairs, check the children
        return self.left_child.split(0) or self.right_child.split(1)

    def _parse_number(self, number, parent):
        left = number[0]
        if isinstance(left, int):
            parent.set_left(RegularNumber(left, parent=parent))
        else:
            parent.set_left(PairNumber.parse_number(left))

        right = number[1]
        if isinstance(right, int):
            parent.set_right(RegularNumber(right, parent=parent))
        else:
            parent.set_right(PairNumber.parse_number(right))

        return parent

    @classmethod
    def parse_number(self, number):
        """
        Parse the number and create the tree of parent/children
        :param number:
        :return:
        """
        root = PairNumber()
        root._parse_number(number, root)
        return root

    def print(self):
        return '[' + self.left_child.print() + ',' + self.right_child.print() + ']'

    def magnitude(self):
        return 3 * self.left_child.magnitude() + 2 * self.right_child.magnitude()


current_number = PairNumber.parse_number(numbers[0])
for number in numbers[1:]:
    current_number = current_number.add_number(number)
    current_number.reduce()

print('Part 1: ', current_number.magnitude())

current_max = -1e9

for idx1, num1 in enumerate(numbers):
    for idx2, num2 in enumerate(numbers):
        if idx1 == idx2:
            continue
        number = PairNumber.parse_number(num1)
        number = number.add_number(num2)
        number.reduce()
        mag = number.magnitude()
        current_max = max(current_max, mag)


print('Part 2: ', current_max)

