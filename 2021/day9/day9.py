# -*- coding: utf-8 -*-

from collections import deque

with open('day9.in') as f:
    in_lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

grid = []
for in_line in in_lines:
    line = []
    for char in in_line:
        line.append(int(char))
    grid.append(line)

def get_neighbours(grid, row, col):
    # Only deal with orthogonal neighbours
    neighbours = []
    for y_diff in [-1, 1]:
        if (0 <= row + y_diff < len(grid)):
            neighbours.append((row + y_diff, col))
    for x_diff in [-1, 1]:
        if (0 <= col + x_diff < len(grid[0])):
            neighbours.append((row, col + x_diff))

    return neighbours

def calculate_low_points(grid):
    low_points = []

    for y, row in enumerate(grid):
        for x, col in enumerate(row):
            neighbours = get_neighbours(grid, y, x)
            is_low_spot = True
            for rr, cc in neighbours:
                if col >= grid[rr][cc]:
                    is_low_spot = False
                    break
            if is_low_spot:
                low_points.append((y, x))

    return low_points

def calculate_basins(grid, low_points):
    # DFS from every low point to find the size of the basin (= increasing neighbours that are
    # not 9)
    def _dfs(row, col, grid):
        SEEN = set()
        SEEN.add((row, col))

        Q = deque()
        Q.append((row, col))
        BASIN_POINTS = {(row, col)}

        while Q:
            pos = Q.popleft()
            SEEN.add(pos)

            neighbours = get_neighbours(grid, *pos)
            for neighbour in neighbours:
                if neighbour in SEEN:
                    continue
                if grid[neighbour[0]][neighbour[1]] == 9:
                    continue
                if grid[neighbour[0]][neighbour[1]] <= grid[pos[0]][pos[1]]:
                    # Only check increasing neighbours
                    continue
                BASIN_POINTS.add(neighbour)
                Q.append(neighbour)

        return BASIN_POINTS

    basins = []
    for point in low_points:
        basins.append(_dfs(*point, grid))

    return basins

low_points = calculate_low_points(grid)
part1 = 0
for point in low_points:
    part1 += grid[point[0]][point[1]] + 1
print('Part 1: ', part1)

basins = calculate_basins(grid, low_points)
part2 = 1
# Keep the 3 largest basins
basins = sorted(basins, key=lambda el: len(el), reverse=True)[:3]
for basin in basins:
    part2 *= len(basin)

print('Part 2: ', part2)
