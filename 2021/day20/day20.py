# -*- coding: utf-8 -*-
from collections import defaultdict

with open('day20.in') as f:
    lines = f.readlines()

enhancer, image = ''.join(lines).split('\n\n')
enhancer = enhancer.replace('\n', '')
image = image.split('\n')

img = defaultdict(lambda: '.')
for rr, row in enumerate(image):
    for cc, col in enumerate(row):
        img[(rr, cc)] = col


class Image(object):
    def __init__(self, image, enhancer):
        self.image = image
        self.enhancer = enhancer
        self.step = 0
        # Start bounds
        min_x, max_x, min_y, max_y = self.get_bounds()
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y
        self.infinite_part = '.'

    def enhance(self):
        # Increase the search space by 1
        self.min_y -= 1
        self.max_y += 1
        self.min_x -= 1
        self.max_x += 1
        new_img = defaultdict(lambda: self.infinite_part)
        for row in range(self.min_y, self.max_y + 1):
            for col in range(self.min_x, self.max_x + 1):
                bit_string = ''
                for dy in [-1, 0, 1]:
                    for dx in [-1, 0, 1]:
                       bit_string += self.image[(row + dy, col + dx)]
                binary_string = bit_string.replace('.', '0').replace('#', '1')
                index = int(binary_string, 2)
                new_img[(row, col)] = self.enhancer[index]
        self.image = new_img
        self.step += 1

        if self.infinite_part == '.':
            self.infinite_part = '#'
        else:
            self.infinite_part = '.'

    def get_bounds(self):
        min_x = min([key[1] for key in self.image.keys()])
        max_x = max([key[1] for key in self.image.keys()])
        min_y = min([key[0] for key in self.image.keys()])
        max_y = max([key[1] for key in self.image.keys()])
        return min_x, max_x, min_y, max_y

    def print(self):
        for rr in range(self.min_y - 5, self.max_y + 5):
            row = ''
            for cc in range(self.min_x - 5, self.max_x + 5):
                row += self.image[(rr, cc)]
            print(row)

    def lit(self):
        cnt = 0
        for rr in range(self.min_y, self.max_y + 1):
            for cc in range(self.min_x, self.max_x + 1):
                if self.image[(rr, cc)] == '#':
                    cnt += 1
        return cnt


p1_img = Image(img, enhancer)
for _ in range(2):
    p1_img.enhance()
    # Swap the default value
print('Part 1: ', p1_img.lit())

p2_img = Image(img, enhancer)
for i in range(50):
    p2_img.enhance()
print('Part 2: ', p2_img.lit())