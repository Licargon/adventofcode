# -*- coding: utf-8 -*-
from collections import defaultdict, Counter

with open('day12.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

graph = defaultdict(list)
for line in lines:
    start, end = line.split('-')
    graph[start].append(end)
    graph[end].append(start)


paths = []
def dfs(graph, allow_visit_twice, current_path, visited):
    cur_cave = current_path[-1]

    if cur_cave == 'end':
        paths.append(current_path)
        return

    for connection in graph[cur_cave]:
        """
        allow_visit_twice = False: Don't allow visiting of lower case caves 
        allow_visit_twice = True: You can revisit 1 cave twice, so the most_common of the visited counter should be 0 or 1
        
        Don't revisit start
        """
        if (visited[connection] == 0 or (allow_visit_twice and visited and visited.most_common(1)[0][1] < 2)) and connection != 'start':
            # Edge case: Dont consider 'end' a lower case cave
            if connection.lower() == connection and connection != 'end':
                visited[connection] += 1
            dfs(graph, allow_visit_twice, current_path[:] + [connection], visited)
            if connection.lower() == connection and connection != 'end':
                visited[connection] -= 1


dfs(graph, False, ['start'], Counter())
print('Part 1: ', len(paths))

paths = []
dfs(graph, True, ['start'], Counter())
print('Part 2: ', len(paths))


