# -*- coding: utf-8 -*-
from copy import deepcopy

import heapq
import math

with open('day16.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))


def hex_to_binary(hex):
    binary_values = ''
    for char in hex:
        # Hex to int
        int_value = int(char, 16)
        # Int to binary, 4 digits
        binary_values += '{:04b}'.format(int_value)
    return binary_values


def parse_literal_value(binary):
    """
    Returns literal value, and the offset where we stopped
    :param binary:
    :return:
    """
    # Pad to multiple of 4
    padding = len(binary) % 4

    if padding:
        binary = binary + '0' * (4-padding)

    value = ''
    current_index = 0
    while True:
        if current_index + 5 > len(binary):
            break
        else:
            segment = binary[current_index:current_index+5]
            value += segment[1:5]
            current_index += 5
            if segment[0] == '0':
                break

    return int(value, 2), current_index


operations_dict = {
    0: sum,
    1: math.prod,
    2: min,
    3: max,
    5: lambda l: 1 if l[0] > l[1] else 0, #gt
    6: lambda l: 1 if l[0] < l[1] else 0, #lt,
    7: lambda l: 1 if l[0] == l[1] else 0, #eq
}

# Part 1 is the sum of all version numbers
part1 = 0


def parse_operator_packet(binary):
    global part1
    version_id = int(binary[0:3], 2)
    part1 += version_id

    type_id = int(binary[3:6], 2)
    if type_id == 4:
        literal_value, offset = parse_literal_value(binary[6:])
        print(f'Literal value packet with value {literal_value} and length {offset}')

        # Return the value, and the length of the packet.
        return literal_value, 6 + offset
    else:
        subpacket_values = []
        length_type_id = binary[6]
        if length_type_id == '0':
            packet_length = int(binary[7:22], 2)
            print(f'Operator packet with {packet_length} bits for subpackets, type {type_id}')
            offset = 22
            # Dont process packets that are too short. Min 5
            while packet_length > 5:
                subpacket_return = parse_operator_packet(binary[offset:offset+packet_length])
                if isinstance(subpacket_return, tuple):
                    subpacket_length = subpacket_return[1]
                    subpacket_values.append(subpacket_return[0])
                else:
                    subpacket_length = subpacket_return
                offset += subpacket_length
                packet_length -= subpacket_length

            result = operations_dict[int(type_id)](subpacket_values)
            print(f'-- Returning result {result}')
            # Return result and length of packet
            return result, 22 + int(binary[7:22], 2)
        else:
            total_subpackets = int(binary[7:18], 2)
            print(f'Operator packet with {total_subpackets} subpackets, type {type_id}')
            offset = 18
            for i in range(total_subpackets):
                subpacket_return = parse_operator_packet(binary[offset:])
                # If this is a tuple, we have (literal_value_result, offset)
                if isinstance(subpacket_return, tuple):
                    offset += subpacket_return[1]
                    subpacket_values.append(subpacket_return[0])
                else:
                    offset += subpacket_return
            result = operations_dict[int(type_id)](subpacket_values)
            print(f'-- Returning result {result}')
            # Return result and length of packet
            return result, offset


# Part 1 tests
# parse_operator_packet('00111000000000000110111101000101001010010001001000000000')
# parse_operator_packet('11101110000000001101010000001100100000100011000001100000')
# parse_operator_packet(hex_to_binary('8A004A801A8002F478'))
# parse_operator_packet(hex_to_binary('620080001611562C8802118E34'))
# parse_operator_packet(hex_to_binary('C0015000016115A2E0802F182340'))
# parse_operator_packet(hex_to_binary('A0016C880162017C3686B18A3D4780'))


binary_values = hex_to_binary(lines[0][:])
return_value = parse_operator_packet(binary_values)
print('Part 1: ', part1)
print('Part 2: ', return_value[0])

# Part 2 tests and return values
# print(parse_operator_packet(hex_to_binary('C200B40A82'))[0], 3)
# print(parse_operator_packet(hex_to_binary('04005AC33890'))[0], 54)
# print(parse_operator_packet(hex_to_binary('880086C3E88112'))[0], 7)
# print(parse_operator_packet(hex_to_binary('CE00C43D881120'))[0], 9)
# print(parse_operator_packet(hex_to_binary('D8005AC2A8F0'))[0], 1)
# print(parse_operator_packet(hex_to_binary('F600BC2D8F'))[0], 0)
# print(parse_operator_packet(hex_to_binary('9C005AC2F8F0'))[0], 0)
# print(parse_operator_packet(hex_to_binary('9C0141080250320F1802104A08'))[0], 1)
