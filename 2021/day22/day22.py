# -*- coding: utf-8 -*-
import re


def part1(cubes):
    # Count all active cubes within [-50, 50]
    on_cubes = set()
    for state, x0, x1, y0, y1, z0, z1 in cubes:
        for x in range(max(x0, -50), min(x1, 50)+1):
            for y in range(max(y0, -50), min(y1, 50)+1):
                for z in range(max(z0, -50), min(z1, 50)+1):
                    if state == 'on':
                        on_cubes.add((x,y,z))
                    else:
                        if (x,y,z) in on_cubes:
                            on_cubes.remove((x,y,z))

    return len(on_cubes)


def part2_split_combine(new_cube, current_cubes):
    # Helper function to combine a new incoming cube with currently existing ones
    stateA, xa0, xa1, ya0, ya1, za0, za1 = new_cube

    # Keep track of wether or not we added our cube, incase of no overlap
    added_cube = False

    # To avoid counting double
    cubes_to_remove = set()
    # New split cubes to add to our current set of cubes
    cubes_to_add = set()
    for (stateB, xb0, xb1, yb0, yb1, zb0, zb1) in current_cubes:
        # If no overlap, ignore
        # if (xa1 < xb0 or xb1 < xa0 or ya1 < yb0 or yb1 < ya0 or za1 < zb0 or zb1 < za0):
        #     continue
        if not (xb0 <= xa1 and xa0 <= xb1 and yb0 <= ya1 and ya0 <= yb1 and zb0 <= za1 and za0 <= zb1):
            continue
        # if not (xb0 <= xa1 and xa0 <= xb1 and yb0 <= ya1 and ya0 <= yb1 and zb0 <= za1 and za0 <= zb1):
        #     continue

        # Calculate the common parts of the cubes, AKA the overlap
        xc0, xc1 = max(xa0, xb0), min(xa1, xb1)
        yc0, yc1 = max(ya0, yb0), min(ya1, yb1)
        zc0, zc1 = max(za0, zb0), min(za1, zb1)

        # Remove the entire cube as we will be splitting it up due to overlap
        cubes_to_remove.add((stateB, xb0, xb1, yb0, yb1, zb0, zb1))
        added_cube = True

        # Add all cubes between [outer bound, overlap] in all dimensions.
        if xb0 < xc0:
            cubes_to_add.add((stateB, xb0, xc0-1, yb0, yb1, zb0, zb1))
        if xc1 < xb1:
            cubes_to_add.add((stateB, xc1+1, xb1, yb0, yb1, zb0, zb1))

        if yb0 < yc0:
            cubes_to_add.add((stateB, xc0, xc1, yb0, yc0-1, zb0, zb1))
        if yc1 < yb1:
            cubes_to_add.add((stateB, xc0, xc1, yc1+1, yb1, zb0, zb1))

        if zb0 < zc0:
            cubes_to_add.add((stateB, xc0, xc1, yc0, yc1, zb0, zc0-1))
        if zc1 < zb1:
            cubes_to_add.add((stateB, xc0, xc1, yc0, yc1, zc1+1, zb1))

        # Final case, cube is COMPLETELY within other cube
        cubes_to_add.add((stateA, min(xa0, xc0), max(xa1, xc1), min(ya0, yc0), max(ya1, yc1), min(za0, zc0), max(za1, zc1)))

    for to_remove in cubes_to_remove:
        current_cubes.remove(to_remove)

    for to_add in cubes_to_add:
        current_cubes.add(to_add)

    if not added_cube:
        # If no overlaps, just add this cube to the list as it wont be added otherwise.
        current_cubes.add((stateA, xa0, xa1, ya0, ya1, za0, za1))


def part2(input_cubes):
    # Keep track of all (split) cubes to count at the end
    # format (state,x0,x1,y0,y1,z0,z1)
    cubes = set()
    # print('Processing {} cubes'.format(len(input_cubes)))
    for input_cube in input_cubes:
        # print('Len cubes: ', len(cubes))
        part2_split_combine(input_cube, cubes)

    part2 = 0
    for state, x0, x1, y0, y1, z0, z1 in cubes:
        if state == 'on':
            part2 += abs(x1-x0+1) * abs(y1-y0+1) * abs(z1-z0+1)

    return part2


with open('day22.in') as f:
    lines = f.readlines()

regex = re.compile(r'([a-z]+) x=(-?[0-9]+)..(-?[0-9]+),y=(-?[0-9]+)..(-?[0-9]+),z=(-?[0-9]+)..(-?[0-9]+)')

cubes = []
for idx, line in enumerate(lines):
    match = regex.match(line.strip('\n'))
    if not match:
        assert False
    groups = match.groups()
    state = groups[0]
    coords = [int(x) for x in groups[1:]]
    x0, x1, y0, y1, z0, z1 = coords
    cubes.append((state, x0, x1, y0, y1, z0, z1))

print('Part 1: ', part1(cubes))
print('Part 2: ', part2(cubes))
