# -*- coding: utf-8 -*-
from collections import Counter

with open('day14.in') as f:
    lines = list(map(lambda line: line.rstrip('\n'), f.readlines()))

template = lines[0]
rules = {}
for line in lines[2:]:
    if line:
        rule_in, rule_out = line.split(' -> ')
        rules[rule_in] = rule_out


def solve(template, rules, steps):
    """
    Dont calculate the entire string, just keep track of pairs and which letters get added.
    """
    pairs_ctr = Counter()
    letter_ctr = Counter(template)
    for pair in zip(template, template[1:]):
        pairs_ctr[''.join(pair)] = 1

    for step in range(steps):
        new_ctr = Counter()
        for key, value in pairs_ctr.items():
            insert = rules[key]
            # e.g. AC -> B means that we have to insert AB and AC into our new Counter of pairs
            new_ctr[key[0] + insert] += value
            new_ctr[insert + key[1]] += value
            # Also count the letter being added.
            letter_ctr[insert] += value
        pairs_ctr = new_ctr

    return letter_ctr.most_common()[0][1] - letter_ctr.most_common()[-1][1]

print('Part 1: ', solve(template, rules, 10))
print('Part 2: ', solve(template, rules, 40))
