# -*- coding: utf-8 -*-

with open('day1.in') as f:
    lines = list(map(int, f.readlines()))

part1 = 0
for i in range(1, len(lines)):
    if lines[i] > lines[i - 1]:
        part1 += 1

print('Part 1: ', part1)

part2 = 0
# The middle 2 elements of the 2 windows are common, so we only need to compare 2 elements
for i in range(0, len(lines) - 3):
    if lines[i + 3] > lines[i]:
        part2 += 1

print('Part 2: ', part2)