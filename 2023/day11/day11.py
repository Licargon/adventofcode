# -*- coding: utf-8 -*-

import math

with open('day11.in') as f:
    lines = [line.strip() for line in f.readlines()]

expanded = []
expanded_rows = []
for idx, line in enumerate(lines):
    if all(ch == '.' for ch in line):
        expanded_rows.append(idx)

# lines = expanded
expanded_cols = []
for c in range(len(lines[0])):
    if all(lines[r][c] == '.' for r in range(len(lines))):
        expanded_cols.append(c)

# print('Expanded rows: ', expanded_rows)
# print('Expanded cols: ', expanded_cols)

# Find galaxies
galaxies = set()
for r, line in enumerate(lines):
    for c, ch in enumerate(line):
        if ch == '#':
            galaxies.add((r, c))

expanded_rows = set(expanded_rows)
expanded_cols = set(expanded_cols)


def get_total_distance(galaxies, part2=False):
    total_distance = 0
    expansion_factor = 1_000_000-1 if part2 else 1
    galaxies = list(galaxies)
    for idx, galaxy1 in enumerate(galaxies):
        for galaxy2 in galaxies[idx+1:]:
            ay, ax = min(galaxy1[0], galaxy2[0]), min(galaxy1[1], galaxy2[1])
            by, bx = max(galaxy1[0], galaxy2[0]), max(galaxy1[1], galaxy2[1])
            # Base manhattan distance
            dist = (by-ay) + (bx-ax)
            # Calculate for expansion factor
            dist += len(expanded_rows & set(range(ay, by+1))) * expansion_factor
            dist += len(expanded_cols & set(range(ax, bx+1))) * expansion_factor

            total_distance += dist
    return total_distance


print('Part 1: ', get_total_distance(galaxies))
print('Part 2: ', get_total_distance(galaxies, True))
