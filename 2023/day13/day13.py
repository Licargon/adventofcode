# -*- coding: utf-8 -*-

with open('day13.in') as f:
    patterns = f.read().split('\n\n')

def transpose(pattern):
    return [*zip(*pattern)]

def find_mirrored(pattern, part2=False):
    # Return the row above/below which the rows are mirrored
    if part2:
        allowed_diff = 1
    else:
        allowed_diff = 0
    for idx in range(1, len(pattern)):
        diff = 0
        for d in range(1, len(pattern)):
            top = idx-d
            bottom = idx+d-1
            if 0 <= top and bottom < len(pattern):
                for ch1, ch2 in zip(pattern[top], pattern[bottom]):
                    if ch1 != ch2:
                        diff += 1

        if diff == allowed_diff:
            return idx

    return 0


p1 = 0
for pattern in patterns:
    pattern = pattern.strip().split('\n')
    p1 += 100 * find_mirrored(pattern, False)
    p1 += find_mirrored(transpose(pattern), False)
print('Part 1:', p1)

p2 = 0
for pattern in patterns:
    pattern = pattern.strip().split('\n')
    p2 += 100 * find_mirrored(pattern, True)
    p2 += find_mirrored(transpose(pattern), True)
print('Part 2:', p2)

