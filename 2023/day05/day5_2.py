# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import re
import sys


class GardenMapping(object):
    def __init__(self, src, dest, ranges):
        self.src = src
        self.dest = dest
        self.ranges = []
        self.src_ranges = []
        self.dest_ranges = []

        self.parse_ranges(ranges)

    def map(self, input):
        for range_index, src_range in enumerate(self.src_ranges):
            if src_range[0] <= input < src_range[1]:
                return self.dest_ranges[range_index][0] + (input-src_range[0])
        # Not found -> return same number
        return input

    def parse_ranges(self, ranges):
        # [a, b) . Left inclusive, right not
        for dest_range_start, src_range_start, length in ranges:
            self.src_ranges.append((src_range_start, src_range_start+length))
            self.dest_ranges.append((dest_range_start, dest_range_start+length))


class FoodProduction(object):
    def __init__(self, lines):
        self.mappings = []
        self.parse(lines)

    def map(self, in_seed):
        for mapping in self.mappings:
            orig = in_seed
            in_seed = mapping.map(in_seed)
            # print('--- Mapping: {}-{}: {} -> {}'.format(mapping.src, mapping.dest, orig, in_seed))
        return in_seed

    def parse(self, lines):
        self.seeds = list(map(int, re.findall('\d+', lines[0])))
        src, dest = False, False
        values = []
        for line in lines[2:]:
            if not line:
                # Add current mapping
                self.mappings.append(
                    GardenMapping(src, dest, values)
                )
                values = []
            elif 'map:' in line:
                src = line.split('-to-')[0]
                dest = line.split('-to-')[1].split(' ')[0]
            else:
                values.append(list(map(int, re.findall(r'\d+', line))))
        # Add the last one
        self.mappings.append(
            GardenMapping(src, dest, values)
        )
        values = []


with open('day05.in') as f:
    lines = [line.strip() for line in f.readlines()]


production = FoodProduction(lines)
# print(0, production.mappings[0].map(0))
# print(1, production.mappings[0].map(1))
# print(48, production.mappings[0].map(48))
# print(49, production.mappings[0].map(49))
# print(50, production.mappings[0].map(50))
# print(51, production.mappings[0].map(51))
# print(96, production.mappings[0].map(96))
# print(97, production.mappings[0].map(97))
# print(98, production.mappings[0].map(98))
# print(99, production.mappings[0].map(99))

p1 = 1e50
print(production.seeds)
for seed in production.seeds:
    output = production.map(seed)
    p1 = min(p1, output)

print('Part 1: ', p1)



from time import time
p2 = 1e50
for range_start, range_length in zip(production.seeds[0::2], production.seeds[1::2]):
    # Re-calculate the range per layer
    # new_start, new_end = production.map_ranges(range_start, range_start+range_length)
    # p2 = min(p2, new_start)
    start = time()
    print(range_start, range_length, 'Cur min: ', p2)
    for seed in range(range_start, range_start+range_length ):
        output = production.map(seed)
        # if output < p2:
        #     print('{} < {}'.format(output, p2))
        p2 = min(p2, output)
    print('--- Done: {}'.format(time()-start))
    # lo = range_start
    # hi = range_start + range_length - 1
    # while lo < hi:
    #     mid = (lo + hi) // 2
    #     output = production.map(mid)

print('Part 2: ', p2)
# 100230635 too high
# 100170635 too high
# 100165635 too high
