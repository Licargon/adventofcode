# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import intervaltree
import re
import sys


class GardenMapping(object):
    def __init__(self, src, dest, ranges):
        self.src = src
        self.dest = dest
        self.ranges = []
        self.src_ranges = []
        self.dest_ranges = []

        self.parse_ranges(ranges)

    def map(self, input):
        for range_index, src_range in enumerate(self.src_ranges):
            if src_range[0] <= input < src_range[1]:
                return self.dest_ranges[range_index][0] + (input-src_range[0])
        # Not found -> return same number
        return input

    def parse_ranges(self, ranges):
        # [a, b) . Left inclusive, right not
        for dest_range_start, src_range_start, length in ranges:
            self.src_ranges.append((src_range_start, src_range_start+length))
            self.dest_ranges.append((dest_range_start, dest_range_start+length))

    def map_ranges(self, ranges):
        """
        Take in a list of tuples (ranges) and map them to where they belong after conversion by this layer
        """
        new_ranges = []

        for src_start, src_end in self.src_ranges:
            to_check = []
            # Check all range tuples. If we find one without any overlaps it will be added to 'ranges' again to be
            # checked by the other source tuples
            # If the list is empty it means we converted everything we needed to
            while ranges:
                range_start, range_end = ranges.pop()
                before = (range_start, min(range_end, src_start))
                overlap = ((max(range_start, src_start), min(range_end, src_end)))
                after = (max(src_end, range_start), range_end)
                if before[1] > before[0]:
                    to_check.append(before)
                if overlap[1] > overlap[0]:
                    # Dirty hack. End is non-inclusive, but are kept as inclusive in the list for no good reason at all
                    new_ranges.append((self.map(overlap[0]), self.map(overlap[1]-1)+1))
                if after[1] > after[0]:
                    to_check.append(after)
            ranges = to_check

        # Return all converted ranges + whatever ranges were not converted by this layer
        return new_ranges + to_check

class FoodProduction(object):
    def __init__(self, lines):
        self.mappings = []
        self.parse(lines)

    def map(self, in_seed):
        for mapping in self.mappings:
            in_seed = mapping.map(in_seed)
        return in_seed

    def parse(self, lines):
        self.seeds = list(map(int, re.findall('\d+', lines[0])))
        src, dest = False, False
        values = []
        for line in lines[2:]:
            if not line:
                # Add current mapping
                self.mappings.append(
                    GardenMapping(src, dest, values)
                )
                values = []
            elif 'map:' in line:
                src = line.split('-to-')[0]
                dest = line.split('-to-')[1].split(' ')[0]
            else:
                values.append(list(map(int, re.findall(r'\d+', line))))
        # Add the last one
        self.mappings.append(
            GardenMapping(src, dest, values)
        )

    def map_ranges(self, ranges):
        for mapping in self.mappings:
            ranges = mapping.map_ranges(ranges)
        return ranges


with open('day05.in') as f:
    lines = [line.strip() for line in f.readlines()]


production = FoodProduction(lines)

p1 = 1e50
print(production.seeds)
for seed in production.seeds:
    output = production.map(seed)
    p1 = min(p1, output)

print('Part 1: ', p1)

p2 = 1e50
test = []
for range_start, range_length in zip(production.seeds[0::2], production.seeds[1::2]):
    # Re-calculate the range per layer
    ranges = production.map_ranges([(range_start, range_start+range_length)])
    test.append(ranges)
    # Keep track of the current minimum point
    p2 = min(p2, min(ranges)[0])

print('Part 2: ', p2)
# 336906655 WRONG
# 450342284 WRONG
# 100230635 too high
# 100170635 too high
# 100165635 too high
# 140161318 too high
