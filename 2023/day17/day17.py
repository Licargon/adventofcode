# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from heapq import heappop, heappush
import sys

with open('day17.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = [[int(ch) for ch in row] for row in lines]


def solve(part2=False):
    # heat, row, col, dr, dc, amount of moves in this dir
    pq = [(0, 0, 0, 0, 0, 0)]
    MAX_SAME = 10 if part2 else 3
    MIN_BEFORE_TURN = 4 if part2 else 1
    SEEN = set()
    while pq:
        heat, row, col, dr, dc, moves = heappop(pq)

        if (row, col, dr, dc, moves) in SEEN:
            continue

        SEEN.add((row, col, dr, dc, moves))

        if row == len(G)-1 and col == len(G[0])-1:
            if part2:
                if moves >= MIN_BEFORE_TURN:
                    return heat
            else:
                return heat

        # IF we are moving and we are below the treshold for moves-in-same-direction: Move ahead
        if (dr, dc) != (0, 0) and moves < MAX_SAME:
            nr = row + dr
            nc = col + dc
            if 0 <= nr < len(G) and 0 <= nc < len(G[0]):
                nheat = heat + G[nr][nc]
                heappush(pq, (nheat, nr, nc, dr, dc, moves+1))

        # If we are standing still OR we moved enough to be able to turn: Check for turns
        if moves >= MIN_BEFORE_TURN or (dr, dc) == (0, 0):
            for ndr, ndc in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                if (ndr, ndc) == (-dr, -dc) or (ndr, ndc) == (dr, dc): #Cant go back immediatly, and skip 'same direction' as covered above
                    continue
                nr = row + ndr
                nc = col + ndc
                if 0 <= nr < len(G) and 0 <= nc < len(G[0]):
                    nheat = heat + G[nr][nc]
                    heappush(pq, (nheat, nr, nc, ndr, ndc, 1))


print(solve())
print(solve(True))