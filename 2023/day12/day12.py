# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import functools
import sys

with open('day12.in') as f:
    lines = [line.strip() for line in f.readlines()]


@functools.lru_cache(maxsize=100000000)
def solve(spring, groups):
    # print('In recursive call: ', spring, groups)

    if not spring:
        # If all groups are parsed, good config
        if not groups:
            return 1
        else:
            return 0

    if not groups:
        # If we still have springs left, faulty config
        if '#' in spring:
            return 0
        else:
            return 1

    good = 0

    # Skip dots
    if spring[0] == '.':
        good += solve(spring[1:], groups)

    # ? -> Can be . or #. Treat as . first
    if spring[0] == '?':
        good += solve(spring[1:], groups)

    if spring[0] in ['#', '?']:
        # Treat # and ? both as # and check for validity
        # Conditions:
        # - substring of length groups doesnt contain a period
        # - group can fit within the rest of the spring
        # - group cover the entire spring OR we end up at a not-# after this group
        if '.' not in spring[:groups[0]] and groups[0] <= len(spring) and (len(spring) == groups[0] or spring[groups[0]] != '#'):
            good += solve(spring[groups[0]+1:], groups[1:])
    return good


p1 = 0
p2 = 0
for line in lines:
    spring, groups = line.split(' ')
    groups = tuple(int(g) for g in groups.split(','))
    p1 += solve(spring, tuple(groups))

    spring = '?'.join([spring]*5)
    groups = groups * 5
    p2 += solve(spring, groups)

print('Part 1: ', p1)
print('Part 2: ', p2)

