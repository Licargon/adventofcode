# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys, os

clear = lambda: os.system('cls')

with open('day16.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = [[ch for ch in row] for row in lines]

def show(G):
    for row in G:
        print(''.join(row))

def solve(G, sr, sc, sdy, sdx):
    def check_and_add(beams, row, col, diry, dirx):
        row += diry
        col += dirx
        if 0 <= row < len(G) and 0 <= col < len(G[0]):
            beams.append((row, col, diry, dirx))

    # row, col, diry, dirx
    beams = deque([(sr, sc, sdy, sdx)])
    energized = set()
    SEEN = set((sr, sc, sdy, sdx))
    while beams:
        # print(beams)
        row, col, diry, dirx = beams.popleft()
        if (row, col, diry, dirx) in SEEN:
            continue
        energized.add((row, col))
        SEEN.add((row, col, diry, dirx))

        ch = G[row][col]
        if ch == '.':
            check_and_add(beams, row, col, diry, dirx)
        elif ch == '-' and (diry, dirx) in [(0, -1), (0, 1)]:
            check_and_add(beams, row, col, diry, dirx)
        elif ch == '|' and (diry, dirx) in [(-1, 0), (1, 0)]:
            check_and_add(beams, row, col, diry, dirx)
        elif ch == '|' and (diry, dirx) in [(0, 1), (0, -1)]:
            # Split
            check_and_add(beams, row, col, 1, 0)
            check_and_add(beams, row, col, -1, 0)
        elif ch == '-' and (diry, dirx) in [(-1, 0), (1, 0)]:
            # Split
            check_and_add(beams, row, col, 0, -1)
            check_and_add(beams, row, col, 0, 1)
        elif ch == '/':
            if (diry, dirx) == (0, 1):
                diry, dirx = -1, 0
                check_and_add(beams, row, col, diry, dirx)
            elif (diry, dirx) == (0, -1):
                diry, dirx = 1, 0
                check_and_add(beams, row, col, diry, dirx)
            elif (diry, dirx) == (-1, 0):
                diry, dirx = 0, 1
                check_and_add(beams, row, col, diry, dirx)
            elif (diry, dirx) == (1, 0):
                diry, dirx = 0, -1
                check_and_add(beams, row, col, diry, dirx)
        elif ch == '\\':
            if (diry, dirx) == (0, 1):
                diry, dirx = 1, 0
                check_and_add(beams, row, col, diry, dirx)
            elif (diry, dirx) == (0, -1):
                diry, dirx = -1, 0
                check_and_add(beams, row, col, diry, dirx)
            elif (diry, dirx) == (-1, 0):
                diry, dirx = 0, -1
                check_and_add(beams, row, col, diry, dirx)
            elif (diry, dirx) == (1, 0):
                diry, dirx = 0, 1
                check_and_add(beams, row, col, diry, dirx)

    return len(energized)


print(solve(G, 0, 0, 0, 1))


p2 = 0
for row in range(len(G)):
    p2 = max(p2, solve(G, row, 0, 0, 1))
    p2 = max(p2, solve(G, row, len(G[0])-1, 0, -1))

for col in range(len(G[0])):
    p2 = max(p2, solve(G, 0, col, 1, 0))
    p2 = max(p2, solve(G, len(G)-1, col, -1, 0))

print(p2)

