
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

with open('day09.in') as f:
    lines = [list(map(int, line.strip().split())) for line in f.readlines()]

def get_differences(line, part2=False):
    differences = [line]
    cur = line
    while True:
        new_diff = []
        for i1, i2, in zip(cur, cur[1:]):
            new_diff.append(i2-i1)
        differences.append(new_diff)
        if all(i == 0 for i in new_diff):
            break
        cur = new_diff


    for idx in reversed(list(range(len(differences)))):
        diff = differences[idx]
        if part2:
            if idx == len(differences)-1:
                diff.insert(0, 0)
            else:
                diff.insert(0, diff[0] - differences[idx+1][0])
        else:
            if idx == len(differences)-1:
                diff.append(0)
            else:
                diff.append(diff[-1] + differences[idx+1][-1])
    return differences


differences = [get_differences(line, False) for line in lines]
p1 = 0
for diff in differences:
    p1 += diff[0][-1]
print('Part 1: ', p1)

differences = [get_differences(line, True) for line in lines]
p2 = 0
for diff in differences:
    p2 += diff[0][0]
print('Part 2: ', p2)