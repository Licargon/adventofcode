# -*- coding: utf-8 -*-

from collections import defaultdict, deque, Counter
from functools import cmp_to_key, partial

import re
import sys

with open('day07.in') as f:
    lines = [line.strip() for line in f.readlines()]


CARDS = 'AKQJT98765432'


def get_hand_rank(hand, part2=False):
    # Return 'rank' of hand. Lower = better. Starts at 0 for 5 of a kind

    ctr = Counter(hand)

    if part2 and 'J' in hand:
        j_count = ctr['J']
        ctr.pop('J')
        if len(ctr) == 0:
            ctr['A'] = 5
        else:
            ctr[ctr.most_common(1)[0][0]] += j_count
    most_common = ctr.most_common(20)
    if most_common[0][1] == 5:
        rank = 0
    elif most_common[0][1] == 4:
        rank = 1
    elif len(most_common) > 1 and most_common[0][1] == 3 and most_common[1][1] == 2: # Full house
        rank = 2
    elif most_common[0][1] == 3:
        rank = 3
    elif len(most_common) > 1 and most_common[0][1] == 2 and most_common[1][1] == 2: # 2 pairs
        rank = 4
    elif most_common[0][1] == 2:
        rank = 5
    else:
        rank = 6

    return rank


def compare_hands(part2=False, first=False, second=False):
    hand1 = first[0]
    hand2 = second[0]
    rank1 = get_hand_rank(hand1, part2)
    rank2 = get_hand_rank(hand2, part2)

    if rank1 == rank2:
        # Compare high card
        for idx in range(len(hand1)):
            value1, value2 = CARDS.index(hand1[idx]), CARDS.index(hand2[idx])
            # To balance this, J cards are now the weakest individual cards, weaker even than 2
            # Rescale J's to be bad
            if part2:
                if hand1[idx] == 'J':
                    value1 = 200
                if hand2[idx] == 'J':
                    value2 = 200

            if value1 < value2:
                return -1
            elif value2 < value1:
                return 1
    else:
        if rank1 < rank2:
            return -1
        else:
            return 1


data = []
for line in lines:
    data.append(line.split(' '))

data = sorted(data, key=cmp_to_key(partial(compare_hands, False)))

part1 = 0
for idx, (hand, bid) in enumerate(data[::-1], start=1):
    part1 += idx * int(bid)
print('Part 1: ', part1)

data = sorted(data, key=cmp_to_key(partial(compare_hands, True)))

part2 = 0
for idx, (hand, bid) in enumerate(data[::-1], start=1):
    part2 += idx * int(bid)
print('Part 2: ', part2)
