# -*- coding: utf-8 -*-

with open('day14.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = [[ch for ch in line] for line in lines]


def move_rocks(G):
    rocks_moved = True
    while rocks_moved:
        # Move all O rocks 1 row up if there is no O or # above them
        rocks_moved = False
        for r, row in enumerate(G):
            for c, ch in enumerate(row):
                if ch == 'O':
                    if r == 0 or G[r-1][c] in ('O', '#'):
                        continue
                    G[r][c] = '.'
                    G[r-1][c] = 'O'
                    rocks_moved = True

p1 = 0
move_rocks(G.copy())
for i in range(len(G)):
    p1 += G[i].count('O') * (len(G)-i)

print(p1)

MEMO = {}
G = [[ch for ch in line] for line in lines]

def transpose(grid):
    # Rotate 2d grid clockwise
    result = list(zip(*grid[::-1]))
    return [list(item) for item in result]


def print_grid(G):
    for line in G:
        print(''.join(line))
    print()

i = 0
while i < 1000000000-1:
    move_rocks(G)
    for rot in range(3):
        G = transpose(G)
        move_rocks(G)
    # Transpose once more to get the grid oriented back 'North'
    G = transpose(G)

    key = ''.join([''.join(row) for row in G])
    if key not in MEMO:
        MEMO[key] = i
        i += 1
    else:
        # We encountered this state before, skip ahead
        d = i-MEMO[key]
        times_to_skip = (1000000000 - i) // d
        i += times_to_skip * d
        # Clear memo so we dont keep stalling here, just finish out the iterations
        MEMO.clear()

p2 = 0
for i in range(len(G)):
    p2 += G[i].count('O') * (len(G)-i)
print('Part 2:', p2)
