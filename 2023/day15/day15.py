# -*- coding: utf-8 -*-

from collections import defaultdict, Counter
from pprint import pprint

with open('day15.in') as f:
    lines = [line.strip() for line in f.readlines()]

map = defaultdict(list)

def f(s):
    val = 0
    for ch in s:
        val += ord(ch)
        val *= 17
        val %= 256
    return val

p1 = 0
p2 = 0
for s in lines[0].split(','):
    p1 += f(s)
    if s[-1] == '-':
        box = f(s[:-1])
        map[box] = [[k, v] for [k, v] in map[box] if s[:-1] != k]
    else:
        label = s[:-2]
        focal_length = int(s.split('=')[-1])
        box = f(label)
        if label not in [k for (k, v) in map[box]]:
            map[box].append([label, focal_length])
        else:
            for i in range(len(map[box])):
                if map[box][i][0] == label:
                    map[box][i][1] = focal_length

print('Part 1:', p1)

ctr = Counter()
for box_number, lenses in map.items():
    for idx, (lens, focal_strength) in enumerate(lenses):
        ctr[lens] += (box_number + 1) * (idx + 1) * focal_strength

print('Part 2: ', sum(ctr.values()))


