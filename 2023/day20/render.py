# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import graphviz
import sys

with open('day20.in') as f:
    lines = [line.strip() for line in f.readlines()]

graph = []
nodes = set()
for line in lines:
    line = line.replace('%', '').replace('&', '')
    src = line.split(' -> ')[0].strip()
    nodes.add(src)
    graph.append(line)

for g in graph:
    print(g)

dot = graphviz.Digraph()
for n in nodes:
    dot.node(n, n)

for g in graph:
    src, dst = g.split(' -> ')
    if ',' in dst:
        dst = dst.split(', ')
    else:
        dst = [dst]
    for d in dst:
        dot.edge(src, d)

dot.render('graph', view=True)