# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import math
import sys

with open('day20.in') as f:
    lines = [line.strip() for line in f.readlines()]


class Module(object):
    def __init__(self, name, type, outputs):
        self.name = name
        self.type = type
        self.outputs = outputs

        if self.type == '%':
            # Flipflop
            self.memory = 'off'
        else:
            # Conjuction
            self.memory = {}

    def __repr__(self):
        return f'{self.name} {self.type} {self.outputs} {self.memory}'

config = {}
START = []

for line in lines:
    incoming, outputs = line.split(' -> ')
    if incoming == 'broadcaster':
        START = outputs.split(', ')
    else:
        type = incoming[0]
        name = incoming[1:]
        outputs = outputs.split(', ')

        config[name] = Module(name, type, outputs)

for name, module in config.items():
    for output in module.outputs:
        if output in config and config[output].type == '&':
            config[output].memory[module.name] = 'low'

print('Broadcaster start: ', START)
print('Modules:')
print(config)

low = 0
high = 0

# Figure out what goes into rx, only 1 node in our input
into_rx = [module.name for module in config.values() if 'rx' in module.outputs]
assert len(into_rx) == 1
# Figure out what goes into what goes into rx, 4 nodes here
into_into_rx = [module.name for module in config.values() if into_rx[0] in module.outputs]
# Start recording when we get to these nodes and record how long it took to get 4 cycles
seen = {module: 0 for module in into_into_rx}
cycles = defaultdict(int)

presses = 0
while True:
    presses += 1
    low += 1
    # Broadcaster sends low to every output module
    Q = deque([('broadcaster', output, 'low') for output in START])

    # Keep going through signals untill it ends
    while Q:
        src, dst, signal = Q.popleft()

        if signal == 'low':
            low += 1
        elif signal == 'high':
            high += 1
        else:
            assert False, 'Signal received: {}'.format(signal)
        # print(src, dst, signal)
        # print('BEFORE: ', config)

        if dst == into_rx[0] and signal == 'high':
            seen[src] += 1
            # Record when this happened so we can find a cycle for this into_into_rx thing
            cycles[src] = presses

            # If we saw all (4, in this case) incoming values being 'high', we should have 4 cycle numbers where all of
            # them where high at one point, giving us the 4 distinct cycles
            if all(v > 0 for v in seen.values()):
                p2 = 1
                for c in cycles.values():
                    p2 = math.lcm(p2, c)
                print('Part 2: ', p2)
                exit(0)

        # Unknown dst, like 'output'
        if dst not in config:
            continue

        dst_module = config[dst]

        if dst_module.type == '%':
            if signal == 'low': # High gets ignored
                dst_module.memory = 'on' if dst_module.memory == 'off' else 'off'
                pulse = 'high' if dst_module.memory == 'on' else 'low'
                for output in dst_module.outputs:
                    Q.append((dst, output, pulse))
        else:
            dst_module.memory[src] = signal
            # dst_module.memory[src] = 'high' if dst_module.memory[src] == 'low' else 'low'
            if all(v == 'high' for v in dst_module.memory.values()):
                pulse = 'low'
            else:
                pulse = 'high'

            for output in dst_module.outputs:
                Q.append((dst, output, pulse))

        # print('AFTER: ',config)
        # print()
        # assert True

# print('Part 1: ', high, low, high * low)

