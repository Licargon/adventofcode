# -*- coding: utf-8 -*-

with open('day01.in') as f:
    lines = [line.strip() for line in f.readlines()]

part1 = 0
for line in lines:
    first_digit = False
    last_digit = False
    for ch in line:
        if ch.isdigit() and not first_digit:
            first_digit = ch
        elif ch.isdigit():
            last_digit = ch
    if not last_digit:
        last_digit = first_digit
    calibration_value = int(first_digit + last_digit)
    part1 += calibration_value

print('Part 1:', part1)

part2 = 0
for line in lines:
    idx_first_digit = 10e12
    first_digit = False

    idx_last_digit = -1
    last_digit = False

    mapping = {
        'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5, 'six': 6, 'seven': 7, 'eight': 8,
        'nine': 9
    }

    # Find the leftmost and rightmost index for all numbers and number representations
    for digit in ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine',
                  1, 2, 3, 4, 5, 6, 7, 8, 9]:
        try:
            idx = line.index(str(digit))
            if idx < idx_first_digit:
                idx_first_digit = idx
                if digit in mapping:
                    first_digit = mapping[digit]
                else:
                    first_digit = digit
        except Exception as e:
            pass

        try:
            idx = line.rindex(str(digit))
            if idx > idx_last_digit:
                idx_last_digit = idx
                if digit in mapping:
                    last_digit = mapping[digit]
                else:
                    last_digit = digit
        except Exception as e:
            pass

    if not last_digit:
        last_digit = first_digit

    calibration_value = int(str(first_digit) + str(last_digit))
    part2 += calibration_value

print('Part 2:', part2)

