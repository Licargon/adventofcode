# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import numpy as np
import sys

with open('day21.in') as f:
    lines = [line.strip() for line in f.readlines()]

START = False
G = {}
for r, row in enumerate(lines):
    for c, col in enumerate(row):
        G[(r, c)] = col


def increase_grid(G, factor):
    # G = dict (row, col) -> value
    # Take a grid G, increases it by a factor 'factor'
    # Turns G into
    # G G G
    # G G G
    # G G G
    # Only works for odd factors ATM

    assert factor % 2 == 1
    min_y = min(y for y, x in G)
    max_y = max(y for y, x in G)
    min_x = min(x for y, x in G)
    max_x = max(x for y, x in G)

    H = max_y - min_y + 1
    W = max_x - min_x + 1
    new_G = [['X' for _ in range(H*factor)] for _ in range(W*factor)]
    for fy in range(factor):
        for fx in range(factor):
            for (r, c), ch in G.items():
                new_G[fy * H + r][fx * W + c] = ch

    # Convert back to dict
    ret = {}
    for r, row in enumerate(new_G):
        for c, col in enumerate(row):
            ret[(r, c)] = col
    return ret


def solve(G, MAX_STEPS, start):
    VALID = set()
    SEEN = set()
    # State = (r, c), steps taken
    Q = deque([(start, 0)])
    # Honestly no idea why I need this but the code didnt run properly for odd MAX_STEPS sizes. Thanks reddit.
    parity = MAX_STEPS % 2

    while Q:
        (row, col), steps = Q.popleft()

        if (row, col) in SEEN:
            continue

        SEEN.add((row, col))

        # Valid end pos if we reached the total steps OR even, meaning we can circle back to this point
        if steps % 2 == parity or steps == MAX_STEPS:
            VALID.add((row, col))

        if steps == MAX_STEPS:
            continue

        for dr, dc in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            nr = row + dr
            nc = col + dc
            if (nr, nc) in G and G[(nr, nc)] != '#' and (nr, nc) not in SEEN:
                Q.append(((nr, nc), steps+1))
    return len(VALID)

# Fetch size of grid, which is a square
min_y = min(y for y, x in G)
max_y = max(y for y, x in G)
size = max_y - min_y + 1

print('Part 1: ', solve(G, 64, (size//2, size//2)))

# Verify on sample
# for steps in [6,10,50,100,500,1000,5000]:
#     print('{} - {}'.format(steps, solve(G, steps, START, H, W, True)))

G = increase_grid(G, 9)
min_y = min(y for y, x in G)
max_y = max(y for y, x in G)
min_x = min(x for y, x in G)
max_x = max(x for y, x in G)
size = max_y-min_y+1
START = (size//2, size//2)
# print(solve(G, 65, (size//2, size//2)))
# print(solve(G, 196, (size//2, size//2)))
# print(solve(G, 327, (size//2, size//2)))

points = []
# Get first few points for quadratic fitting, as APPARENTLY this problem perfectly fits a quadratic function
for i in range(3):
    points.append((i, solve(G, 65+i*131, START)))

def fit_quadratic(points):
    coefficients = np.polyfit(*zip(*points), 2)
    return coefficients

# Cast to int because somehow python messes this up
# Use round because somehow np gives '14483.' as the first coeff which int() translates to 14482... What the actual fuck
coeffs = list(map(round, fit_quadratic(points)))

# 26501365 = 202300 * 131 + 65, 131 = size of grid, so we need to check this point as we started at 65
x = 202300
print('Part 2: ', x * x * coeffs[0] + x * coeffs[1] + coeffs[2])
# 592723929260582 CORRECT according to other peoples code
# 592683003970582


# 1030294053330622 too high
# 438951497053966 too low
# 702332682395576 too high
# 592760100213095 wrong
# 610812789095225 wrong (.... even thought its reddit code)
# 610158187362102 # WRONG

