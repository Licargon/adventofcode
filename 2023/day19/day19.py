# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import re
import sys

with open('day19.sample') as f:
    content = f.read()

workflows, parts = content.split('\n\n')

for workflow in workflows.split('\n'):
    print(workflow)
    name, rest = workflow.split('{')

    rest = rest.replace(':', ' and ')
    rest = rest.replace(',', '_() or ')
    rest = rest.replace('}', '_()')

    workflow = '{}_ = lambda:'.format(name) + rest
    print(workflow)
    exec(workflow)

p1 = 0
# Final functions
# +1 / -1 to fix stuff like 'R_() or A_()' in the evaluation, which still returns A_() if R_() returns 0

A_ = lambda: 1 + x + m + a + s
R_ = lambda: 1

for part in parts.split('\n'):
    print(part)
    part = part.replace(',', ';')
    part = part.replace('{', '')
    part = part.replace('}', '; p1 += in_()-1')
    print(part)
    exec(part)
print('Part 1:', p1)


possible_ranges = {char: [0, 4000] for char in 'xmas'}

# Find all possible ranges in the workflows, by looking for stuff like x > value.
# If it's smaller than, we need to subtract 1 because we are looking at it from the END of the interval, so we need to make it non-inclusive on the right
for char, operator, value in re.findall(r'(\w+)(<|>)(\d+)', workflows):
    value = int(value)
    if operator == '<':
        value -= 1
    possible_ranges[char].append(value)

def range_sizes(ranges):
    # Return the range sizes and range ENDS for all ranges
    ret = []
    ranges = sorted(ranges)
    for start, end in zip(ranges, ranges[1:]):
        ret.append((end, end-start))
    return ret

X_ranges = range_sizes(possible_ranges['x'])
M_ranges = range_sizes(possible_ranges['m'])
A_ranges = range_sizes(possible_ranges['a'])
S_ranges = range_sizes(possible_ranges['s'])

part2 = 0
# Bruteforce
# For all ranges we have, figure out which combinations get accepted and add the range sizes
# Init as x, m, a, s because of our lambdas in memory
for x, x_size in X_ranges:
    for m, m_size in M_ranges:
        for a, a_size in A_ranges:
            for s, s_size in S_ranges:
                # IF this passes, it means the ENTIRE range passes, and we can add the product to our results
                # Once again, in_()-1 to make sure lambda works nicely
                part2 += x_size * m_size * a_size * s_size * bool(in_()-1)

print('Part 2: ', part2)
