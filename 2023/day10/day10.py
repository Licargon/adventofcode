# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys


class Grid():

    def __init__(self, filename):
        with open(filename) as f:
            lines = [line.strip() for line in f.readlines()]

        self.G = {}
        self.START = False
        self.SYMBOLS = set()
        for ir, row in enumerate(lines):
            for ic, col in enumerate(row):
                self.G[(ir, ic)] = col
                if col == 'S':
                    self.START = (ir, ic)
                if col in '|-LJF7':
                    self.SYMBOLS.add(col)

        self.H = len(lines)
        self.W = len(lines[0])

    def print(self):
        for R in range(self.H):
            row = []
            for C in range(self.W):
                row.append(self.G[R, C])
            print(''.join(row))
        print(''.join(row))

        print('Starting position:', self.START)

    def get_dir(self, cur_dir, symbol):
        DIR_MAPPING = {
            ((-1, 0), '|'): (-1, 0),
            ((1, 0), '|'): (1, 0),
            ((0, -1), '-'): (0, -1),
            ((0, 1), '-'): (0, 1),

            ((-1, 0), 'L'): (0, 1),
            ((0, -1), 'L'): (-1, 0),
            ((0, 1), 'J'): (-1, 0),
            ((1, 0), 'J'): (0, -1),
            ((0, 1), '7'): (-1, 0),
            ((-1, 0), '7'): (0, -1),
            ((-1, 0), 'F'): (0, 1),
            ((0, -1), 'F'): (1, 0),
        }
        return DIR_MAPPING[(cur_dir, symbol)]

    def valid_directions(self, symbol):
        VALID_DIRECTIONS = {
            '|': [(-1, 0), (1, 0)],
            '-': [(0, -1), (0, 1)],
            'L': [(-1, 0), (0, 1)],
            'J': [(0, -1), (-1, 0)],
            '7': [(0, -1,), (1, 0)],
            'F': [(1, 0), (0, 1)],
        }
        assert symbol in VALID_DIRECTIONS, f'Invalid symbol {symbol}'

        return VALID_DIRECTIONS[symbol]

    def part1(self):
        """
        Find out the max distance from starting block S
        """
        # State = pos, SEEN
        Q = deque([])
        Q.append((self.START, set()))

        DIST_MAP = defaultdict(int)

        while Q:
            (row, col), SEEN = Q.pop()

            if (row, col) in SEEN:
                continue

            SEEN.add((row, col))
            symbol = self.G[(row, col)]
            # print(row, col, symbol)
            # print(DIST_MAP)
            for dir in self.valid_directions(symbol):
                new_row = row + dir[0]
                new_col = col + dir[1]
                if (new_row, new_col) in SEEN:
                    continue
                # If we already encounted this space, update min distance to either the currect, or the previous + 1
                if (new_row, new_col) in DIST_MAP:
                    DIST_MAP[(new_row, new_col)] = min(DIST_MAP[(new_row, new_col)],
                                                       DIST_MAP[(row, col)] + 1)
                else:
                    # Else: Just add it
                    DIST_MAP[(new_row, new_col)] = DIST_MAP[(row, col)] + 1
                Q.append(((new_row, new_col), SEEN.copy()))

        return DIST_MAP

    def expand(self):
        """
        Rewrite the grid to replace every square by a 3x3 square
        """

        height = self.H * 3
        width = self.W * 3
        new_grid = {}

        for row in range(self.H):
            for col in range(self.W):
                # Default: Fill with periods
                for rr in range(3):
                    for cc in range(3):
                        new_grid[(3*row + rr, 3*col+cc)] = '.'

                if self.G[(row, col)] == '|':
                    new_grid[(3*row, 3*col+1)] = '#'
                    new_grid[(3*row + 1, 3*col+1)] = '#'
                    new_grid[(3*row + 2, 3*col+1)] = '#'
                elif self.G[(row, col)] == '-':
                    new_grid[(3 * row + 1, 3 * col)] = '#'
                    new_grid[(3 * row + 1, 3 * col + 1)] = '#'
                    new_grid[(3 * row + 1 ,3 * col + 2)] = '#'
                elif self.G[(row, col)] == 'F':
                    new_grid[(3 * row + 1, 3 * col + 1)] = '#'
                    new_grid[(3 * row + 2, 3 * col + 1)] = '#'
                    new_grid[(3 * row + 1, 3 * col + 2)] = '#'
                elif self.G[(row, col)] == '7':
                    new_grid[(3 * row + 1, 3 * col + 1)] = '#'
                    new_grid[(3 * row + 1, 3 * col)] = '#'
                    new_grid[(3 * row + 2, 3 * col + 1)] = '#'
                elif self.G[(row, col)] == 'J':
                    new_grid[(3 * row + 1, 3 * col + 1)] = '#'
                    new_grid[(3 * row, 3 * col + 1)] = '#'
                    new_grid[(3 * row + 1, 3 * col)] = '#'
                elif self.G[(row, col)] == 'L':
                    new_grid[(3 * row + 1, 3 * col + 1)] = '#'
                    new_grid[(3 * row, 3 * col + 1)] = '#'
                    new_grid[(3 * row + 1, 3 * col+2)] = '#'
        self.H = height
        self.W = width
        self.G = new_grid

    def replace_S(self, new_symbol):
        for coords, symbol in self.G.items():
            if symbol == 'S':
                self.G[coords] = new_symbol

    def floodfill(self):
        Q = deque([])
        SEEN = set()

        # Start from all edges
        for c in range(self.W):
            Q.append((0, c))
            SEEN.add((0, c))
            Q.append((self.H-1, c))
            SEEN.add((self.H-1, c))

        for r in range(self.H):
            Q.append((r, 0))
            SEEN.add((r, 0))
            Q.append((r, self.W-1))
            SEEN.add((r, self.W-1))

        while Q:
            row, col = Q.pop()

            # Up down left right
            DY = [1, -1, 0, 0]
            DX = [0, 0, -1, 1]
            for d in range(4):
                new_row = row+DY[d]
                new_col = col+DX[d]
                if (new_row, new_col) in SEEN:
                    # ALready visited
                    continue

                if not(0 <= new_row < self.H and 0 <= new_col < self.W):
                    # Out of bounds
                    continue

                if self.G[(new_row, new_col)] == '#':
                    # Blocked
                    continue

                SEEN.add((new_row, new_col))
                Q.append((new_row, new_col))
        return SEEN


from time import time
start = time()
G = Grid('day10.in')
# G.print()
# Set the S pipe to the correct piece
G.replace_S('J')
dist_map = G.part1()
print('Part 1: ', max(list(dist_map.values())))
print(time()-start, ' seconds')

start = time()
G = Grid('day10.in')
# Set the S pipe to the correct piece
G.replace_S('J')

# Keep track of the original grid and dimensions for later
orig_G = G.G.copy()
orig_height = G.H
orig_width = G.W
# Expand grid 1x1 -> 3x3
G.expand()

FILLED = G.floodfill()

# DEBUG
# for (row, col) in SEEN:
#     G.G[(row, col)] = 'O'
#
# G.print()

p2 = 0
# Loop over original squares, if ANY of the expanded squares is touched by floodfill, disregard it
for r in range(orig_height):
    for c in range(orig_width):
        seen = False
        for dy in range(3):
            for dx in range(3):
                if (3*r + dy, 3*c + dx) in FILLED:
                    seen = True
        if not seen:
            p2 += 1

print('Part 2: ', p2)
# print('Part 2: ', (G.H*G.W - len(SEEN)))
print(time()-start, ' seconds')


