# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

with open('day02.in') as f:
    lines = [line.strip() for line in f.readlines()]

def extract_cubes(cubes):
    blue = 0
    red = 0
    green = 0
    for cubes in cubes.split(','):
        cubes = cubes.strip()
        if 'blue' in cubes:
            blue = int(cubes.split(' ')[0])
        elif 'red' in cubes:
            red = int(cubes.split(' ')[0])
        elif 'green' in cubes:
            green = int(cubes.split(' ')[0])
    return blue, green, red

def is_valid(game):
    cube_draws = game.split(';')
    for cubes in cube_draws:
        blue, green, red = extract_cubes(cubes.strip())
        if red > 12 or green > 13 or blue > 14:
            return False
    return True

part1 = 0
part2 = 0

for line in lines:
    line = line.replace('Game ', '')
    number, game = line.split(':')
    if is_valid(game):
        part1 += int(number)
    blue_max = green_max = red_max = 0
    for cube_draw in game.split(';'):
        blue, green, red = extract_cubes(cube_draw.strip())
        blue_max = max(blue, blue_max)
        green_max = max(green, green_max)
        red_max = max(red, red_max)
    part2 += blue_max * green_max * red_max


print('Part 1: ', part1)
print('Part 2: ', part2)

