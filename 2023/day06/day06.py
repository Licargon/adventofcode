# -*- coding: utf-8 -*-

# Sample
times = [7, 15, 30]
dists = [9, 40, 200]
# Input
times = [40, 81, 77, 72]
dists = [219, 1012, 1365, 1089]


p1 = 1

for i in range(len(times)):
    total_time = times[i]
    race_wins = 0
    for time_held in range(total_time):
        result = time_held * (total_time - time_held)
        if result > dists[i]:
            race_wins += 1
    p1 *= race_wins

print('Part 1: ', p1)

race_wins = 0
# Sample
time = 71530
dist = 940200
# # Input
time = 40817772
dist = 219101213651089
for time_held in range(time):
    result = time_held * (time-time_held)
    if result > dist:
        race_wins += 1
print('Part 2: ', race_wins)

