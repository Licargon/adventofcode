# -*- coding: utf-8 -*-

import math

with open('day08.in') as f:
    lines = [line.strip() for line in f.readlines()]

INSTRUCTIONS = lines[0]
MAP = {}
START_POS = []
END_POS = []
for line in lines[2:]:
    src, dst_tuple = line.split(' = ')
    left = dst_tuple[1:4]
    right = dst_tuple[6:9]
    MAP[src] = {
        'L': left,
        'R': right,
    }

    # For part 2
    if src[-1] == 'A':
        START_POS.append(src)
    if src[-1] == 'Z':
        END_POS.append(src)


def steps_to_end(current, part2=False):
    ctr = 0
    while current != 'ZZZ':
        ins = INSTRUCTIONS[ctr % len(INSTRUCTIONS)]
        current = MAP[current][ins]
        ctr += 1
    return ctr


def steps_to_end_p2(current, end_positions):
    ctr = 0
    while current not in end_positions:
        ins = INSTRUCTIONS[ctr % len(INSTRUCTIONS)]
        current = MAP[current][ins]
        ctr += 1
    return ctr


p1 = steps_to_end('AAA', MAP)
print('Part 1: ', p1)


p2 = 1
times = []
for pos in START_POS:
    time = steps_to_end_p2(pos, END_POS)
    p2 = math.lcm(p2, time)
    times.append(time)

print('Part 2: ', p2)
