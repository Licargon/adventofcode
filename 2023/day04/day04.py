# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import re
import sys

with open('day04.in') as f:
    lines = [line.strip() for line in f.readlines()]

part1 = 0
copies = defaultdict(lambda: 0)
for idx, line in enumerate(lines, start=1):
    game, line = line.split(': ')
    winning, ours = line.split(' | ')
    winning_numbers = set(map(int, re.findall(r'\d+', winning)))
    ours = set(map(int, re.findall(r'\d+', ours)))

    matches = winning_numbers & ours
    if matches:
        score = 2**(len(matches)-1)
        part1 += score

        # Add all copies we get + copies for any duplicates we already have
        for i in range(1, len(matches)+1):
            copies[idx+1+i] += 1 + copies[idx+1]

print('Part 1: ', part1)
print('Part 2: ', len(lines) + sum(copies.values()))
