
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import re
import sys

with open('day03.in') as f:
    lines = [line.strip() for line in f.readlines()]


def is_symbol(grid, row, col):
    # If the coords are within the grid and it's not a number or a period -> its a symbol
    if 0 <= row < len(grid) and 0 <= col < len(grid[0]):
        if (not grid[row][col].isdigit() and grid[row][col] != '.'):
            return grid[row][col]
    return False


def get_symbols_around(grid, ri, ci1, ci2):
    # Return (coordinates, symbol) for all symbols located around the number at (ri, ci1:ci2)
    symbols = []
    # Row above
    for idx in range(ci1-1, ci2+2):
        symbol = is_symbol(grid, ri-1, idx)
        if symbol:
            symbols.append(((ri-1, idx), symbol))
    # Cell left, cell right
    symbol = is_symbol(grid, ri, ci1-1)
    if symbol:
        symbols.append(((ri, ci1-1), symbol))
    symbol = is_symbol(grid, ri, ci2 + 1)
    if symbol:
        symbols.append(((ri, ci2+1), symbol))

    # Row below
    for idx in range(ci1-1, ci2+2):
        symbol = is_symbol(grid, ri+1, idx)
        if symbol:
            symbols.append(((ri+1, idx), symbol))
    return symbols

p1 = 0
gears = defaultdict(list)
for ri, line in enumerate(lines):
    for match in re.finditer(r'\d+', line):
        start, end = match.start(0), match.end(0)
        symbols = get_symbols_around(lines, ri, start, end-1)
        if symbols:
            p1 += int(match[0])
            for coords, symbol in symbols:
                if symbol == '*':
                    gears[coords].append(int(match[0]))
print('Part 1: ', p1)

p2 = 0
for coord, numbers in gears.items():
    if len(numbers) == 2:
        p2 += numbers[0] * numbers[1]
# 76007295 too low
print('Part 2:', p2)
