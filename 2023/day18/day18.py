# -*- coding: utf-8 -*-

points = [(0, 0)]
dirs = {"U": (-1, 0), "D": (1, 0), "L": (0, -1), "R": (0, 1)}

b = 0

with open('day18.in') as f:
    lines = [line.strip() for line in f.readlines()]

# Shoelace theorem + Picks combined.
for line in lines:
    dir, n, hex = line.split(' ')
    dr, dc = dirs[dir]
    n = int(n)
    b += n
    r, c = points[-1]
    points.append((points[-1][0] + dr * n, points[-1][1] + dc * n))

A = 0
for idx, p in enumerate(points):
    A += p[0] * (points[idx-1][1] - points[(idx+1)%len(points)][1])
A //= 2
i = A - b // 2 + 1


print(i + b)

points = [(0, 0)]
b = 0
for line in lines:
    _, _, hex = line.split(' ')

    n = int(hex[2:-2], 16)
    dir = 'RDLU'[int(hex[-2])]

    dr, dc = dirs[dir]
    b += n
    r, c = points[-1]
    points.append((points[-1][0] + dr * n, points[-1][1] + dc * n))

A = 0
for idx, p in enumerate(points):
    A += p[0] * (points[idx - 1][1] - points[(idx + 1) % len(points)][1])
A //= 2
i = A - b // 2 + 1

print(i + b)
