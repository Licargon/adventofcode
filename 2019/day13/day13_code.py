# -*- coding: utf-8 -*-

from collections import deque
from intcode import Program

with open('day13.in') as f:
    program = [int(element) for element in f.read().split(',')]

currently_painted = set()
total_painted = set()

P = Program(deque([]), program[:])

BLOCKS = set()

while True:
    output1 = P.run()
    if output1 == None:
        break
    output2 = P.run()
    output3 = P.run()

    if output3 == 2:
        BLOCKS.add((output2, output1))

print('Part 1: ', len(BLOCKS), ' blocks')

program[0] = 2
P = Program(deque([0]), program[:])

score = None
# Figure out the game state
BLOCKS = set()
PADDLE_POS = None
BALL_POS = None
scanning_blocks = True

while True:
    output1 = P.run()
    if output1 == None:
        # Done. Output the last given score
        print('Part 2: ', score)
        break
    output2 = P.run()
    output3 = P.run()
    if (output1, output2) == (-1, 0):
        scanning_blocks = False
        score = output3
    elif output3 == 2 and scanning_blocks:
        BLOCKS.add((output2, output1))
    elif output3 == 3:
        PADDLE_POS = (output2, output1)
    elif output3 == 4:
        BALL_POS = (output2, output1)

    # Move the paddle towards the ball
    # By sheer luck this actually works and gives the correct solution
    if BALL_POS and PADDLE_POS:
        if BALL_POS[1] == PADDLE_POS[1]:
            P.Q = deque([0])
        elif BALL_POS[1] < PADDLE_POS[1]:
            P.Q = deque([-1])
        else:
            P.Q = deque([1])

    # print('Game state: ')
    # print('Ball: ', BALL_POS)
    # print('Paddle: ', PADDLE_POS)
    # print('Blocks: ', BLOCKS)
    # print('Score: ', score)



