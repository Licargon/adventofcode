# -*- coding: utf-8 -*-

ADD = 1
MULT = 2
INPUT = 3
OUTPUT = 4
JUMP_IF_TRUE = 5
JUMP_IF_FALSE = 6
LESS_THAN = 7
EQUALS = 8
QUIT = 99

POSITIONAL = 0
IMMEDIATE = 1

# Add, mult: ip = ip+4
# input, output: ip = ip+2
ip_increases = {
    ADD: 4,
    MULT: 4,
    INPUT: 2,
    OUTPUT: 2,
    JUMP_IF_TRUE: 3,
    JUMP_IF_FALSE: 3,
    LESS_THAN: 4,
    EQUALS: 4,
}

parameter_count = {
    ADD: 2,
    MULT: 2,
    INPUT: False,
    OUTPUT: False,
    JUMP_IF_TRUE: 2,
    JUMP_IF_FALSE: 2,
    LESS_THAN: 2,
    EQUALS: 2,
}


class UnknownOpCodeException(Exception):
    pass


def parse_parameter_modes_from_opcode(opcode):
    # Everything but the last 2 digits of the opcode, reversed because the sepcification said so
    new_opcode = int(opcode[-2:])
    parameters = opcode[:-2]
    # Pad with zeroes if needed
    if parameter_count[new_opcode] and len(parameters) < parameter_count[new_opcode]:
        parameters = '0' * (parameter_count[new_opcode] - len(parameters)) + parameters
    return new_opcode, list(reversed([{
        'mode': int(parameter)
    } for parameter in parameters]))


def process_opcode(opcode, instruction_pointer, intcode_program):
    use_parameter_modes = False
    if len(str(opcode)) > 2:
        opcode, parameter_modes = parse_parameter_modes_from_opcode(str(opcode))
        use_parameter_modes = True

    if opcode == ADD:
        if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
            arg1 = intcode_program[instruction_pointer + 1]
        else:
            arg1_pos = intcode_program[instruction_pointer + 1]
            arg1 = intcode_program[arg1_pos]
            
        if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
            arg2 = intcode_program[instruction_pointer + 2]
        else:
            arg2_pos = intcode_program[instruction_pointer + 2]
            arg2 = intcode_program[arg2_pos]

        result_pos = intcode_program[instruction_pointer + 3]
        intcode_program[result_pos] = int(arg1 + arg2)
        return instruction_pointer + ip_increases[opcode]
    elif opcode == MULT:
        if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
            arg1 = intcode_program[instruction_pointer + 1]
        else:
            arg1_pos = intcode_program[instruction_pointer + 1]
            arg1 = intcode_program[arg1_pos]

        if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
            arg2 = intcode_program[instruction_pointer + 2]
        else:
            arg2_pos = intcode_program[instruction_pointer + 2]
            arg2 = intcode_program[arg2_pos]

        result_pos = intcode_program[instruction_pointer + 3]
        intcode_program[result_pos] = int(arg1 * arg2)
        return instruction_pointer + ip_increases[opcode]
    elif opcode == INPUT:
        user_input = input('Please input a value: ')
        store_pos = intcode_program[instruction_pointer + 1]
        intcode_program[store_pos] = int(user_input)
        return instruction_pointer + ip_increases[opcode]
    elif opcode == OUTPUT:
        if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
            output = intcode_program[instruction_pointer + 1]
        else:
            fetch_pos = intcode_program[instruction_pointer + 1]
            output = intcode_program[fetch_pos]
        print('Output: ', output)
        return instruction_pointer + ip_increases[opcode]
    elif opcode == JUMP_IF_TRUE:
        if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
            arg1 = intcode_program[instruction_pointer + 1]
        else:
            arg1_pos = intcode_program[instruction_pointer + 1]
            arg1 = intcode_program[arg1_pos]

        if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
            arg2 = intcode_program[instruction_pointer + 2]
        else:
            arg2_pos = intcode_program[instruction_pointer + 2]
            arg2 = intcode_program[arg2_pos]

        # Arg 1 True: ip = arg2
        if bool(arg1):
            return arg2
        else:
            return instruction_pointer + ip_increases[JUMP_IF_TRUE]
    elif opcode == JUMP_IF_FALSE:
        if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
            arg1 = intcode_program[instruction_pointer + 1]
        else:
            arg1_pos = intcode_program[instruction_pointer + 1]
            arg1 = intcode_program[arg1_pos]

        if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
            arg2 = intcode_program[instruction_pointer + 2]
        else:
            arg2_pos = intcode_program[instruction_pointer + 2]
            arg2 = intcode_program[arg2_pos]

        # Arg 1 False: ip = arg2
        if not bool(arg1):
            return arg2
        else:
            return instruction_pointer + ip_increases[opcode]
    elif opcode == LESS_THAN:
        if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
            arg1 = intcode_program[instruction_pointer + 1]
        else:
            arg1_pos = intcode_program[instruction_pointer + 1]
            arg1 = intcode_program[arg1_pos]

        if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
            arg2 = intcode_program[instruction_pointer + 2]
        else:
            arg2_pos = intcode_program[instruction_pointer + 2]
            arg2 = intcode_program[arg2_pos]

        result_pos = intcode_program[instruction_pointer + 3]
        if arg1 < arg2:
            intcode_program[result_pos] = 1
        else:
            intcode_program[result_pos] = 0
        return instruction_pointer + ip_increases[opcode]
    elif opcode == EQUALS:
        if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
            arg1 = intcode_program[instruction_pointer + 1]
        else:
            arg1_pos = intcode_program[instruction_pointer + 1]
            arg1 = intcode_program[arg1_pos]

        if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
            arg2 = intcode_program[instruction_pointer + 2]
        else:
            arg2_pos = intcode_program[instruction_pointer + 2]
            arg2 = intcode_program[arg2_pos]

        result_pos = intcode_program[instruction_pointer + 3]
        if arg1 == arg2:
            intcode_program[result_pos] = 1
        else:
            intcode_program[result_pos] = 0
        return instruction_pointer + ip_increases[opcode]
    else:
        raise UnknownOpCodeException('Unknown opcode: {}'.format(opcode))

def run_intcode_program(intcode_program):
    instruction_pointer = 0
    while True:
        opcode = intcode_program[instruction_pointer]
        if opcode == QUIT:
            print(intcode_program)
            return intcode_program[0]
        else:
            new_ip = process_opcode(opcode, instruction_pointer, intcode_program)
            instruction_pointer = new_ip


def main():
    with open('input.txt') as f:
        original_intcode_program = [int(element) for element in f.read().split(',')]

    # Part 1
    intcode_program = original_intcode_program[:]

    result = run_intcode_program(intcode_program)
    print('Answer: ', result)


if __name__ == "__main__":
    main()
