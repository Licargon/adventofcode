# -*- coding: utf-8 -*-

from collections import deque
from intcode import Program

with open('day19.in') as f:
    program = [int(element) for element in f.read().split(',')]


def check(x, y):
    P = Program(deque([x, y]), program[:])
    return bool(P.run())


affected = 0
for y in range(50):
    for x in range(50):
        affected += int(check(x, y))

print('Part 1: ', affected)


# Startvalue was found after a lot of bruteforcing, put it near the actual value for debug reasons
for y in range(1095, 100000):
    """Strategy:
    1. Per y coordinate, look for the left and right edge of the tractor beam
    2. (right_edge, y) gives us the top right tile of the square
    3. (right_edge-99, y-99) gives us the bottom left tile of the 100x100 square, given top right edge
    4. We test if that is in the beam. If so, we got our solution
    """
    x = 100
    while not check(x, y):
        x += 1
    left_edge = x
    x += 90
    while check(x, y):
        x += 1
    right_edge = x-1
    if right_edge - left_edge < 99:
        continue

    check_left_edge = right_edge-99
    c = check(right_edge-99, y+99)
    if c:
        print('Part 2: ', (check_left_edge*10000+y))
        break
