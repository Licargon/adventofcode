import copy

ADD = 1
MULT = 2
INPUT = 3
OUTPUT = 4
JUMP_IF_TRUE = 5
JUMP_IF_FALSE = 6
LESS_THAN = 7
EQUALS = 8
ADJUST_RELATIVE_BASE = 9
QUIT = 99

POSITIONAL = 0
IMMEDIATE = 1
RELATIVE = 2

# Add, mult: ip = ip+4
# input, output: ip = ip+2
ip_increases = {
    ADD: 4,
    MULT: 4,
    INPUT: 2,
    OUTPUT: 2,
    JUMP_IF_TRUE: 3,
    JUMP_IF_FALSE: 3,
    LESS_THAN: 4,
    EQUALS: 4,
    ADJUST_RELATIVE_BASE: 2,
}

parameter_count = {
    ADD: 2,
    MULT: 2,
    INPUT: False,
    OUTPUT: False,
    JUMP_IF_TRUE: 2,
    JUMP_IF_FALSE: 2,
    LESS_THAN: 2,
    EQUALS: 2,
    ADJUST_RELATIVE_BASE: 1,
}


class UnknownOpCodeException(Exception):
    pass


class Program(object):
    def __init__(self, input_Q, program, ip=0, relative_base=0):
        self.Q = input_Q
        self.P = program
        self.ip = ip
        self.relative_base = relative_base

    def copy(self):
        return Program(copy.copy(self.Q), self.P[:], self.ip, self.relative_base)

    def parse_parameter_modes_from_opcode(self, opcode):
        # Everything but the last 2 digits of the opcode, reversed because the sepcification said so
        new_opcode = int(opcode[-2:])
        parameters = opcode[:-2]
        # Pad with zeroes if needed
        try:
            if parameter_count[new_opcode] and len(parameters) < parameter_count[new_opcode]:
                parameters = '0' * (parameter_count[new_opcode] - len(parameters)) + parameters
        except Exception as e:
            print('Key: ', e)
            raise

        parameter_modes = list(reversed([{
            'mode': int(parameter)
        } for parameter in parameters]))

        ret = dict()
        for idx, parameter_mode in enumerate(parameter_modes):
            ret[idx] = parameter_mode

        return new_opcode, ret

    def addr(self, ip_offset, parameter_mode=False):
        # Calculates an address to fetch anything based on the given parameter mode
        # ip_offset will always be 1, 2, 3 when fetching parameters for an opcode
        # Parameter_mode = False is treated as POSITIONAL
        if not parameter_mode or parameter_mode['mode'] == POSITIONAL:
            addr = self.P[self.ip + ip_offset]
        elif parameter_mode['mode'] == RELATIVE:
            addr = self.relative_base + self.P[self.ip + ip_offset]
        elif parameter_mode['mode'] == IMMEDIATE:
            addr = self.ip + ip_offset
        if addr > len(self.P)-1:
            to_add = addr - len(self.P) + 1
            for i in range(to_add):
                self.P.append(0)
        return addr

    def run(self):
        while True:
            cmd = opcode = self.P[self.ip]
            # print('Processing cmd: ', cmd, self.ip)
            parameter_modes = dict()
            if len(str(cmd)) > 2:
                opcode, parameter_modes = self.parse_parameter_modes_from_opcode(str(opcode))

            if opcode == ADD:
                arg1 = self.P[self.addr(1, parameter_modes.get(0))]
                arg2 = self.P[self.addr(2, parameter_modes.get(1))]

                result_pos = self.addr(3, parameter_modes.get(2))
                self.P[result_pos] = int(arg1 + arg2)
                self.ip += ip_increases[opcode]
            elif opcode == MULT:
                arg1 = self.P[self.addr(1, parameter_modes.get(0))]
                arg2 = self.P[self.addr(2, parameter_modes.get(1))]
                result_pos = self.addr(3, parameter_modes.get(2))

                self.P[result_pos] = int(arg1 * arg2)
                self.ip += ip_increases[opcode]
            elif opcode == INPUT:
                val = self.Q.popleft()
                # store_pos = self.P[self.addr(self.ip + 1)]
                store_pos = self.addr(1, parameter_modes.get(0))
                self.P[store_pos] = int(val)
                self.ip += ip_increases[opcode]
            elif opcode == OUTPUT:
                output = self.P[self.addr(1, parameter_modes.get(0))]
                self.ip += ip_increases[opcode]
                return output
            elif opcode == JUMP_IF_TRUE:
                arg1 = self.P[self.addr(1, parameter_modes.get(0))]
                arg2 = self.P[self.addr(2, parameter_modes.get(1))]

                # Arg 1 True: ip = arg2
                if bool(arg1):
                    self.ip = arg2
                else:
                    self.ip += ip_increases[JUMP_IF_TRUE]
            elif opcode == JUMP_IF_FALSE:
                arg1 = self.P[self.addr(1, parameter_modes.get(0))]
                arg2 = self.P[self.addr(2, parameter_modes.get(1))]

                # Arg 1 False: ip = arg2
                if not bool(arg1):
                    self.ip = arg2
                else:
                    self.ip += ip_increases[opcode]
            elif opcode == LESS_THAN:
                arg1 = self.P[self.addr(1, parameter_modes.get(0))]
                arg2 = self.P[self.addr(2, parameter_modes.get(1))]
                result_pos = self.addr(3, parameter_modes.get(2))
                if arg1 < arg2:
                    self.P[result_pos] = 1
                else:
                    self.P[result_pos] = 0
                self.ip += ip_increases[opcode]
            elif opcode == EQUALS:
                arg1 = self.P[self.addr(1, parameter_modes.get(0))]
                arg2 = self.P[self.addr(2, parameter_modes.get(1))]
                result_pos = self.addr(3, parameter_modes.get(2))
                if arg1 == arg2:
                    self.P[result_pos] = 1
                else:
                    self.P[result_pos] = 0
                self.ip += ip_increases[opcode]
            elif opcode == ADJUST_RELATIVE_BASE:
                arg1 = self.P[self.addr(1, parameter_modes.get(0))]
                self.relative_base += arg1
                self.ip += ip_increases[opcode]
            elif opcode == QUIT:
                return None
            else:
                raise UnknownOpCodeException('Unknown opcode: {}'.format(opcode))
