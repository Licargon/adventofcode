
def main():
    input1 = 402328
    input2 = 864247

    # Part 1
    matches = 0
    for number in range(input1, input2):
        number = str(number)
        # There has to be a count >= 2 of any number
        if list(number) == sorted(number) and max(map(number.count, number)) >= 2:
            matches += 1

    print('Part 1: ', matches, ' matches')

    # Part 2
    matches = 0
    for number in range(input1, input2):
        number = str(number)
        # There has to be a count = 2 for any number, rest is irrelevant #VagueShit
        if list(number) == sorted(number) and 2 in map(number.count, number):
            matches += 1

    print('Part 2: ', matches, ' matches')


if __name__ == '__main__':
    main()