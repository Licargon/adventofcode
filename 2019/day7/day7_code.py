# -*- coding: utf-8 -*-

from itertools import permutations
from collections import deque

# import day7
from day7.intcode import Program


def main():
    with open('input.txt') as f:
        program = [int(element) for element in f.read().split(',')]

    ans = 0
    for permutation in permutations([0,1,2,3,4]):
        PROGRAMS = [Program(deque([permutation[i]]), program) for i in range(len(permutation))]
        PROGRAMS[0].Q.append(0)
        score = 0
        done = False
        while not done:
            for i in range(len(permutation)):
                value = PROGRAMS[i].run()
                if value == None:
                    if score > ans:
                        ans = score
                    done = True
                    break
                elif i == len(permutation)-1:
                    # print('Got output: ', value)
                    score = value
                PROGRAMS[(i+1)%len(PROGRAMS)].Q.append(value)

    print('Part 1: Max output: ', ans)

    ans = 0
    for permutation in permutations([5,6,7,8,9]):
        PROGRAMS = [Program(deque([permutation[i]]), program) for i in range(len(permutation))]
        PROGRAMS[0].Q.append(0)
        score = 0
        done = False
        while not done:
            for i in range(len(permutation)):
                value = PROGRAMS[i].run()
                # If a machine quits and it is the last machine, we should have our answer
                if value == None and i == len(permutation)-1:
                    if score > ans:
                        ans = score
                    done = True
                    break
                elif i == len(permutation) - 1:
                    # Loop input from the end back into the first machine
                    score = value
                    PROGRAMS[0].Q.append(value)
                else:
                    PROGRAMS[(i + 1) % len(PROGRAMS)].Q.append(value)

    print('Part 2: Max output: ', ans)

if __name__ == "__main__":
    main()
