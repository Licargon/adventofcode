ADD = 1
MULT = 2
INPUT = 3
OUTPUT = 4
JUMP_IF_TRUE = 5
JUMP_IF_FALSE = 6
LESS_THAN = 7
EQUALS = 8
QUIT = 99

POSITIONAL = 0
IMMEDIATE = 1

# Add, mult: ip = ip+4
# input, output: ip = ip+2
ip_increases = {
    ADD: 4,
    MULT: 4,
    INPUT: 2,
    OUTPUT: 2,
    JUMP_IF_TRUE: 3,
    JUMP_IF_FALSE: 3,
    LESS_THAN: 4,
    EQUALS: 4,
}

parameter_count = {
    ADD: 2,
    MULT: 2,
    INPUT: False,
    OUTPUT: False,
    JUMP_IF_TRUE: 2,
    JUMP_IF_FALSE: 2,
    LESS_THAN: 2,
    EQUALS: 2,
}


class UnknownOpCodeException(Exception):
    pass


class Program(object):
    def __init__(self, input_Q, program, ip=0):
        self.Q = input_Q
        self.P = program
        self.ip = ip

    def parse_parameter_modes_from_opcode(self, opcode):
        # Everything but the last 2 digits of the opcode, reversed because the sepcification said so
        new_opcode = int(opcode[-2:])
        parameters = opcode[:-2]
        # Pad with zeroes if needed
        if parameter_count[new_opcode] and len(parameters) < parameter_count[new_opcode]:
            parameters = '0' * (parameter_count[new_opcode] - len(parameters)) + parameters
        return new_opcode, list(reversed([{
            'mode': int(parameter)
        } for parameter in parameters]))

    def run(self):
        while True:
            cmd = opcode = self.P[self.ip]
            use_parameter_modes = False
            if len(str(cmd)) > 2:
                opcode, parameter_modes = self.parse_parameter_modes_from_opcode(str(opcode))
                use_parameter_modes = True

            if opcode == ADD:
                if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
                    arg1 = self.P[self.ip + 1]
                else:
                    arg1_pos = self.P[self.ip + 1]
                    arg1 = self.P[arg1_pos]

                if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
                    arg2 = self.P[self.ip + 2]
                else:
                    arg2_pos = self.P[self.ip + 2]
                    arg2 = self.P[arg2_pos]

                result_pos = self.P[self.ip + 3]
                self.P[result_pos] = int(arg1 + arg2)
                self.ip += ip_increases[opcode]
            elif opcode == MULT:
                if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
                    arg1 = self.P[self.ip + 1]
                else:
                    arg1_pos = self.P[self.ip + 1]
                    arg1 = self.P[arg1_pos]

                if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
                    arg2 = self.P[self.ip + 2]
                else:
                    arg2_pos = self.P[self.ip + 2]
                    arg2 = self.P[arg2_pos]

                result_pos = self.P[self.ip + 3]
                self.P[result_pos] = int(arg1 * arg2)
                self.ip += ip_increases[opcode]
            elif opcode == INPUT:
                input = self.Q.popleft()
                store_pos = self.P[self.ip + 1]
                self.P[store_pos] = int(input)
                self.ip += ip_increases[opcode]
            elif opcode == OUTPUT:
                if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
                    output = self.P[self.ip + 1]
                else:
                    fetch_pos = self.P[self.ip + 1]
                    output = self.P[fetch_pos]
                self.ip += ip_increases[opcode]
                return output
            elif opcode == JUMP_IF_TRUE:
                if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
                    arg1 = self.P[self.ip + 1]
                else:
                    arg1_pos = self.P[self.ip + 1]
                    arg1 = self.P[arg1_pos]

                if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
                    arg2 = self.P[self.ip + 2]
                else:
                    arg2_pos = self.P[self.ip + 2]
                    arg2 = self.P[arg2_pos]

                # Arg 1 True: ip = arg2
                if bool(arg1):
                    self.ip = arg2
                else:
                    self.ip += ip_increases[JUMP_IF_TRUE]
            elif opcode == JUMP_IF_FALSE:
                if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
                    arg1 = self.P[self.ip + 1]
                else:
                    arg1_pos = self.P[self.ip + 1]
                    arg1 = self.P[arg1_pos]

                if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
                    arg2 = self.P[self.ip + 2]
                else:
                    arg2_pos = self.P[self.ip + 2]
                    arg2 = self.P[arg2_pos]

                # Arg 1 False: ip = arg2
                if not bool(arg1):
                    self.ip = arg2
                else:
                    self.ip += ip_increases[opcode]
            elif opcode == LESS_THAN:
                if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
                    arg1 = self.P[self.ip + 1]
                else:
                    arg1_pos = self.P[self.ip + 1]
                    arg1 = self.P[arg1_pos]

                if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
                    arg2 = self.P[self.ip + 2]
                else:
                    arg2_pos = self.P[self.ip + 2]
                    arg2 = self.P[arg2_pos]

                result_pos = self.P[self.ip + 3]
                if arg1 < arg2:
                    self.P[result_pos] = 1
                else:
                    self.P[result_pos] = 0
                self.ip += ip_increases[opcode]
            elif opcode == EQUALS:
                if use_parameter_modes and parameter_modes[0]['mode'] == IMMEDIATE:
                    arg1 = self.P[self.ip + 1]
                else:
                    arg1_pos = self.P[self.ip + 1]
                    arg1 = self.P[arg1_pos]

                if use_parameter_modes and parameter_modes[1]['mode'] == IMMEDIATE:
                    arg2 = self.P[self.ip + 2]
                else:
                    arg2_pos = self.P[self.ip + 2]
                    arg2 = self.P[arg2_pos]

                result_pos = self.P[self.ip + 3]
                if arg1 == arg2:
                    self.P[result_pos] = 1
                else:
                    self.P[result_pos] = 0
                self.ip += ip_increases[opcode]
            elif opcode == QUIT:
                return None
            else:
                raise UnknownOpCodeException('Unknown opcode: {}'.format(opcode))
