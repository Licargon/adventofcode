# -*- coding: utf-8 -*-

from itertools import permutations
from collections import deque
from intcode import Program

with open('day9.in') as f:
    program = [int(element) for element in f.read().split(',')]

P = Program(deque([1]), program[:])
print('Part 1: ', P.run())

P = Program(deque([2]), program[:])
print('Part 2: ', P.run())