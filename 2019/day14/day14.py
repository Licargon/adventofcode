# -*- coding: utf-8 -*-

from collections import deque, defaultdict
import math


def get_recipes(filename):
    with open(filename) as f:
        input_lines = f.readlines()

    RECIPES = dict()
    for line in input_lines:
        line = line.rstrip('\n')
        IN_LIST, OUT = line.split(' => ')
        OUT_amt, OUT_ingr = OUT.split(' ')
        # Should be unique
        assert OUT_ingr not in RECIPES
        ingredients_list = []
        for IN in IN_LIST.split(', '):
            IN_AMT, IN_INGR = IN.split(' ')
            ingredients_list.append({'name': IN_INGR, 'amt': int(IN_AMT)})
        RECIPES[OUT_ingr] = {'amt': int(OUT_amt), 'ingredients': ingredients_list}

    return RECIPES


def calculate_ore_for_fuel(fuel_amt, RECIPES):
    needs = deque([{'name': 'FUEL', 'amt': fuel_amt}])
    have = defaultdict(int)
    ORE_COUNTER = 0
    while needs:
        need = needs.popleft()
        need_amt = need['amt']

        if need['name'] in have and have[need['name']] >= need['amt']:
            # Case 1: Should not produce anything as we already have more than enough of it
            have[need['name']] -= need['amt']
            continue
        elif need['name'] in have and have[need['name']] < need['amt']:
            # Case 2: We have SOME, but not enough
            need_amt -= have[need['name']]
            del have[need['name']]

        # We dont have enough pre-existing, so we need to produce more
        recipe = RECIPES[need['name']]
        if len(recipe['ingredients']) == 1 and recipe['ingredients'][0]['name'] == 'ORE':
            # Case 1: We only need to mine ore

            # Figure out how many times we have to intake ore to get the right amount
            times = math.ceil(need_amt / recipe['amt'])
            ORE_COUNTER += times * recipe['ingredients'][0]['amt']
            # Store the leftovers in 'have'
            if need_amt < times*recipe['amt']:
                have[need['name']] += times * recipe['amt'] - need_amt
        else:
            scale = math.ceil(need_amt / recipe['amt'])

            # Store leftovers in 'have'
            have[need['name']] += scale * recipe['amt'] - need_amt

            for ingredient in recipe['ingredients']:
                present = False
                for needed in needs:
                    if needed['name'] == ingredient['name']:
                        present = True
                        needed['amt'] += scale * ingredient['amt']
                        break

                if not present:
                    needs.append({
                        'name': ingredient['name'],
                        'amt': scale * ingredient['amt'],
                    })

    return ORE_COUNTER


recipes = get_recipes('day14.in')
print('Part 1: ', calculate_ore_for_fuel(1, recipes))

# Tests
# results = [31, 165, 13312, 180697, 2210736]
# for idx, filename in enumerate([
#     'day14.sample',
#                  'day14.sample2',
#                  'day14.sample3',
#                  'day14.sample4',
#                  'day14.sample5',
#                  ]):
#     result = calculate_ore_for_fuel(1, get_recipes(filename))
#     print('{} = {}?'.format(result, results[idx]), (result == results[idx]))
lo = 0
hi = 1e12
while lo < hi:
    fuel = (lo+hi)//2
    ores = calculate_ore_for_fuel(fuel, recipes)

    # print(f'{ores} ores for {fuel} fuel')
    # print(f'[{lo},{hi}]')
    if ores > 1e12:
        hi = lo + (hi - lo) // 2
    else:
        lo = lo + (hi - lo) // 2 + 1

print('Part 2: ', int(fuel))
