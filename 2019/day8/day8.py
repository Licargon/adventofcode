# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw

def main():

    # Part 1
    with open('input.txt') as f:
        img_data = f.read()
    WIDTH = 25
    HEIGHT = 6

    pixels_per_layer = WIDTH * HEIGHT
    layers = []
    for i in range(int(len(img_data)/pixels_per_layer)):
        layers.append(img_data[i*pixels_per_layer:i*pixels_per_layer + pixels_per_layer])
    sorted_layers = sorted(layers, key=lambda l: l.count('0'))

    print('Answer = ', sorted_layers[0].count('1') * sorted_layers[0].count('2'))

    # Part 2
    TRANSPARENT = 2
    final_result = [0] * pixels_per_layer
    for i in range(pixels_per_layer):
        for layer in layers:
            if int(layer[i]) == TRANSPARENT:
                continue
            final_result[i] = layer[i]
            break

    # Draw the pixels to get the decoded message
    im = Image.new('RGB', (25, 6), (0,0,0))
    for col in range(WIDTH):
        for row in range(HEIGHT):
            xy = (col, row)
            pixel = int(final_result[WIDTH*row+col])
            if pixel == 0:
                color = (0, 0, 0)
            else:
                color = (255, 255, 255)
            im.putpixel(xy, color)
    im.show()



if __name__ == '__main__':
    main()