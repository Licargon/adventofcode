# -*- coding: utf-8 -*-

from collections import deque
from intcode import Program

with open('day17.in') as f:
    program = [int(element) for element in f.read().split(',')]

P = Program(deque([]), program[:])
image = []
row = ''
# Get the initial image from the program
while True:
    out = P.run()
    if out == None:
        break
    if out == 10:
        if row:
            image.append(row)
            row = ''
    else:
        row += chr(out)
        if out == 94:
            start_pos = (len(image), len(row)-1)

# Look for intersections (They look like crosses on the image)
part1 = 0
for ri, row in enumerate(image):
    for ci, col in enumerate(row):
        if col == '#':
            is_intersection = True
            for dx, dy in [(0, -1), (0, 1), (1, 0), (-1, 0)]:
                new_x = ci + dx
                new_y = ri + dy
                if 0 <= new_x <= len(row)-1 and 0 <= new_y <= len(image)-1:
                    try:
                        if image[new_y][new_x] != '#':
                            is_intersection = False
                    except IndexError as ie:
                        print(ri, ci)
                        raise
            if is_intersection:
                part1 += ci * ri

print('Part 1: ', part1)


def inbounds(x, y, image):
    if 0 <= y <= len(image)-1 and 0 <= x <= len(image[y])-1:
        return True
    else:
        return False


# We are currently facing up
dir = [1, 0]
y, x = start_pos
commands = []

# Find the path the robot has to run by following all girders to the ends, then checking if we need to go left or right
# Rinse and repeat untill we reach the end of the path.
image[y] = image[y].replace('^', '#')
while True:
    left = [dir[1], -dir[0]]
    left_y, left_x = y-left[0], x+left[1]
    right = [-dir[1], dir[0]]
    right_y, right_x = y-right[0], x+right[1]

    old_dir = dir
    dir = None
    if inbounds(left_x, left_y, image) and image[left_y][left_x] == '#':
        if image[left_y][left_x] == '#':
            dir = left
            command = 'L'
    if inbounds(right_x, right_y, image) and image[right_y][right_x] == '#':
        if image[right_y][right_x] == '#':
            dir = right
            command = 'R'

    if dir is None:
        # Reached the end
        break
    length = 1
    while inbounds(x + length * dir[1], y - length * dir[0], image):
        ty = y - length * dir[0]
        tx = x + length * dir[1]
        if image[y - length * dir[0]][x + length * dir[1]] == '#':
            length += 1
        else:
            break
    length += -1
    command += str(length)
    commands.append(command)
    y = y - length * dir[0]
    x = x + length * dir[1]

# Manually did this part using a text editor
A = 'L,10,L,8,R,8,L,8,R,6'
B = 'R,6,R,8,R,8'
C = 'R,6,R,6,L,8,L,10'

program[0] = 2
input_strings = ['A,A,B,C,B,C,B,C,B,A',
                 A,
                 B,
                 C,
                 'n 10']
inputs = []
# Convert everything to ASCII using ord()
# and put '10' after every command for a newline
for s in input_strings:
    converted_i = [ord(str(c)) for c in s]
    converted_i.append(10)
    inputs.extend(converted_i)
P = Program(deque(inputs), program[:])
image = []
row = ''
prev_out = None
# Dumb code to just show the last output.
while True:
    new_out = P.run()
    if new_out == None:
        break
    else:
        prev_out = new_out
print('Part 2: ', prev_out)
