# -*- coding: utf-8 -*-

with open('day16.sample') as f:
    line = list(map(int, f.readlines()[0].rstrip('\n')))


def tick(line):
    base_pattern = [0, 1, 0, -1]
    new_line = []
    for i in range(len(line)):
        new_pattern = []
        for p in base_pattern:
            new_pattern.extend([base_pattern[p]] * (i+1))
        ans = 0
        for idx, number in enumerate(line):
            ans += number*new_pattern[(1+idx) % len(new_pattern)]
        new_line.append(abs(ans) % 10)
    return new_line

for i in range(100):
    line = tick(line)
    # print(line)

print('Part 1: ', ''.join(map(str, line[0:8])))

