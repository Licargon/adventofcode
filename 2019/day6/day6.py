from collections import defaultdict


class CelestialBody(object):
    def __init__(self, name):
        self.orbiters = []
        self.parent = None
        self.name = name

    def add(self, orbitee):
        self.orbiters.append(orbitee)
        orbitee.parent = self

    @property
    def orbits(self):
        return len(self.orbiters) + sum([orbiter.orbits for orbiter in self.orbiters])


def figure_out_YOU_SAN_path(YOU_path, SAN_path):
    for node in YOU_path:
        for node_2 in SAN_path:
            if node == node_2:
                print('Found it')
                return YOU_path.index(node) + SAN_path.index(node_2)

def main():
    # Part 1
    orbit_map = defaultdict(CelestialBody)
    orbit_map = dict()
    with open('input.txt') as f:
        for line in f.readlines():
            orbitee, orbiter = line.strip('\n').split(')')
            if orbitee not in orbit_map:
                orbit_map[orbitee] = CelestialBody(orbitee)
            if orbiter not in orbit_map:
                orbit_map[orbiter] = CelestialBody(orbiter)
            orbit_map[orbitee].add(orbit_map[orbiter])

    orbit_count = sum([body.orbits for body in orbit_map.values()])
    print('Total: ', orbit_count)

    # Part 2
    YOU = orbit_map['YOU']
    SAN = orbit_map['SAN']
    # We trace 2 paths from each body to COM
    YOU_path = []
    SAN_path = []

    while YOU.parent != None:
        YOU_path.append(YOU.parent.name)
        YOU = YOU.parent

    while SAN.parent != None:
        SAN_path.append(SAN.parent.name)
        SAN = SAN.parent

    # We now find the first body that is both in YOU_path and SAN_path and we add their indices to figure out the path
    # from YOU to SAN
    print(figure_out_YOU_SAN_path(YOU_path, SAN_path))


if __name__ == '__main__':
    main()