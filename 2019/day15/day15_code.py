# -*- coding: utf-8 -*-

from collections import deque
from intcode import Program


# Can't work unless we replicate a program's state..
def dfs(P, SEEN, pos, current_path, map_values):
    SEEN.add(pos)
    # 3 west
    # 4 east
    # 1 north
    # 2 south

    for dir, dy, dx in [(3, 0, -1), (4, 0, 1), (1, 1, 0), (2, -1, 0)]:
        new_y = pos[0] + dy
        new_x = pos[1] + dx
        if (new_y, new_x) in SEEN:
            # Skip if we've already been there
            continue
        else:
            new_path = current_path[:]
            new_path.append((new_y, new_x))
            P2 = P.copy()
            # Set the dir we are going in.
            P2.Q.append(dir)
            out = P2.run()
            if out == 2:
                # We found the oxygen system
                map_values.add((new_y, new_x, 'O'))
                return current_path
            elif out == 1:
                map_values.add((new_y, new_x, '.'))
                # We actually moved
                ret = dfs(P2, SEEN, (new_y, new_x), new_path, map_values)
                if ret:
                    return ret
            elif out == 0:
                map_values.add((new_y, new_x, '#'))

def bfs(program):
    P = Program(deque([]), program[:])
    Q = deque([(0, 0, P)])
    SEEN = set()
    map_values = set()
    while len(Q):
        cur_y, cur_x, P2 = Q.popleft()
        # print(len(map_values), len(Q))

        for dir, dy, dx in [(3, 0, -1), (4, 0, 1), (1, 1, 0), (2, -1, 0)]:
            new_y = cur_y + dy
            new_x = cur_x + dx
            # if cur_y == -18:
            #     print('Pause')
            if (new_y, new_x) not in SEEN:
                P3 = P2.copy()
                # Set the dir we are going in.
                P3.Q.append(dir)
                out = P3.run()
                if out == 2:
                    # We found the oxygen system
                    map_values.add((new_y, new_x, 'O'))
                    # if (new_y, new_x) not in SEEN:
                    Q.append((new_y, new_x, P3))
                elif out == 1:
                    # We actually moved to an empty square
                    map_values.add((new_y, new_x, '.'))
                    # Add it to the Queue so we can explore further
                    # if (new_y, new_x) not in SEEN:
                    Q.append((new_y, new_x, P3))
                elif out == 0:
                    map_values.add((new_y, new_x, '#'))
        SEEN.add((cur_y, cur_x))
    return map_values


with open('day15.in') as f:
    program = [int(element) for element in f.read().split(',')]

currently_painted = set()
total_painted = set()

P = Program(deque([]), program[:])
map_values = set()
result = dfs(P, set(), (0, 0), [(0, 0)], map_values)
if result:
    print(result)
    print('Part 1: ', len(result))
    print('Total map squares found: ', len(map_values))

# Approach 2: Get the entire maze using 'follow-the-right-wall'
# Our DFS approach from part 1 doesnt discover the entire maze.
map_values = bfs(program[:])
print('BFS map squares found: ', len(map_values))

# Draw out the field.
min_y = min([mv[0] for mv in map_values])
max_y = max([mv[0] for mv in map_values])
min_x = min([mv[1] for mv in map_values])
max_x = max([mv[1] for mv in map_values])
print(min_y, max_y, min_x, max_x)
field = [['.' for _ in range(max_x+abs(min_x)+1)] for _ in range(max_y+abs(min_y)+1)]
# print('Width: ', max_x+abs(min_x))
# print('Height: ', max_y+abs(min_y))
for y, x, symbol in map_values:
    field[y+abs(min_y)][x+abs(min_x)] = symbol
    if symbol == 'O':
        start_y = y+abs(min_y)
        start_x = x+abs(min_x)

steps = 0
while True:
    new_field = [[col for col in row] for row in field]
    for ri, row in enumerate(field):
        for ci, col in enumerate(row):
            # Check the 4 directions
            if col == '#':
                continue
            for dx, dy in [(0, -1), (0, 1), (1, 0), (-1, 0)]:
                new_x = ci + dx
                new_y = ri + dy
                if 0 <= new_x <= len(row)-1 and 0 <= new_y <= len(field)-1:
                    if field[new_y][new_x] == 'O':
                        new_field[ri][ci] = 'O'

    empty_count = 0
    oxygen_count = 0
    for row in new_field:
        for col in row:
            if col == '.':
                empty_count += 1
            elif col == 'O':
                oxygen_count += 1


    # Everything filled, or no unreachable squares left
    if empty_count == 0 or field == new_field:
        print('Part 2: ', steps)
        break
    else:
        steps += 1
        field = new_field
