import math


def calc_fuel_cost(mass):
    return math.floor(mass/3) - 2


def main():
    running_total = 0
    for line in open('input.txt'):
        try:
            mass = int(line.strip())
        except Exception as e:
            print(e)
            return
        fuel_cost = calc_fuel_cost(mass)
        running_total += fuel_cost

    print('Total fuel cost: ', running_total)


if __name__ == "__main__":
    main()
