import math


def calc_fuel_cost(mass):
    return math.floor(mass/3) - 2


def calc_additional_fuel_cost(initial_fuel_cost):
    fuel_mass = initial_fuel_cost
    additional_fuel_cost = 0
    while fuel_mass >= 0:
        extra_cost = calc_fuel_cost(fuel_mass)
        if extra_cost > 0:
            additional_fuel_cost += extra_cost
        else:
            break
        fuel_mass = extra_cost
    return additional_fuel_cost


def main():
    running_total = 0
    for line in open('input.txt'):
        try:
            mass = int(line.strip())
        except Exception as e:
            print(e)
            return
        fuel_cost = calc_fuel_cost(mass)
        additional_fuel_cost = calc_additional_fuel_cost(fuel_cost)

        running_total += fuel_cost + additional_fuel_cost

    print('Total fuel cost: ', running_total)


if __name__ == "__main__":
    main()
