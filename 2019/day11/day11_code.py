# -*- coding: utf-8 -*-

from collections import deque
from intcode import Program

from PIL import Image, ImageDraw


def run_day11(start_color):
    with open('day11.in') as f:
        program = [int(element) for element in f.read().split(',')]

    currently_painted = set()
    total_painted = set()

    P = Program(deque([start_color]), program[:])
    dx = 0
    dy = -1
    x = y = 0

    dir = [dy, dx]

    while True:
        state = P.run()
        if state == None:
            return total_painted, currently_painted
        elif state == 0:
            currently_painted.discard((y, x))
        elif state == 1:
            currently_painted.add((y, x))
            total_painted.add((y, x))

        turn = P.run()
        # print('Turning: ', turn, dir)
        if turn == 1:
            dir = [dir[1], -dir[0]]
        elif turn == 0:
            dir = [-dir[1], dir[0]]
        y += dir[0]
        x += dir[1]
        if (y, x) in currently_painted:
            P.Q.append(1)
        else:
            P.Q.append(0)

total_painted, _ = run_day11(0)
print('Day 1: ', len(total_painted))
_, currently_painted = run_day11(1)
y_values = [c[0] for c in currently_painted]
min_y = min(y_values)
max_y = max(y_values)
x_values = [c[1] for c in currently_painted]
min_x = min(x_values)
max_x = max(x_values)

im = Image.new('RGB', (max_y+1, max_x+1), (0, 0, 0))
for c in currently_painted:
    try:
        im.putpixel(c, (255, 255, 255))
    except Exception as e:
        print(e)
        raise
# It's small and mirrored but you can figure it out. For this input it's RKURGKGK
im.show()
