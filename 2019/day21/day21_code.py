# -*- coding: utf-8 -*-

from collections import deque
from intcode import Program

with open('day21.in') as f:
    program = [int(element) for element in f.read().split(',')]

def convert_input(s):
    return [ord(c) for c in s] + [10]

input_Q = deque()
def input_function():
    if len(input_Q):
        return input_Q.popleft()
    else:
        val = input()
        input_Q.extend(convert_input(val))
        return input_Q.popleft()


P = Program(deque([]), program[:], input_function=input_function)
cmds = [
    'NOT C J',
    'NOT D T',
    'WALK'
]
for cmd in cmds:
    input_Q.extend(convert_input(cmd))
while True:
    out = P.run()
    if out is None:
        break
    if out == 10:
        print()
    else:
        print(chr(out), end='')
