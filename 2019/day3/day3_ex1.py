# -*- coding: utf-8 -*-

from collections import defaultdict

import sys

def process_node(node, map_dict, current_position):
    direction = node[0]
    distance = int(node[1:])
    # Mark every tile along the way

    if direction == 'U':
        new_position = (current_position[0] - distance, current_position[1])
        for delta in range(1, distance + 1):
            map_dict[(current_position[0] - delta, current_position[1])] = 1
    elif direction == 'R':
        new_position = (current_position[0], current_position[1] + distance)
        for delta in range(1, distance + 1):
            map_dict[(current_position[0], current_position[1] + delta)] = 1
    elif direction == 'D':
        new_position = (current_position[0] + distance, current_position[1])
        for delta in range(1, distance + 1):
            map_dict[(current_position[0] + delta, current_position[1])] = 1
    elif direction == 'L':
        new_position = (current_position[0], current_position[1] - distance)
        for delta in range(1, distance + 1):
            map_dict[(current_position[0], current_position[1] - delta)] = 1
    return new_position

def main():
    with open('input.txt') as f:
        route1 = f.readline().split(',')
        route2 = f.readline().split(',')

    map_dict_1 = defaultdict(int)
    # Central port
    map_dict_1[(0, 0)] = 1

    current_position = (0, 0)
    # Route 1
    for node in route1:
        current_position = process_node(node, map_dict_1, current_position)

    map_dict_2 = defaultdict(int)
    # Central port
    map_dict_2[(0, 0)] = 1
    # Central port
    current_position = (0, 0)
    # Route 2
    for node in route2:
        current_position = process_node(node, map_dict_2, current_position)

    # Merge the 2 maps. 1 if 1 in both maps
    map_dict_3 = defaultdict(int)
    for position in set(map_dict_1.keys()).union(set(map_dict_2.keys())):
        if map_dict_1[position] and map_dict_2[position]:
            map_dict_3[position] = 1

    # Remove (0, 0)
    map_dict_3.pop((0,0), False)

    min_distance = sys.maxsize
    min_distance_point = None
    for key in map_dict_3.keys():
        manhattan_distance = abs(key[0]) + abs(key[1])
        if manhattan_distance < min_distance:
            min_distance = manhattan_distance
            min_distance_point = key

    print('Lowest intersection distance ', min_distance, ' at point ', min_distance_point)


if __name__ == "__main__":
    main()