# -*- coding: utf-8 -*-

from collections import defaultdict

import sys

def process_node(node, map_dict, current_position, steps):
    direction = node[0]
    distance = int(node[1:])
    # Mark every tile along the way with how many steps it took to get there

    if direction == 'U':
        new_position = (current_position[0] - distance, current_position[1])
        for delta in range(1, distance + 1):
            if (current_position[0] - delta, current_position[1]) not in map_dict:
                map_dict[(current_position[0] - delta, current_position[1])] = {
                    'steps': steps + delta
                }
    elif direction == 'R':
        new_position = (current_position[0], current_position[1] + distance)
        for delta in range(1, distance + 1):
            if (current_position[0], current_position[1] + delta) not in map_dict:
                map_dict[(current_position[0], current_position[1] + delta)] = {
                    'steps': steps + delta
                }
    elif direction == 'D':
        new_position = (current_position[0] + distance, current_position[1])
        for delta in range(1, distance + 1):
            if (current_position[0] + delta, current_position[1]) not in map_dict:
                map_dict[(current_position[0] + delta, current_position[1])] = {
                    'steps': steps + delta
                }
    elif direction == 'L':
        new_position = (current_position[0], current_position[1] - distance)
        for delta in range(1, distance + 1):
            if (current_position[0], current_position[1] - delta) not in map_dict:
                map_dict[(current_position[0], current_position[1] - delta)] = {
                    'steps': steps + delta
                }

    return new_position, steps + distance

def main():
    with open('input.txt') as f:
        route1 = f.readline().split(',')
        route2 = f.readline().split(',')

    map_dict_1 = defaultdict(dict)
    # Central port
    map_dict_1[(0, 0)] = {
        'steps': 0
    }

    current_position = (0, 0)
    steps = 0
    # Route 1
    for node in route1:
        current_position, steps = process_node(node, map_dict_1, current_position, steps)

    map_dict_2 = defaultdict(dict)
    # Central port
    map_dict_2[(0, 0)] = {
        'steps': 0
    }
    # Central port
    current_position = (0, 0)
    steps = 0
    # Route 2
    for node in route2:
        current_position, steps = process_node(node, map_dict_2, current_position, steps)

    # Merge the 2 maps. Add the steps from route 1 and route 2
    map_dict_3 = defaultdict(int)
    for position in set(map_dict_1.keys()).union(set(map_dict_2.keys())):
        if map_dict_1[position] and map_dict_2[position]:
            try:
                map_dict_3[position] = map_dict_1[position]['steps'] + map_dict_2[position]['steps']
            except Exception as e:
                print(e)

    # Remove (0, 0)
    map_dict_3.pop((0,0), False)

    print('Lowest combined steps to intersection: ', min(map_dict_3.values()))


if __name__ == "__main__":
    main()