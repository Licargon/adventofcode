# -*- coding: utf-8 -*-

ADD = 1
MULT = 2
QUIT = 99

class UnknownOpCodeException(Exception):
    pass

def process_opcode(opcode, instruction_pointer, intcode_program):
    if opcode == ADD:
        arg1_pos = intcode_program[instruction_pointer + 1]
        arg1 = intcode_program[arg1_pos]

        arg2_pos = intcode_program[instruction_pointer + 2]
        arg2 = intcode_program[arg2_pos]

        result_pos = intcode_program[instruction_pointer + 3]
        intcode_program[result_pos] = arg1 + arg2
    elif opcode == MULT:
        arg1_pos = intcode_program[instruction_pointer + 1]
        arg1 = intcode_program[arg1_pos]

        arg2_pos = intcode_program[instruction_pointer + 2]
        arg2 = intcode_program[arg2_pos]

        result_pos = intcode_program[instruction_pointer + 3]
        intcode_program[result_pos] = arg1 * arg2
    else:
        raise UnknownOpCodeException('Unknown opcode: {}'.format(opcode))

def run_intcode_program(intcode_program):
    instruction_pointer = 0
    while True:
        opcode = intcode_program[instruction_pointer]
        if opcode == QUIT:
            return intcode_program[0]
        else:
            process_opcode(opcode, instruction_pointer, intcode_program)
            instruction_pointer += 4


def main():
    with open('input.txt') as f:
        original_intcode_program = [int(element) for element in f.read().split(',')]
    # Part 1
    intcode_program = original_intcode_program[:]
    # Instructions: Replace position 1 with value '12' and position 2 with value '2'
    intcode_program[1] = 12
    intcode_program[2] = 2
    result = run_intcode_program(intcode_program)
    print('Answer: ', result)

    # Part 2
    # Bruteforce
    for noun in range(100):
        for verb in range(100):
            intcode_program = original_intcode_program[:]
            intcode_program[1] = noun
            intcode_program[2] = verb
            result = run_intcode_program(intcode_program)
            if result == 19690720:
                print('Noun = {} Verb = {} Answer = {}'.format(noun, verb, (100*noun+verb)))
                return

if __name__ == "__main__":
    main()