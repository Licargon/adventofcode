# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day22.in') as f:
    lines = [int(line.strip()) for line in f.readlines()]


# For each starting number, keep track of what profits show up in what order for every 4 tuple of changes
# e,.g. 1 -> {(-1, -1, +1, +1) -> [0, 5, 9]}
profit_sequences_map = {}

all_sequences = set()
part1 = 0
for number in lines:
    start = number
    profit_sequences_map[number] = defaultdict(list)
    last_digits = [number % 10]
    change_seq = [0]

    for _ in range(2000):
        orig = number
        number = ((number*64) ^ number) % 16777216
        number = ((number//32) ^ number) % 16777216
        number = ((number*2048) ^ number) % 16777216

        last_digit = number % 10
        change = last_digit - last_digits[-1]
        last_digits.append(last_digit)
        change_seq.append(change)
        # if last_digit > cur_max and len(change_seq) >= 4:
        if len(change_seq) >= 4 :
            profit_sequences_map[start][tuple(change_seq[-4:])].append(last_digit)
            all_sequences.add(tuple(change_seq[-4:]))

    part1 += number

print('Part 1:', part1)


part2 = 0
cur_max = -9999999
for seq in all_sequences:
    cur = 0
    for start_number, seq_to_profits in profit_sequences_map.items():
        if seq in seq_to_profits:
            # Check the first profit that shows up for this sequence
            cur += seq_to_profits[seq][0]
    cur_max = max(cur_max, cur)

print('Part 2: ', cur_max)
