# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day13.in') as f:
    blocks = f.read().split('\n\n')

part1 = 0
part2 = 0
for block in blocks:
    Ax, Ay, Bx, By, Px, Py = list(map(int, re.findall(r'\d+', block)))
    # A, B, PRIZE = block.split('\n')
    #
    # Ax, Ay = list(map(int, re.findall(r'\d+', A)))
    # Bx, By = list(map(int, re.findall(r'\d+', B)))
    # Px, Py = list(map(int, re.findall(r'\d+', PRIZE)))

    for i in range(100):
        for j in range(100):
            x = i * Ax + j * Bx
            y = i * Ay + j * By
            if x == Px and y == Py:
                part1 += 3 * i + j

    # Part 2: Figure out how many we need to push if the points are 1000000.... units in the distance
    # 2 equations, 2 variables -> unique solution
    Px += 10000000000000
    Py += 10000000000000
    A_presses = (Px * By - Py * Bx) / (Ax * By - Ay * Bx)
    B_presses = (Px - Ax * A_presses) / Bx
    # Only a valid solution if integers!
    if A_presses == int(A_presses) and B_presses == int(B_presses):
        part2 += 3 * A_presses + B_presses


print('Part 1: ', part1)
print('Part 2: ', int(part2))
