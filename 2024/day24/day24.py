# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import copy
import graphviz
import itertools
import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day24.in') as f:
    INITIAL, RULES = f.read().split('\n\n')

state = {}

class Rule(object):
    def __init__(self, left, op, right, result):
        self.left = left
        self.op = op
        self.right = right
        self.result = result

    def can_compute(self, state):
        return (self.left in state and self.right in state)

    def __repr__(self):
        return '{} {} {} -> {}'.format(self.left, self.op, self.right, self.result)

    def compute(self, state):
        if self.op == 'AND':
            return self.result, state[self.left] and state[self.right]
        elif self.op == 'OR':
            return self.result, state[self.left] or state[self.right]
        elif self.op == 'XOR':
            return self.result, state[self.left] ^ state[self.right]
        else:
            raise Exception('Invalid operand: {}'.format(self.op))

    def key(self):
        return '{}{}{}'.format(self.left,self.op,self.right)


for line in INITIAL.strip().split('\n'):
    wire, value = line.split(': ')
    value = int(value)
    state[wire] = value

INITIAL_STATE = copy.deepcopy(state)

def parse_rules(lines, sub={}):

    OUTPUTS = set()
    rules = []
    for line in lines.strip().split('\n'):
        match = re.match(r'(.*) (AND|XOR|OR) (.*) -> (.*)', line)
        new_rule = Rule(*match.groups())
        if sub.get(new_rule.result, None):
            new_rule.result = sub[new_rule.result]
        rules.append(new_rule)

        OUTPUTS.add(new_rule.result)
    return rules, OUTPUTS


def solve(rules, state):
    ok = False

    # Keep trying untill the rules resolve everything
    while not ok:
        all_rules_possible = True
        for rule in rules:
            if rule.result not in state and rule.can_compute(state):
                all_rules_possible = False
                wire, value = rule.compute(state)
                state[wire] = value

        if all_rules_possible:
            ok = True

    return state

def get_result(state):
    z_wires = sorted([(w, value) for w, value in state.items() if w.startswith('z')], key=lambda v: v[0])

    res = ''
    # z00 is least significant -> concatenate in reverse
    for wire in z_wires[::-1]:
        res += str(wire[1])
    return int(res, 2)

def print_graph(rules):
    dot = graphviz.Graph()

    for rule in rules:

        dot.node(rule.left, rule.left)
        dot.node(rule.right, rule.right)
        if rule.op == 'XOR':
            color='green'
        else:
            color = 'white'
        dot.node(rule.key(), rule.op, shape='diamond', color=color, style='filled')
        if rule.result.startswith('z'):
            dot.node(rule.result, color='blue', style='filled', fontcolor='white')
        else:
            dot.node(rule.result)
        dot.edge(rule.left, rule.key())
        dot.edge(rule.right, rule.key())
        dot.edge(rule.key(), rule.result)

    dot.render('graph', view=True, format='png')


rules, OUTPUTS = parse_rules(RULES)

wrong = []
for rule in rules:
    if rule.result.startswith('z') and rule.op != 'XOR' and rule.result != 'z45':
        # A Zxx node MUST be preceded by a XOR, EXCEPT the final one
        wrong.append(rule)
    elif rule.op == 'XOR':
        # No XOR allowed between any 3 nodes that arent input or output
        if rule.left[0] not in 'xy' and rule.right[0] not in 'xy' and rule.result[0] != 'z':
            wrong.append(rule)
        else:
            for rule2 in rules:
                if (rule2.left == rule.result or rule2.right == rule.result) and rule2.op == 'OR':
                    wrong.append(rule)
    elif rule.op == 'AND' and 'x00' not in [rule.left, rule.right]:
        # For any AND that doesnt start with the initial 2 bits, it MUST be followed by an OR
        for rule2 in rules:
            if (rule2.left == rule.result or rule2.right == rule.result) and rule2.op != 'OR':
                wrong.append(rule)

print_graph(rules)

state = solve(rules, state)
print('Part 1:', get_result(state))

"""
Methodology:

- Printed out the graph with graphviz (print_graph call above)
- Analysed the structure visually to find wrong parts in the adder (ripple adder or something?)
- Wrote some code to check soms basic rules, ended up with 8 outputs and thus the answer
"""
wrong_outputs = list(set([rule.result for rule in wrong]))
print('Part 2: ', ','.join(sorted(wrong_outputs)))
