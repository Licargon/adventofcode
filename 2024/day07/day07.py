
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

with open('day07.in') as f:
    lines = [line.strip() for line in f.readlines()]


def is_valid(target, values, part2=False):

    if len(values) == 1:
        return values[0] == target

    ret = is_valid(target, [values[0] + values[1]] + values[2:], part2) or is_valid(target, [values[0] * values[1]] + values[2:], part2)
    if part2 and len(values) > 1:
        # Try concatenation
        new_value = int(str(values[0]) + str(values[1]))
        ret = ret or is_valid(target, [new_value] + values[2:], part2)
    return ret

part1 = 0
part2 = 0
for line in lines:
    target, values = line.split(': ')
    target = int(target)
    values = [int(v) for v in values.split(' ')]
    if is_valid(target, values, 0):
        part1 += target
    if is_valid(target, values,  True):
        part2 += target

print('Part 1: ', part1)
print('Part 2: ', part2)






