# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import math
import sys

with open('day10.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = {}
STARTS = []
for ir, row in enumerate(lines):
    for ic, col in enumerate(row):
        if col != '.':
            col = int(col)
        G[(ir, ic)] = col
        if col == 0:
            STARTS.append((ir, ic))

def path_to_key(path):
    return ''.join([str(p) for p in path])

def dfs(G, STARTS, p2):
    ENDS_PER_START = defaultdict(set)
    PATHS_PER_START = defaultdict(set)
    for (sr, sc) in STARTS:
        Q = deque([(sr, sc, [])])
        SEEN = set()
        while Q:
            r, c, path = Q.popleft()
            # Don't skip here because if one node '8' links to multiple node 9's we want to count all of them
            if not p2:
                if (r, c) in SEEN:
                    continue
            SEEN.add((r, c))
            for (dr, dc) in [(-1, 0), (0, 1), (1, 0), (0, -1)]:
                nr, nc = r + dr, c + dc
                if (nr, nc) in G:
                    if G[(nr, nc)] == G[(r, c)] + 1 and (nr, nc) not in SEEN:
                        if G[(nr, nc)] == 9:
                            ENDS_PER_START[(sr, sc)].add((nr, nc))
                            PATHS_PER_START[(sr, sc)].add(path_to_key(path + [(nr, nc)]))
                        else:
                            # Viable neighbour
                            Q.append((nr, nc, path + [(nr, nc)]))

    if p2:
        return sum([len(v) for v in PATHS_PER_START.values()])
    else:
        return sum([len(v) for v in ENDS_PER_START.values()])

print('Part 1: ', dfs(G, STARTS, False))
print('Part 2: ', dfs(G, STARTS, True))
# 523 too low
