# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

SIZE = 71
TO_PLACE = 1024

with open('day18.in') as f:
    lines = [line.strip() for line in f.readlines()]

coords = []
for line in lines:
    coords.append(list(map(int, line.split(','))))

def find_path(to_place):
    G = [['.' for _ in range(SIZE)] for _ in range(SIZE)]

    for x, y in coords[:to_place]:
        G[y][x] = '#'


    Q = deque([(0, 0, 0, [])])
    SEEN = set()
    while Q:
        y, x, dist, path = Q.popleft()
        if (y, x) in SEEN:
            continue

        SEEN.add((y, x))

        for dy, dx in DIRS:
            ny = y+dy
            nx = x+dx
            if 0 <= ny < SIZE and 0 <= nx < SIZE:
                if G[ny][nx] == '#':
                    continue
                if (ny, nx) == (SIZE-1, SIZE-1):
                    if to_place == TO_PLACE:
                        # Part 1
                        return dist + 1
                    else:
                        # Part 2
                        return True

                if (ny, nx) in SEEN:
                    continue
                new_path = path[:]
                new_path.append((ny, nx))
                Q.append((ny, nx, dist+1, new_path))
    return False

print('Part 1:', find_path(1024))

# Bruteforce.
i = TO_PLACE
while i < len(coords):
    if not find_path(i):
        print('Part 2: {},{}'.format(coords[i-1][0], coords[i-1][1]))
        break
    i += 1