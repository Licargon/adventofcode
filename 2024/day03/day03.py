
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import re
import sys

with open('day03.in') as f:
    lines = [line.strip() for line in f.readlines()]

all_lines = '-'.join(lines)

regex = r'mul\((\d{1,3}),(\d{1,3})\)|(do\(\))|(don\'t\(\))'

part1 = 0
ADDING = True
part2 = 0
for res in re.findall(regex, all_lines):
    if res[3] == "don't()":
        ADDING = False
    elif res[2] == "do()":
        ADDING = True
    else:
        part1 += int(res[0]) * int(res[1])
        if ADDING:
            part2 += int(res[0]) * int(res[1])

print('Part 1: ', part1)
print('Part 2: ', part2)



