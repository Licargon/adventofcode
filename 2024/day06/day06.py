# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from itertools import permutations

import copy
import re
import sys

with open('day06.in') as f:
    lines = [line.strip() for line in f.readlines()]


G = {}

face_direction = False
for ir, row in enumerate(lines):
    for ic, col in enumerate(row):
        if col in '^v><':
            cur_y, cur_x = ir, ic
            face_direction = col
            G[(ir, ic)] = '.'
        else:
            G[(ir, ic)] = col

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left
DIR_MAP = {
    '^': 0,
    '>': 1,
    'v': 2,
    '<': 3
}
cur_dir = DIR_MAP[face_direction]


def simulate(G, cur_y, cur_x, cur_dir, DIRS, max_steps=10000):
    SEEN = set()
    PATH = []
    step = 0
    went_outside = False
    while step < max_steps:
        new_y = cur_y + DIRS[cur_dir][0]
        new_x = cur_x + DIRS[cur_dir][1]
        if (new_y, new_x) not in G:
            went_outside = True
            break

        if G[(new_y, new_x)] == '.':
            SEEN.add((new_y, new_x))
            PATH.append((new_y, new_x))
            cur_y = new_y
            cur_x = new_x
            step += 1
        elif G[(new_y, new_x)] == '#':
            cur_dir = (cur_dir+1)%4

    return step, went_outside, SEEN, PATH

_, _, SEEN, path = simulate(G, cur_y, cur_x, cur_dir, DIRS)
print('Part 1: ', len(SEEN))

# Bruteforce
part2 = 0
# Try to place obstacles on the original path
for (ir, ic) in SEEN:
    if G[(ir, ic)] == '.':
        new_G = copy.copy(G)
        new_G[(ir, ic)] = '#'
        step, went_outside, SEEN, path = simulate(new_G, cur_y, cur_x, cur_dir, DIRS)
        if not went_outside:
            part2 += 1

print('Part 2: ', part2)
