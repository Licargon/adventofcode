
# -*- coding: utf-8 -*-

from collections import defaultdict, deque, Counter

from copy import copy
import sys

with open('day11.in') as f:
    lines = [line.strip() for line in f.readlines()]

rocks = list(map(int, lines[0].split(' ')))
print(rocks)

def tick(rocks):
    keys = [k for k in rocks.keys() if rocks[k] > 0]
    new_rocks = Counter()
    for key in keys:
        if rocks[key] == 0:
            continue
        if key == 0:
            new_rocks[1] += rocks[0]
        elif len(str(key)) % 2 == 0:
            k1, k2 = str(key)[0:len(str(key)) // 2], str(key)[len(str(key)) // 2:]
            assert k1+k2 == str(key), f'False split {key} != {k1} + {k2}'
            new_rocks[int(k1)] += rocks[key]
            new_rocks[int(k2)] += rocks[key]
        else:
            new_rocks[2024*key] += rocks[key]
    return new_rocks

rocks = Counter(rocks)
for i in range(1, 26):
    rocks = tick(rocks)


print('Part 1: ', rocks.total())

for i in range(50):
    rocks = tick(rocks)

print('Part 2: ', rocks.total())
