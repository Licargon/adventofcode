# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

with open('day01.in') as f:
    lines = [line.strip() for line in f.readlines()]

left = []
right = []
for line in lines:
    l, r = line.split()
    left.append(int(l))
    right.append(int(r))

left = sorted(left)
right = sorted(right)

part1 = 0
for l, r in zip(left, right):
    part1 += abs(r-l)

print(part1)

part2 = 0
for l in left:
    part2 += l * right.count(l)

print(part2)