
# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import math
import sys

with open('day08.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = {}
antennas = defaultdict(list)

for ir, row in enumerate(lines):
    for ic, ch in enumerate(row):
        G[(ir, ic)] = ch
        if ch != '.':
            antennas[ch].append((ir, ic))


def get_dist(y1, x1, y2, x2):
    return math.sqrt((y2-y1)**2 + (x2-x1)**2)


antinodes = set()
antinodes2 = set()

for R in range(len(lines)):
    for C in range(len(lines[0])):
        # Check if eligible for antinode
        # Can be antinode IF in line with 2 antennas and dist1 is double dist 2

        for key, locations in antennas.items():
            for i, (ay1, ax1) in enumerate(locations):
                for j, (ay2, ax2) in enumerate(locations):
                    if (ay1, ax1) != (ay2,ax2):
                        dist1 = get_dist(R, C, ay1, ax1)
                        dist2 = get_dist(R, C, ay2, ax2)

                        slope = (ay2-ay1)/(ax2-ax1)

                        # Prevent div by 0
                        # new_slope = (R-ay1)/(C-ax1)
                        # (R-ay1)/(C-ax1) == (ay2-ay1)/(ax2-ax1)
                        # => (ay2-ay1) * (C-ax1) == (R-ay1) * (ax2-ax1)
                        slope_condition = (ay2-ay1) * (C-ax1) == (R-ay1) * (ax2-ax1)

                        if (dist1 == dist2*2 or dist2 == dist1*2) and slope_condition:
                            antinodes.add((R, C))
                        if slope_condition:
                            antinodes2.add((R, C))



print('Part 1: ', len(antinodes))
print('Part 2: ', len(antinodes2))