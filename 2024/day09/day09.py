# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import math
import sys

with open('day09.in') as f:
    line = f.readlines()[0].strip()

memory = []
cur_data = 0
programs = []
for i, ch in enumerate(line):
    if i % 2 == 0:
        programs.append({
            'code': cur_data,
            'length': int(ch),
            'start': len(memory),
        })
        for _ in range(int(ch)):
            memory.append(str(cur_data))
        cur_data += 1
    else:
        for _ in range(int(ch)):
            memory.append('.')


def has_gaps(memory):
    for i, ch in enumerate(memory):
        if ch == '.':
            return i
    return False

def defragment(memory):
    cnt = 0
    empty_spaces = [i for i, ch in enumerate(memory) if ch == '.']
    i = 0
    while i < len(empty_spaces) and empty_spaces[i] < len(memory):
        to_place = memory.pop()
        if to_place == '.':
            continue
        memory[empty_spaces[i]] = to_place
        i += 1
    return memory


def defragment2(memory, programs):
    # Move entire blocks
    # Scan for empty block sizes
    empty_blocks = []
    i = 0
    while i < len(memory):
        if memory[i] != '.':
            i += 1
        else:
            # Scan length
            j = i
            while memory[j] == '.':
                j += 1
            empty_blocks.append({
                'start': i,
                'length': j-i,
            })
            i = j
    for program in programs[::-1]:
        for empty_block in empty_blocks:
            if program['length'] <= empty_block['length'] and program['start'] > empty_block['start']:
                # Move program to empty block and remove at original position
                for i in range(program['length']):
                    memory[program['start'] + i] = '.'
                for i in range(program['length']):
                    memory[empty_block['start'] + i] = str(program['code'])
                empty_block['length'] -= program['length']
                empty_block['start'] += program['length']
                if empty_block['length'] == 0:
                    empty_blocks.remove(empty_block)
                break

    return memory

def checksum(memory):
    res = 0
    for idx, ch in enumerate(memory):
        if ch != '.':
            res += idx * int(ch)
    return res


memory2 = defragment(memory[:])
print('Part 1: ', checksum(memory2))

memory3 = defragment2(memory[:], programs)
print('Part 2: ', checksum(memory3))
