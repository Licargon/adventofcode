# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day12.in') as f:
    lines = [line.strip() for line in f.readlines()]


part1 = 0

AREAS = defaultdict(set)
SEEN = set()
for ir in range(len(lines)):
    for ic in range(len(lines[0])):
        if (ir, ic) in SEEN:
            continue
        area = 0
        perimeter = 0
        Q = deque([(ir, ic)])

        AREAS[(lines[ir][ic], ir, ic)].add((ir, ic))
        while Q:
            r, c = Q.popleft()
            if (r, c) in SEEN:
                continue
            area += 1
            SEEN.add((r, c))
            for dy, dx in DIRS:
                nr = r + dy
                nc = c + dx
                if 0 <= nr < len(lines) and 0 <= nc < len(lines[0]) and lines[nr][nc] == lines[r][c]:
                    Q.append((nr, nc))
                    AREAS[(lines[ir][ic], ir, ic)].add((nr, nc))
                else:
                    perimeter += 1

        part1 += area * perimeter

print('Part 1: ', part1)

# Part 2: Find the corners
part2 = 0

for letter, spaces in AREAS.items():
    corners = 0
    for (sy, sx) in spaces:
        up, right, down, left = [(sy+dy, sx+dx) for (dy, dx) in DIRS]
        # print('Space {} Neighbours {}'.format((sy, sx), (up, right, down, left)))
        # Top left corner
        if (right in spaces or down in spaces) and left not in spaces and up not in spaces:
            corners += 1
        # Bottom left corner
        if (right in spaces or up in spaces) and left not in spaces and down not in spaces:
            corners += 1
        # Top right corner
        if (left in spaces or down in spaces) and up not in spaces and right not in spaces:
            corners += 1
        # Bottom right corner
        if (left in spaces or up in spaces) and right not in spaces and down not in spaces:
            corners += 1
        bottomleft = (sy+1, sx-1)
        bottomright = (sy+1, sx+1)
        topleft = (sy-1, sx-1)
        topright = (sy-1, sx+1)
        # Inside corners
        # _|
        if (left in spaces and up in spaces) and topleft not in spaces:
            corners += 1
        # |_
        if (up in spaces and right in spaces) and topright not in spaces:
            corners += 1
        # -|
        if (left in spaces and down in spaces) and bottomleft not in spaces:
            corners += 1
        # |-
        if (right in spaces and down in spaces) and bottomright not in spaces:
            corners += 1

        # Single square?
        if len({up, right, down, left} & spaces) == 0:
            corners = 4

    part2 += len(spaces) * corners

print('Part 2: ', part2)
