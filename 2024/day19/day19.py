# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

from functools import cache

import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day19.in') as f:
    lines = [line.strip() for line in f.readlines()]

towels = lines[0].split(', ')

@cache
def can_make(target, towels):

    if not target:
        return 1

    count = 0
    for towel in towels:
        if target.startswith(towel):
            count += can_make(target[len(towel):], towels)
    return count


part1 = 0
part2 = 0

for target in lines[2:]:
    count = can_make(target, tuple(towels))
    part1 += bool(count)
    part2 += count


print('Part 1: ', part1)
print('Part 2: ', part2)

