# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import heapq
import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day16.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = [list(line) for line in lines]

for ir, row in enumerate(G):
    for ic, col in enumerate(row):
        if col == 'E':
            end_row, end_col = ir, ic
        elif col == 'S':
            start_row, start_col = ir, ic

# for row in G:
#     print(*row)

def find_path_part1(G):
    # 0 = north, 1 = east, 2 = south, 3 = west
    # Start = dist 0, east
    # State = score, row, col, dir, path
    Q = [(0, start_row, start_col, 1)]
    SEEN = set()
    while Q:
        score, r, c, dir = heapq.heappop(Q)

        # 2 options: We move a step forward in current direction, or we turn
        nr = r + DIRS[dir][0]
        nc = c + DIRS[dir][1]
        if (nr, nc, dir) in SEEN:
            continue
        SEEN.add((nr, nc, dir))
        if 0 <= nr < len(G) and 0 <= nc < len(G[0]):
            if G[nr][nc] == 'E':
                return score + 1 # Cur score + 1 step

            elif G[nr][nc] != '#':
                heapq.heappush(Q, (score+1, nr, nc, dir))

            # Only allow turn left and right, not go back
            heapq.heappush(Q, (score + 1000, r, c, (dir+1)%4))
            heapq.heappush(Q, (score + 1000, r, c, (dir+3)%4))


part1 = find_path_part1(G)
print('Part 1: ', part1)

# Figure out ALL paths to the end and their cost with a BFS

# Cache seen path as string representation of the path
SEEN = {str([(start_row, start_col)])}

# Row, col, dir, score, path
Q = deque([(start_row, start_col, 1, 0, [(start_row, start_col)])])

# (r, c, dir) => score
# We record the lowest score for a coord + dir combination
# If we find a coord/dir combination with a score high than the current lowest, don't further explore it

SEEN_COORDS = {}

# (score, path)
paths = []
while Q:
    r, c, dir, score, path = Q.popleft()

    cur_score = SEEN_COORDS.get((r, c, dir), 10e15)
    score_cutoff = min(cur_score, part1)
    # Only further check a path if its score is not higher than other paths at this node/direction, or
    # higher than the best path found in p1
    if score > score_cutoff:
        continue

    SEEN_COORDS[(r, c, dir)] = min(score, cur_score)

    # 2 options: We move a step forward in current direction, or we turn
    nr = r + DIRS[dir][0]
    nc = c + DIRS[dir][1]


    # Failsafe to make sure we never go backwards, for any reason
    if (nr, nc) in path:
        continue

    temp_path = path[:]
    temp_path.append((nr, nc))

    # Dont further explore paths that we already encountered, despite the score
    if str(temp_path) in SEEN:
        continue

    SEEN.add(str(temp_path))
    if 0 <= nr < len(G) and 0 <= nc < len(G[0]):
        new_path = path[:]
        new_path.append((nr, nc))
        if G[nr][nc] == 'E':
            paths.append((score+1, new_path))
            continue

        elif G[nr][nc] != '#':
            Q.append((nr, nc, dir, score+1, new_path))

        # Only allow turn left and right, not go back
        Q.append((r, c, (dir+1)%4, score+1000, path))
        Q.append((r, c, (dir+3)%4, score+1000, path))


# Get all unique coords part of all lowest paths
paths = sorted(paths, key=lambda t: t[0])
lowest_cost_paths = [p for p in paths if p[0] == paths[0][0]]

all_nodes = set()

for _, path in lowest_cost_paths:
    for r, c in path:
        all_nodes.add((r, c))

print('Part 2: ', len(all_nodes))


