# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day25.in') as f:
    blocks = f.read().strip().split('\n\n')

locks = []
keys = []
for block in blocks:
    G = [list(line.strip()) for line in block.split('\n')]
    columns = []
    for ic in range(len(G[0])):
        # Need to subtract 1 to get rid of top/bottom row
        col_count = -1
        for ir in range(0, len(G)):
            if G[ir][ic] == '#':
                col_count += 1
        columns.append(col_count)

    if all(x == '#' for x in G[0]):
        locks.append(columns)
    else:
        keys.append(columns)

part1 = 0

for lock in locks:
    for key in keys:
        ok = True
        for col1, col2 in zip(lock, key):
            if col1 + col2 > 5:
                ok = False

        if ok:
            part1 += 1

print('Part 1:', part1)
