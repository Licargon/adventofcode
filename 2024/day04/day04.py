# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import re
import sys

with open('day04.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = {}

for ir, row in enumerate(lines):
    for ic, col in enumerate(row):
        G[(ir, ic)] = col

part1 = 0
TO_FIND = 'XMAS'
for (y, x), value in G.items():
    if value == 'X':
        found = False
        # Start searching for the word X-mas
        STOP = False
        for dir_y, dir_x in [(-1, 0), (1, 0), (0, -1), (0, 1), (-1, -1), (-1, 1), (1, -1), (1, 1)]:
            for i in range(1, 4):
                new_y = (y + dir_y * i)
                new_x = (x + dir_x * i)
                char = G.get((new_y, new_x), None)
                # print('Start = ', (y, x), 'New = ', (new_y, new_x), ' letter = ', char, ' looking for: ', TO_FIND[i])
                if (new_y, new_x) not in G:
                    break
                elif G[(new_y, new_x)] != TO_FIND[i]:
                    break
            else:
                part1 += 1

print('Part 1: ', part1)

part2 = 0
for (y, x), value in G.items():
    if value == 'A':
        # Check the 4 'corners' next to this A value
        top_left = (y-1, x-1)
        top_right = (y-1, x+1)
        bottom_left = (y+1, x-1)
        bottom_right = (y+1, x+1)
        valid_start = True
        for (new_y, new_x) in [top_left, top_right, bottom_left, bottom_right]:
            if (new_y, new_x) not in G:
                # Out of bounds.
                valid_start = False
        if not valid_start:
            continue

        set1 = {G[top_left], G[bottom_right]}
        set2 = {G[top_right], G[bottom_left]}
        # Both 'sets' of opposite corners should be S and M, so they spell 'MAS' with the middle A in any dir.
        if set1 == set2 == set(['M', 'S']):
            part2 += 1
print('Part 2: ', part2)
