# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import math
import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day17.in') as f:
    lines = [line.strip() for line in f.readlines()]

A = int(re.findall(r'-?\d+', lines[0])[0])
B = int(re.findall(r'-?\d+', lines[1])[0])
C = int(re.findall(r'-?\d+', lines[2])[0])

P_string = ','.join(re.findall(r'\d+', lines[-1]))
P = list(map(int, re.findall(r'\d+', lines[-1])))

class Program():
    def __init__(self, P, A):
        self.P = P
        self.A = A
        self.B = 0
        self.C = 0

    def get_combo(self, operand):
        if 0 <= operand <= 3:
            return operand
        if operand == 4:
            return self.A
        if operand == 5:
            return self.B
        if operand == 6:
            return self.C
        if operand == 7:
            raise Exception('Invalid')

    def run(self, part2=False):
        IP = 0
        out = []
        while True:
            if IP >= len(self.P):
                if part2:
                    return out
                else:
                    return self.format_output(out)
            opcode = self.P[IP]
            operand = self.P[IP+1]

            if opcode == 0: #adv
                self.A = self.A // 2 ** self.get_combo(operand)
            elif opcode == 1: #bxl
                self.B = self.B ^ operand
            elif opcode == 2: #bst
                self.B = self.get_combo(operand) % 8
            elif opcode == 3: #jnz
                if self.A != 0:
                    IP = operand - 2
            elif opcode == 4: #bxc
                self.B = self.B ^ self.C
            elif opcode == 5: #out
                out.append(self.get_combo(operand) % 8)
            elif opcode == 6: #bdv
                self.B = self.A // 2 ** self.get_combo(operand)
            elif opcode == 7: #cdv
                self.C = self.A // 2 ** self.get_combo(operand)

            IP += 2
    def format_output(self, out):
        return ','.join([str(num) for num in out])


program = Program(P, A)
out = program.run()
print('Part 1: ', out)

def find(program, program_left, cur):
    T_P = program[len(program)-len(cur)-1:]
    if program_left == []:
        return cur

    if not cur:
        A_START = 0
    else:
        A_START = 0
        for i, x in enumerate(cur[::-1]):
            A_START += x*8**i

    for d in range(8):
        A = (A_START << 3) + d
        p = Program(program, A)
        output = p.run(True)
        if output == T_P:
            new_cur = cur[:]
            # new_cur.append(output[-1])
            new_cur.append(d)
            sub_problem = find(program, program_left[:-1], new_cur)
            if not sub_problem:
                continue
            return sub_problem

part2 = find(P, P, [])
# Convert back to int
A = 0
for i, x in enumerate(part2[::-1]):
    A += x * 8**i
print('Part 2: ', A)
    # 3006165110264632 too high....?
    # 105981155568026

"""
2,4,1,5,7,5,1,6,0,3,4,1,5,5,3,0


2,4,    B = A % 8
1,5,    B = B ^ 5
7,5,    C = A // 2**B or A >> B
1,6,    B = B ^ 6
0,3,    A = A // 2**3 or A >> 3
4,1,    B = B^C
5,5,    out B % 8
3,0     JUMP

"""