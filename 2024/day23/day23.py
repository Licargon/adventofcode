# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import networkx

import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day23.in') as f:
    lines = [line.strip() for line in f.readlines()]

l = len(lines)
print(l)
print(l**3)

NETWORK = defaultdict(set)
for line in lines:
    l, r = line.split('-')
    NETWORK[l].add(r)
    NETWORK[r].add(l)

computers = list(NETWORK.keys())
groups = set()
for idx, c1 in enumerate(computers):
    for idx2, c2 in enumerate(computers[idx+1:]):
        for idx3, c3 in enumerate(computers[idx2+1:]):
            if c1 in NETWORK[c3] and c2 in NETWORK[c3] and c1 in NETWORK[c2] and c3 in NETWORK[c2] and c2 in NETWORK[c1] and c3 in NETWORK[c1]:
                # Sorted because doubles are a thing
                groups.add(tuple(list(sorted([c1, c2, c3]))))

part1 = 0
for group in groups:
    if any(s.startswith('t') for s in group):
        part1 += 1
print('Part 1:', part1)

G = networkx.Graph()
G.add_nodes_from(computers)

for line in lines:
    l, r = line.split('-')
    G.add_edge(l, r)

cur_max = -1
cur_max_clique = None
for clique in networkx.find_cliques(G):
    if len(clique) > cur_max:
        cur_max_clique = clique
        cur_max = len(clique)

print('Part 2: ', ','.join(list(sorted(cur_max_clique))))
