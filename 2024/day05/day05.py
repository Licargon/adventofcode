# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from itertools import permutations

import re
import sys

with open('day05.in') as f:
    content = f.read()

rules, updates = content.split('\n\n')

R = defaultdict(list)
R_REVERSE = defaultdict(list)
for line in rules.split('\n'):
    src, dest = line.split('|')
    R[src].append(dest)
    R_REVERSE[dest].append(src)

def is_valid(parts):
    """
    Loop over every part. Check that parts BEFORE cur_part are not defined to be behind in the rules and vice versa
    """
    ok = True
    for ip, part in enumerate(parts):
        # Check parts ahead.
        if part in R:
            for part_next in parts[ip + 1:]:
                if part in R_REVERSE and part_next in R_REVERSE[part]:
                    ok = False
                    break
        # Check parts behind
        if ip > 0:
            if part in R_REVERSE:
                for part_prev in parts[:ip]:
                    if part_prev in R[part]:
                        ok = False
                        break
    return ok


valid = []
invalid = []
for update in updates.strip().split('\n'):
    parts = update.split(',')

    if is_valid(parts):
        valid.append(update)
    else:
        invalid.append(update)

part1 = 0
for update in valid:
    parts = update.split(',')
    # middle element
    part1 += int(parts[len(parts)//2])

print('Part 1:', part1)

def swap(R, parts):
    # Loop over all rules and swap wrong parts if needed
    for before, after in R.items():
        for idx, left in enumerate(parts):
            for idx2, right in enumerate(parts[idx+1:], start=idx+1):
                if right == before and left in after:
                    parts[idx], parts[idx2] = parts[idx2], parts[idx]
                    return parts
    return parts

part2 = 0
for update in invalid:
    parts = update.split(',')
    while True:
        for before, after in R.items():
            parts = swap(R, parts)
        if is_valid(parts):
            break
    part2 += int(parts[len(parts)//2])

print('Part 2: ', part2)


# Works on example, too slow on real input
# fixed = []
# print('Checking ', len(invalid))
# for idx, update in enumerate(invalid):
#     print(idx)
#     parts = update.split(',')
#     all_permutations = permutations(parts)
#     ok = False
#     cur = 0
#     while not ok:
#         perm = list(next(all_permutations))
#         ok = is_valid(perm)
#         if ok:
#             break
#         if not ok:
#             cur += 1
#     fixed.append(list(perm))
#
# part2 = 0
# for update in fixed:
#     part2 += int(update[len(update)//2])
# print('Part 2: ', part2)
