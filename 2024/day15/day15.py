# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import copy
import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day15.in') as f:
    G, instructions = f.read().split('\n\n')
    G = [list(row) for row in G.split('\n')]
    instructions = instructions.replace('\n', '')

# Keep a copy for creating the expanded grid in part 2
ORIG_G = copy.deepcopy(G)

def print_grid(G):
    for row in G:
        print(''.join(row))
    print()

def solve(G):
    for ir, row in enumerate(G):
        for ic, col in enumerate(row):
            if col == '@':
                robot_y = ir
                robot_x = ic

    for ins in instructions:
        SEEN = set()
        dr, dc = {'^': (-1, 0), '>': (0, 1), 'v': (1, 0), '<': (0, -1)}[ins]

        cr = robot_y + dr
        cc = robot_x + dc

        can_move = True
        target = G[cr][cc]
        if target == '#':
            # We hit a wall, can't do anything with this instruction
            continue
        if target in '[]O':
            # DFS to get all pushable things in the given direction. If we encounter a wall ANYWHERE we mark it as can't move
            Q = deque([(cr, cc)])
            # Add the other part of the wall first if we are in part 2.
            if target == '[':
                Q.append((cr, cc + 1))
            elif target == ']':
                Q.append((cr, cc - 1))
            SEEN = set()
            can_move = True
            while Q:
                r, c = Q.popleft()
                if (r, c) in SEEN:
                    continue
                SEEN.add((r, c))

                nr = r + dr
                nc = c + dc
                if G[nr][nc] == '#':
                    can_move = False
                    break
                if G[nr][nc] == '[':
                    Q.append((nr, nc))
                    Q.append((nr, nc + 1))
                elif G[nr][nc] == ']':
                    Q.append((nr, nc))
                    Q.append((nr, nc - 1))
                elif G[nr][nc] == 'O':
                    Q.append((nr, nc))

        if can_move:
            # copy the grid for easier modification
            grid_copy = [list(r) for r in G]

            # Clear old boxes and move boxes from copy to new locations
            for br, bc in SEEN:
                G[br][bc] = '.'
            for br, bc in SEEN:
                G[br + dr][bc + dc] = grid_copy[br][bc]

            # Move the robot
            G[robot_y + dr][robot_x + dc] = '@'
            G[robot_y][robot_x] = '.'

            robot_y += dr
            robot_x += dc

    ans = 0
    for ir, row in enumerate(G):
        for ic, col in enumerate(row):
            if col in '[O':
                ans += 100 * ir + ic
    return ans

print('Part 1:', solve(G))

new_G = []
for row in ORIG_G:
    new_row = []
    for ch in row:
        if ch == '#':
            new_row.append('#')
            new_row.append('#')
        if ch == 'O':
            new_row.append('[')
            new_row.append(']')
        if ch == '@':
            new_row.append('@')
            new_row.append('.')
        if ch == '.':
            new_row.append('.')
            new_row.append('.')

    new_G.append(new_row)

print('Part 2:', solve(new_G))
