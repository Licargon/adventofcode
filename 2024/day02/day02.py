# -*- coding: utf-8 -*-

from collections import defaultdict, deque

import sys

with open('day02.in') as f:
    lines = [list(map(int, line.strip().split(' '))) for line in f.readlines()]


def is_safe(line):
    errors_ascending = 0
    for first, second in zip(line, line[1:]):
        if not (second > first and first+1 <= second <= first+3):
            errors_ascending += 1
    errors_descending = 0
    for first, second in zip(line, line[1:]):
        if not (first > second and second+1 <= first <= second+3):
            errors_descending += 1

    return errors_ascending == 0 or errors_descending == 0

part1 = 0
for line in lines:
    if is_safe(line):
        part1 += 1

print('Part 1:', part1)

part2 = 0
for line in lines:
    if is_safe(line):
        part2 += 1
    else:
        for i in range(len(line)):
            new_line = line[:i] + line[i+1:]
            if is_safe(new_line):
                part2 += 1
                break

print('Part 2: ', part2)
