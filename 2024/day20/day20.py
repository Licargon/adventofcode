# -*- coding: utf-8 -*-

from collections import defaultdict, deque, Counter
from pprint import pprint

import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day20.in') as f:
    lines = [line.strip() for line in f.readlines()]

G = [list(line) for line in lines]


for ir, row in enumerate(G):
    for ic, col in enumerate(row):
        if col == 'S':
            sr, sc = ir, ic
        if col == 'E':
            er, ec = ir, ic

dists = [['#'] * len(G[0]) for _ in G]
dists[sr][sc] = 0

SEEN = set()
Q = deque([(sr, sc)])
while Q:
    r, c = Q.popleft()
    if (r, c) in SEEN:
        continue
    SEEN.add((r, c))
    for dr, dc in DIRS:
        nr = r + dr
        nc = c + dc
        if 0 <= nr < len(G) and 0 <= nc < len(G[0]) and (nr, nc) not in SEEN:
            if G[nr][nc] == '#':
                continue
            else:
                dists[nr][nc] = dists[r][c] + 1
                Q.append((nr, nc))

def get_neighbours(r, c, dist):
    # Draw a 'diamond' around the point at dist 'dist' and return all
    points = set()
    for dr in range(dist+1):
        dc = dist - dr
        points.add((r + dr, c + dc))
        points.add((r + dr, c - dc))
        points.add((r - dr, c - dc))
        points.add((r - dr, c + dc))
    return points

part1 = 0

for ir, row in enumerate(G):
    for ic, col in enumerate(row):
        if col == '#':
            continue
        for nr, nc in get_neighbours(ir, ic, 2):
            if 0 <= nr < len(G) and 0 <= nc < len(G[0]):
                if G[nr][nc] == '#':
                    continue
                if dists[nr][nc] - dists[ir][ic] >= 102:
                    part1 += 1

print('Part 1:', part1)

part2 = 0
for ir, row in enumerate(G):
    for ic, col in enumerate(row):
        if col == '#':
            continue
        # Check all sets of points that are at distance 2 up to 20 around the current point and compare the dists
        for dist in range(2, 21):
            for nr, nc in get_neighbours(ir, ic, dist):
                if 0 <= nr < len(G) and 0 <= nc < len(G[0]):
                    if G[nr][nc] == '#':
                        continue
                    # 100 + dist because dist = time it takes for the skip
                    if dists[nr][nc] - dists[ir][ic] >= 100 + dist:
                        part2 += 1
print('Part 2:', part2)
#25715 too low