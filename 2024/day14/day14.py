# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from pprint import pprint

import re
import sys

DIRS = [(-1, 0), (0, 1), (1, 0), (0, -1)] # Up, right, down, left

with open('day14.in') as f:
    lines = [line.strip() for line in f.readlines()]

W = 101
H = 103

ROBOTS = []
for line in lines:
    px, py, vx, vy = map(int, re.findall(r'-?\d+', line))
    new_x = (px + 100*vx) % W
    new_y = (py + 100*vy) % H
    ROBOTS.append((py, px, vy, vx))

def print_grid(R, pr=False):

    G = [['.'] * W for _ in range(H)]
    for ry, rx, _, _ in R:
        G[ry][rx] = '#'

    FULL_OUT = ''
    for ir in range(H):
        row = ''
        for ic in range(W):
            row += str(G[ir][ic])
        if pr:
            print(row)
        FULL_OUT += row + '\n'

    return FULL_OUT


vertical_middle = H//2
horizontal_middle = W//2

def tick(R):
    new_R = []
    for py, px, vy, vx in R:
        new_R.append(((py + vy) % H, (px + vx) % W, vy, vx))
    return new_R


def count_quadrants(R: list):
    q1 = q2 = q3 = q4 = 0

    for ry, rx, _, _ in R:
        if ry == vertical_middle or rx == horizontal_middle:
            # In the center, ignore
            continue

        if ry < vertical_middle:
            if rx < horizontal_middle:
                q1 += 1
            else:
                q2 += 1
        else:
            if rx < horizontal_middle:
                q3 += 1
            else:
                q4 += 1
    return q1*q2*q3*q4

for _ in range(100):
    ROBOTS = tick(ROBOTS)

print('Part 1: ', count_quadrants(ROBOTS))

cur_max = count_quadrants(ROBOTS)

with open('OUT.txt', 'w') as f:

    for t in range(101, 10000):
        ROBOTS = tick(ROBOTS)
        new_out = print_grid(ROBOTS)
        if '#############################' in new_out:
            print('Part 2:', t)
            break

        f.write('TICK {} \n'.format(t))
        f.write(new_out)
        f.write('\n\n')

# 6531 too low