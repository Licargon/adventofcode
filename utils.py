def increase_grid(G, factor, start):
    # G = dict (row, col) -> value
    # Take a grid G, increases it by a factor 'factor'
    # Turns G into
    # G G G
    # G G G
    # G G G

    # Factor has to be odd for our stuff to work here
    assert factor % 2 == 1
    min_y = min(y for y, x in G)
    max_y = max(y for y, x in G)
    min_x = min(x for y, x in G)
    max_x = max(x for y, x in G)

    H = max_y - min_y + 1
    W = max_x - min_x + 1
    new_G = [['X' for _ in range(H*factor)] for _ in range(W*factor)]
    for fy in range(factor):
        for fx in range(factor):
            for (r, c), ch in G.items():
                if ch == 'S':
                    ch = '.'
                new_G[fy * H + r][fx * W + c] = ch

    # for (r, c), ch in G.items():
    #     for i in range(factor):
    #         new_G[i*H + r][i*W + c] = ch
    print(H, W)
    print(len(new_G), len(new_G[0]))
    # for row in new_G:
    #     print(''.join(row))
    # Convert back to dict
    ret = {}
    for r, row in enumerate(new_G):
        for c, col in enumerate(row):
            ret[(r, c)] = col
    return ret

