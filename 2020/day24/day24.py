import re
from copy import deepcopy

with open('day24.in') as f:
    input_lines = f.readlines()


class Hex(object):
    def __init__(self, q, r, flipped=False):
        self.q = q
        self.r = r
        self.flipped = flipped


def do_move(q, r, dir):
    if dir == 'e':
        return (q+1, r)
    elif dir == 'se':
        return (q, r+1)
    elif dir == 'sw':
        return (q-1, r+1)
    elif dir == 'w':
        return (q-1, r)
    elif dir == 'nw':
        return (q, r-1)
    elif dir == 'ne':
        return (q+1, r-1)
    else:
        raise NotImplemented()


def neighbours(q, r):
    neighbours = []
    for dir in ['e', 'se', 'sw', 'w', 'nw', 'ne']:
        neighbours.append(do_move(q, r, dir))
    return neighbours


reference_hex = Hex(0, 0)
HEXES = {(0, 0): reference_hex}


for line in input_lines:
    start_hex = reference_hex
    current_pos = (start_hex.q, start_hex.r)
    directions = re.findall(r'(e|se|sw|w|nw|ne)', line)
    for idx, direction in enumerate(directions):
        current_pos = do_move(*current_pos, direction)
        if current_pos not in HEXES:
            HEXES[current_pos] = Hex(*current_pos)
    HEXES[current_pos].flipped = not HEXES[current_pos].flipped

part1 = 0
for pos, hex in HEXES.items():
    if hex.flipped:
        part1 += 1

print('Part 1: ', part1)

for i in range(100):
    # First, we take our current hexes and add all non-existant neighbours to allow the board to expand
    NEW_HEXES = deepcopy(HEXES)
    before = len(NEW_HEXES.keys())
    for pos, hex in HEXES.items():
        for n in neighbours(*pos):
            if n not in NEW_HEXES:
                NEW_HEXES[n] = Hex(*n)

    # Check all hexes and flip them according to the rules
    PROCESSED_HEXES = deepcopy(NEW_HEXES)
    for pos, hex in PROCESSED_HEXES.items():
        valid_neighbours = [nn for nn in neighbours(*pos) if nn in NEW_HEXES]
        # print(len(valid_neighbours))
        flipped_neighbours = sum([1 for n in valid_neighbours if NEW_HEXES[n].flipped])
        if not hex.flipped and flipped_neighbours == 2:
            hex.flipped = True
        elif hex.flipped and (flipped_neighbours == 0 or flipped_neighbours > 2):
            hex.flipped = False

    # Finally, replace our initial hexes by our new state
    HEXES = PROCESSED_HEXES

part2 = 0
for pos, hex in HEXES.items():
    if hex.flipped:
        part2 += 1
print('Part 2: ', part2)
