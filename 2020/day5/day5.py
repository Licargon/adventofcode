from collections import Counter
import re

with open('day5.in') as f:
    lines = f.readlines()

MAX_ROW = 127
MIN_ROW = 0
MAX_COL = 7
MIN_COL = 0

def find_value(string, min_value, max_value):
    lo = min_value
    hi = max_value
    cur_index = 0
    while lo < hi and cur_index < len(string):
        c = string[cur_index]
        if string[cur_index] in ['F', 'L']:
            hi = lo + (hi-lo) // 2
        elif string[cur_index] in ['B', 'R']:
            lo = lo + (hi-lo) // 2 + 1
        cur_index += 1
    return lo

seat_ids = []

for line in lines:
    row_string = line[0:7]
    col_string = line[7:]

    row = find_value(row_string, 0, 127)

    col = find_value(col_string, 0, 7)

    seat_ids.append(row * 8 + col)

print('Part 1: ', max(seat_ids))

# Calculate what sum we should end up with for a list of length len(seat_ids) + 1
theoretical_sum = (len(seat_ids) * (len(seat_ids)+1))//2 + (len(seat_ids)+1) * min(seat_ids)
# Subtract the actual sum to find out which number is missing. We know this because
# 'so your seat should be the only missing boarding pass in your list.'
print('Part 2: ', (theoretical_sum) - sum(seat_ids))


