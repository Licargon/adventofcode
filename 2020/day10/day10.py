with open('day10.sample') as f:
    input_lines = f.readlines()

numbers = sorted([int(line) for line in input_lines])
# First adapter is the 'wall', with 0 jolts
numbers = [0] + numbers
# Last adapter is the device with a diff of 3 jolts
numbers.append(numbers[-1] + 3)
diff_1 = 0
diff_3 = 0

for number, number_next in zip(numbers, numbers[1:]):
    if number_next - number == 1:
        diff_1 += 1
    elif number_next - number == 3:
        diff_3 += 1

print('Part 1: ', diff_1*diff_3)

possibilities = [1] + [0] * numbers[-1]

for number in numbers[1:]:
    possibilities[number] = possibilities[number-3] + possibilities[number-2] + possibilities[number-1]
print('Part 2: ', possibilities[-1])

