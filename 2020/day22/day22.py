with open('day22.in') as f:
    input_lines = f.readlines()

input_lines = input_lines[1:]

p1 = []
p2 = []
cur_deck = p1
for line in input_lines:
    line = line.strip('\n')
    if line.startswith('Player'):
        continue
    if line:
        cur_deck.append(int(line))
    else:
        cur_deck = p2


p1_deck = p1[:]
p2_deck = p2[:]
rounds = 1
while bool(p1_deck) and bool(p2_deck):
    rounds += 1
    p1_card = p1_deck.pop(0)
    p2_card = p2_deck.pop(0)
    if p1_card > p2_card:
        winner = p1_deck
    else:
        winner = p2_deck

    winner.append(max(p1_card, p2_card))
    winner.append(min(p1_card, p2_card))

print(rounds)
part1 = 0
for idx, card in enumerate(reversed(winner), start=1):
    part1 += idx * card

print('Part 1: ', part1)


def recursive_combat(deck1, deck2, depth=1):
    SEEN = set()
    while bool(deck1) and bool(deck2):
        if (''.join([str(c) for c in deck1]), ''.join([str(c) for c in deck2])) in SEEN:
            # Player 1 wins due to repetition
            return 0
        else:
            SEEN.add((''.join([str(c) for c in deck1]), ''.join([str(c) for c in deck2])))
            p1_card = deck1.pop(0)
            p2_card = deck2.pop(0)
            if p1_card <= len(deck1) and p2_card <= len(deck2):
                # Take the first p1_card/p2_card number of cards from each deck and start a new game.
                winner = recursive_combat(deck1[:][0:p1_card], deck2[:][0:p2_card], depth+1)
                if winner == 0:
                    deck1.extend([p1_card, p2_card])
                else:
                    deck2.extend([p2_card, p1_card])
            else:
                # Normal rules
                if p1_card > p2_card:
                    winner = deck1
                else:
                    winner = deck2

                winner.append(max(p1_card, p2_card))
                winner.append(min(p1_card, p2_card))

    return 0 if deck1 else 1


p1_deck = p1[:]
p2_deck = p2[:]
winner_deck = recursive_combat(p1_deck, p2_deck)
print('Winner: ', winner_deck)
winner_deck = p1_deck if winner_deck == 0 else p2_deck
print('Winner deck: ', winner_deck)

part2 = 0
for idx, card in enumerate(reversed(winner_deck), start=1):
    part2 += idx * card

print('Part 2: ', part2)
