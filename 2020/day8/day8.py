OP_ACC = 'acc'
OP_JMP = 'jmp'
OP_NOP = 'nop'


class Program(object):
    def __init__(self, instruction_lines, timeout=None):
        self.instructions = instruction_lines
        self.IP = 0
        self.timeout = timeout

    def parse_instruction(self):
        instruction = self.instructions[self.IP]
        if instruction.startswith('nop'):
            return {
                'opcode': OP_NOP,
            }
        elif instruction.startswith('acc'):
            args = instruction.split(' ')[1]
            return {
                'opcode': OP_ACC,
                'value': int(args)
            }
        elif instruction.startswith('jmp'):
            args = instruction.split(' ')[1]
            return {
                'opcode': OP_JMP,
                'value': int(args)
            }
        return None

    def run(self, part1=False):
        """
        Returns acc if the program finishes succesfully, None otherwise
        """
        t = 0
        ACC = 0
        # Part 1: Keep track of which IP's we have checked.
        SEEN = set()
        while True:
            if self.IP == len(self.instructions):
                # Program terminated normally
                return ACC

            if part1 and self.IP in SEEN:
                return ACC
            else:
                SEEN.add(self.IP)
                instruction = self.parse_instruction()

            if instruction['opcode'] == OP_NOP:
                self.IP += 1
            elif instruction['opcode'] == OP_ACC:
                ACC += instruction['value']
                self.IP += 1
            elif instruction['opcode'] == OP_JMP:
                self.IP += instruction['value']

            # Timeout support
            t += 1
            if self.timeout and t > self.timeout:
                # If we ran more instructions than allowed, we conclude that our program will not terminate
                return None


with open('day8.in') as f:
    input_lines = f.readlines()
lines = ''.join(input_lines).split('\n')

# Part 1: See the value of ACC right before an instruction is executed twice
p = Program(lines)
print('Part 1: ', p.run(part1=True))

# Part 2: Change 1 JMP to a NOP or vice versa, and see if it completes. If so: Print the ACC value
for i in range(len(lines)):
    copy_data = lines[:]
    if copy_data[i].startswith('nop'):
        value = copy_data[i].split(' ')[1]
        copy_data[i] = 'jmp ' + value
    elif copy_data[i].startswith('jmp'):
        value = copy_data[i].split(' ')[1]
        copy_data[i] = 'nop ' + value
    p = Program(copy_data, timeout=1000)
    result = p.run()
    if result is not None:
        print('Part 2: ', result)
        break

