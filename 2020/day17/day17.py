import math


def compute(part1=False):
    ACTIVE = set()
    for row in range(len(input_lines)):
        for col in range(len(input_lines[row])):
            if input_lines[row][col] == '#':
                # x, y, z, w
                ACTIVE.add((col, row, 0, 0))

    min_x = min_y = min_z = min_w = math.inf
    max_x = max_y = max_z = max_w = -math.inf
    # Calculate max boundaries for the currently active boxes so we dont search too big of a space
    for x,y,z,w in ACTIVE:
        min_x = min([x, min_x])
        max_x = max([x, max_x])
        min_y = min([y, min_y])
        max_y = max([y, max_y])
        min_z = min([z, min_z])
        max_z = max([z, max_z])
        min_w = min([w, min_w])
        max_w = max([w, max_w])

    for i in range(6):
        NEW_ACTIVE = set()
        w_range = [0] if part1 else range(min_w-1, max_w+2)
        for x in range(min_x-1, max_x+2):
            for y in range(min_y-1, max_y+2):
                for z in range(min_z-1, max_z+2):
                    for w in w_range:
                        NEIGHBOURS = set()
                        for dx in [-1, 0, 1]:
                            for dy in [-1, 0, 1]:
                                for dz in [-1, 0, 1]:
                                    for dw in [-1, 0, 1]:
                                        # Skip the point itself
                                        if (x+dx, y+dy, z+dz, w+dw) == (x, y, z, w):
                                            continue
                                        NEIGHBOURS.add((x+dx, y+dy, z+dz, w+dw))

                        if (x,y,z,w) in ACTIVE:
                            if len(NEIGHBOURS.intersection(ACTIVE)) in [2, 3]:
                                NEW_ACTIVE.add((x,y,z,w))
                        else:
                            if len(NEIGHBOURS.intersection(ACTIVE)) == 3:
                                NEW_ACTIVE.add((x,y,z,w))

        ACTIVE = NEW_ACTIVE
        # Recalculate the boundaries, as the space might've grown/shrunk
        for x, y, z, w in ACTIVE:
            min_x = min([x, min_x])
            max_x = max([x, max_x])
            min_y = min([y, min_y])
            max_y = max([y, max_y])
            min_z = min([z, min_z])
            max_z = max([z, max_z])
            min_w = min([w, min_w])
            max_w = max([w, max_w])
    return len(ACTIVE)


with open('day17.in') as f:
    input_lines = [line.strip('\n') for line in f.readlines() if line != '\n']


print('Part 1: ', compute(True))
print('Part 2: ', compute(False))
