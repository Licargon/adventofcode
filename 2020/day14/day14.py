import re


def apply_mask_p1(int_value, mask):
    value_binary = format(int(int_value), 'b')
    value_binary = list(value_binary.zfill(36))
    for idx, b in enumerate(mask):
        if b != 'X':
            value_binary[idx] = b

    return int(''.join(value_binary), 2)


def recursively_generate_floating_addresses(cur_index, address, results):
    # Loop through the address. If we encounter an X, we replace it by 0 and 1 and keep going
    if cur_index == len(address):
        results.append(address)
    else:
        if address[cur_index] == 'X':
            copy1 = address[:]
            copy1[cur_index] = '0'
            recursively_generate_floating_addresses(cur_index + 1, copy1, results)
            copy2 = address[:]
            copy2[cur_index] = '1'
            recursively_generate_floating_addresses(cur_index + 1, copy2, results)
        else:
            recursively_generate_floating_addresses(cur_index + 1, address, results)


def get_address_for_mask(address_int, mask):
    value_binary = format(int(address_int), 'b')
    value_binary = list(value_binary.zfill(36))
    for idx, b in enumerate(mask):
        if b == '0':
            continue
        else:
            value_binary[idx] = b

    if 'X' in value_binary:
        addresses = []
        recursively_generate_floating_addresses(0, value_binary, addresses)
        return [int(''.join(address), 2) for address in addresses]
    else:
        return [int(''.join(value_binary), 2)]


def part1(lines):
    memory = dict()
    mask = ''
    for line in lines:
        if line.startswith('mask'):
            mask = line.split('=')[1].strip('\n').strip()
        else:
            address, value = re.findall(r'mem\[(\d+)\] = ([0-9]+)', line)[0]
            memory[int(address)] = apply_mask_p1(int(value), mask)

    ans = 0
    for key, value in memory.items():
        ans += value

    print('Part 1: ', ans)


def part2(lines):
    memory = dict()
    mask = ''
    for line in lines:
        if line.startswith('mask'):
            mask = line.split('=')[1].strip('\n').strip()
        else:
            address, value = re.findall(r'mem\[(\d+)\] = ([0-9]+)', line)[0]
            for address in get_address_for_mask(int(address), mask):
                memory[address] = int(value)

    ans = 0
    for key, value in memory.items():
        ans += value

    print('Part 2: ', ans)


with open('day14.in') as f:
    input_lines = f.readlines()

part1(input_lines[:])
part2(input_lines[:])





