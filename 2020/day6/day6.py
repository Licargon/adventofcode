from collections import Counter
import re

with open('day6.in') as f:
    input_lines = f.readlines()

split_lines = [line.split('\n') for line in ''.join(input_lines).split('\n\n')]

part1 = 0
part2 = 0
for group in split_lines:
    answer_set = set(group[0])
    for answers in group[1:]:
        answer_set &= set(answers)

    part2 += len(answer_set)

    total_set = set()
    for person_answers in group:
        for answer in person_answers:
            total_set.add(answer)

    part1 += len(total_set)

print('Part 1: ', part1)
print('Part 2: ', part2)
