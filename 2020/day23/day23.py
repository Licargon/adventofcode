import math


class LinkedList(object):
    def __init__(self):
        self.index = {}
        self.head = LinkedNode(None)
        self.head.next = self.head
        self.head.prev = self.head

    def __getitem__(self, item):
        return self.index[item]

    def add(self, value):
        new_node = LinkedNode(value)

        if not self.index:
            self.head = new_node
            self.head.next = new_node
            self.head.prev = new_node
        else:
            tmp = self.head.next
            self.head.next = new_node
            new_node.prev = self.head
            new_node.next = tmp
            self.head = new_node

        self.index[value] = new_node

    def add_list(self, after, values):
        current = self.index[after]
        tmp = current.next
        for v in values:
            new_node = LinkedNode(v)
            new_node.prev = current
            current.next = new_node
            current = new_node
            self.index[v] = new_node
        current.next = tmp
        tmp.prev = current

    def remove(self, value):
        node = self.index[value]
        node.prev.next = node.next
        node.next.prev = node.prev
        if self.head == node:
            self.head = self.head.next
        self.index.pop(value)


class LinkedNode(object):
    def __init__(self, value):
        self.prev = None
        self.next = None
        self.value = value


def play_cups(cups, rounds, start_cup):
    min_cup = math.inf
    max_cup = -math.inf
    h = cups.head
    for i in range(len(cups.index)):
        min_cup = min(h.value, min_cup)
        max_cup = max(h.value, max_cup)
        h = h.next
    current_cup = cups[start_cup]

    for round in range(rounds):
        next_3_cups = []
        next_cup = current_cup.next
        for i in range(3):
            next_3_cups.append(next_cup.value)
            next_cup = next_cup.next
        destination_cup = current_cup.value - 1
        if destination_cup < min_cup:
            destination_cup = max_cup
        while destination_cup in next_3_cups:
            destination_cup -= 1
            if destination_cup < min_cup:
                destination_cup = max_cup

        # Remove to-be-moved cups from cups
        for cup in next_3_cups:
            cups.remove(cup)

        cups.add_list(destination_cup, next_3_cups)
        h = cups[3]
        cur = ''
        for i in range(10):
            cur += ' ' + str(h.value)
            h = h.next

        current_cup = current_cup.next


sample = '389125467'
input = '712643589'


cups = LinkedList()
for c in input:
    cups.add(int(c))

play_cups(cups, 100, start_cup=int(input[0]))

node = cups[1].next
part1 = ''
for i in range(len(cups.index)-1):
    part1 += str(node.value)
    node = node.next
print('Part 1: ', part1)

cups = LinkedList()
for c in input:
    cups.add(int(c))
i = len(cups.index) + 1
while i < 1000001:
    cups.add(i)
    i += 1
play_cups(cups, 10000000, start_cup=int(input[0]))

node1 = cups[1].next
node2 = node1.next
print('Part 2: ', (node1.value*node2.value))
