with open('day25.in') as f:
    input_lines = f.readlines()

card_public_key = int(input_lines[0].strip('\n'))
door_public_key = int(input_lines[1].strip('\n'))


def get_loop_size(public_key, subject_number=7):
    loop_size = 1
    while True:
        value = pow(subject_number, loop_size, 20201227)
        if value == public_key:
            return loop_size
        loop_size += 1

card_loop_size = get_loop_size(card_public_key)

decryption_key = pow(door_public_key, card_loop_size, 20201227)
print(decryption_key)
