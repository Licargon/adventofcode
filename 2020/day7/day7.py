import re
from collections import defaultdict, deque

with open('day7.in') as f:
    input_lines = f.readlines()
lines = ''.join(input_lines).split('\n')

bag_contained_in = defaultdict(set)
bag_contains = defaultdict(list)
for line in lines:
    split = re.split(r'([a-z ]+) bags contain', line)
    outer_bag = split[1].strip()
    if split[2].lstrip().startswith('no other'):
        continue
    for amount, inner_bag in re.findall(r'(\d+) ([\w ]+) bag[s]*[,.]*', split[2].lstrip()):
        bag_contained_in[inner_bag].add(outer_bag)
        bag_contains[outer_bag].append({'bag_type': inner_bag, 'amount': int(amount)})

# Part 1
# Start at shiny gold, figure out its parents and keep searching 'up', disregardings bags we already know about.
SEEN = set()
queue = deque(['shiny gold'])
SEEN.add('shiny gold')
ctr = 0
while queue:
    current = queue.popleft()
    for bag in bag_contained_in[current]:
        if bag in SEEN:
            continue
        else:
            ctr += 1
            SEEN.add(bag)
            queue.append(bag)

print('Part 1: ', ctr)

# Part 2:
# Start at shiny gold, count what is contained in it, then add all those bags to the queue
queue = deque(['shiny gold'])
ctr = 0
while queue:
    current = queue.popleft()
    for bag in bag_contains[current]:
        ctr += bag['amount']
        for i in range(bag['amount']):
            queue.append(bag['bag_type'])

print('Part 2: ', ctr)
