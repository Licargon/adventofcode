from collections import Counter

lines = []
for line in open('day3.in'):
    lines.append(line.strip('\n'))

slopes = [(1,1), (3,1), (5,1), (7,1), (1,2), ]

row = col = 0
ans = 1
for slope_x, slope_y in slopes:
    trees = 0
    row = col = 0
    while row < len(lines):
        if lines[row][col % len(lines[row])] == '#':
            trees += 1
        row += slope_y
        col += slope_x
    if (slope_x, slope_y) == (3, 1):
        print('Part 1: ', trees)
    ans *= trees

print('Part 2: ', ans)
