from collections import Counter

lines = []
for line in open('day2.in'):
    lines.append(line)

valid_part1 = 0
valid_part2 = 0
for line in lines:
    parts = line.split(':')
    pattern = parts[0].strip()
    amounts = pattern.split(' ')[0].strip()
    lo = int(amounts.split('-')[0].strip())
    hi = int(amounts.split('-')[1].strip())
    letter = pattern.split(' ')[1].strip()
    string = parts[1].strip()
    ctr = Counter(string)
    if letter in ctr and lo <= ctr.get(letter) <= hi:
        valid_part1 += 1
    if bool(string[lo-1] == letter) != bool(string[hi-1] == letter):
        valid_part2 += 1

print('Part 1: ', valid_part1)
print('Part 2: ', valid_part2)
