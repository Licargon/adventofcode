with open('day12.in') as f:
    input_lines = f.readlines()

commands = ''.join(input_lines).split('\n')
commands = [(c[0], int(c[1:])) for c in commands]

def part1(commands):
    # N, E
    dir = [0, 1]
    # N, E
    pos = [0, 0]

    for command, magnitude in commands:
        if command == 'N':
            pos[0] += magnitude
        elif command == 'E':
            pos[1] += magnitude
        elif command == 'S':
            pos[0] -= magnitude
        elif command == 'W':
            pos[1] -= magnitude
        elif command == 'L':
            for i in range(magnitude//90):
                dir = [dir[1], -dir[0]]
        elif command == 'R':
            for i in range(magnitude//90):
                dir = [-dir[1], dir[0]]
        elif command == 'F':
            pos[0] += dir[0] * magnitude
            pos[1] += dir[1] * magnitude
    return abs(pos[0]) + abs(pos[1])


def part2(commands):
    waypoint_pos = [1, 10]
    pos = [0, 0]
    for command, magnitude in commands:
        if command == 'N':
            waypoint_pos[0] += magnitude
        elif command == 'E':
            waypoint_pos[1] += magnitude
        elif command == 'S':
            waypoint_pos[0] -= magnitude
        elif command == 'W':
            waypoint_pos[1] -= magnitude
        elif command == 'L':
            for i in range(magnitude // 90):
                waypoint_pos = [waypoint_pos[1], -waypoint_pos[0]]
        elif command == 'R':
            for i in range(magnitude // 90):
                waypoint_pos = [-waypoint_pos[1], waypoint_pos[0]]
        elif command == 'F':
            pos[0] += waypoint_pos[0] * magnitude
            pos[1] += waypoint_pos[1] * magnitude
    return abs(pos[0]) + abs(pos[1])


print('Part 1: ', part1(commands))
print('Part 2: ', part2(commands))
