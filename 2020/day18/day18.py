"""
Example code taken from https://www.dabeaz.com/ply/example.html, adapted/simplified for this AoC assignment
"""

import ply.yacc as yacc
import ply.lex as lex

tokens = (
    'NUMBER',
    'PLUS', 'TIMES',
    'LPAREN', 'RPAREN',
)

# Tokens

t_PLUS = r'\+'
t_TIMES = r'\*'
t_LPAREN = r'\('
t_RPAREN = r'\)'


def t_NUMBER(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t


# Ignored characters
t_ignore = " \t"


def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


# Build the lexer
lexer = lex.lex()

def p_expression_binop(t):
    '''expression : expression PLUS expression
                  | expression TIMES expression'''
    if t[2] == '+':
        t[0] = t[1] + t[3]
    elif t[2] == '*':
        t[0] = t[1] * t[3]


def p_expression_group(t):
    'expression : LPAREN expression RPAREN'
    t[0] = t[2]


def p_expression_number(t):
    'expression : NUMBER'
    t[0] = t[1]


def p_error(t):
    print("Syntax error at '%s'" % t.value)


with open('day18.in') as f:
    input_lines = [line.strip('\n') for line in f.readlines() if line != '\n']


def part_one(input_lines):
    precedence = (
        ('left', 'PLUS', 'TIMES'),
        ('right', 'LPAREN')
    )
    parser = yacc.yacc()
    ans = 0
    for line in input_lines:
        ans += parser.parse(line)

    print('Part 1: ', ans)


def part_two(input_lines):
    precedence = (
        ('left', 'TIMES'),
        ('left', 'PLUS'),
        ('right', 'LPAREN')
    )
    parser = yacc.yacc()
    ans = 0
    for line in input_lines:
        ans += parser.parse(line)

    print('Part 2: ', ans)

part_one(input_lines)
part_two(input_lines)
