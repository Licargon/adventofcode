from collections import defaultdict


with open('day15.in') as f:
    input_lines = f.readlines()

initial_numbers = list(map(int, input_lines[0].strip('\n').split(',')))

# Mapping number -> [int] with [int] being a list of rounds the number has been spoken in
numbers_spoken = defaultdict(list)

for idx, n in enumerate(initial_numbers):
    numbers_spoken[n].append(idx+1)

round = len(initial_numbers) + 1
last_number_spoken = initial_numbers[-1]

while True:
    if len(numbers_spoken[last_number_spoken]) > 1:
        last_number_spoken = numbers_spoken[last_number_spoken][-1] - numbers_spoken[last_number_spoken][-2]
    else:
        last_number_spoken = 0
    if round == 2020:
        print('Part 1: ', last_number_spoken)
        # break
    if round == 30000000:
        print('Part 2: ', last_number_spoken)
        break

    # Only keep the last 2 rounds for memory reasons (AKA: useless optimization in this case)
    if len(numbers_spoken[last_number_spoken]) > 1:
        numbers_spoken[last_number_spoken] = [numbers_spoken[last_number_spoken][-1], round]
    else:
        numbers_spoken[last_number_spoken].append(round)
    round += 1
