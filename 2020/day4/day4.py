from collections import Counter
import re

with open('day4.in') as f:
    lines = f.readlines()
lines.append('\n')
REQUIRED_FIELDS = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

passports = []
current_passport_parts = []
for line in lines:
    if line != '\n':
        current_passport_parts.append(line.strip('\n'))
    if line == '\n':
        passports.append(' '.join(current_passport_parts))
        current_passport_parts = []


parsed_passports = []
for passport in passports:
    parsed_passports.append({part.split(':')[0]: part.split(':')[1] for part in passport.split(' ')})

valid_part1 = 0
valid_part2 = 0

for p in parsed_passports:
    if all([field in p for field in REQUIRED_FIELDS]):
        valid_part1 += 1
    else:
        continue

    # Part 2 validation
    if not (len(p['byr']) == 4 and 1920 <= int(p['byr']) <= 2002):
        continue
    if not (len(p['iyr']) == 4 and 2010 <= int(p['iyr']) <= 2020):
        continue
    if not (len(p['eyr']) == 4 and 2020 <= int(p['eyr']) <= 2030):
        continue

    h = p['hgt']
    if h[-2:] == 'cm':
        if not(150 <= int(h[:-2]) <= 193):
           continue
    elif h[-2:] == 'in':
        if not (59 <= int(h[:-2]) <= 76):
            continue
    else:
        continue

    color = p['hcl']
    if color[0] != '#':
        continue
    color_string = color[1:]
    if len(color_string) != 6:
        continue
    if not re.match('[0-9a-f]{6,}', color_string):
        continue

    if p['ecl'] not in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
        continue

    if not (len(p['pid']) == 9 and re.match('[0-9]{9,}', p['pid'])):
        continue

    valid_part2 += 1

print('Part 1: ', valid_part1)
print('Part 2: ', valid_part2)
