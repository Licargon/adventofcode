with open('day11.in') as f:
    state = list(map(lambda s: s.strip('\n'), f.readlines()))


def solve(state, part1=True):
    NEIGHBOUR_CUTOFF = 4 if part1 else 5
    while True:
        new_state = state[:]
        for y in range(len(state)):
            for x in range(len(state[y])):
                neighbours = []
                for y_diff in [-1, 0, 1]:
                    for x_diff in [-1, 0, 1]:
                        if x_diff == 0 and y_diff == 0:
                            continue
                        # Part 1
                        if part1:
                            if 0 <= x + x_diff < len(state[y]) and 0 <= y + y_diff < len(state):
                                neighbours.append(state[y+y_diff][x+x_diff])
                        else:
                            # Part 2: Scan along entire lines for the first chair we see
                            temp_xdiff = x_diff
                            temp_ydiff = y_diff
                            while (0 <= x + temp_xdiff < len(state[y])) and (0 <= y + temp_ydiff < len(state)):
                                if state[y+temp_ydiff][x+temp_xdiff] != '.':
                                    neighbours.append(state[y+temp_ydiff][x+temp_xdiff])
                                    break
                                temp_xdiff += x_diff
                                temp_ydiff += y_diff

                if state[y][x] == 'L':
                    if neighbours.count('#') == 0:
                        new_state[y] = new_state[y][:x] + '#' + new_state[y][x+1:]
                if state[y][x] == '#':
                    if neighbours.count('#') >= NEIGHBOUR_CUTOFF:
                        new_state[y] = new_state[y][:x] + 'L' + new_state[y][x + 1:]

        if new_state == state:
            return ''.join(state).count('#')
        else:
            state = new_state[:]


print('Part 1: ', solve(state[:], part1=True))
print('Part 2: ', solve(state[:], part1=False))
