from collections import Counter, defaultdict
from pprint import pprint

all_foods = set()
food_occurences = Counter()
mapping = {}

with open('day21.in') as f:
    input_lines = f.readlines()

for line in input_lines:
    foods = set(line.split('(')[0].rstrip().split(' '))
    all_foods |= foods
    allergens = line.split('(contains ')[1].rstrip(')\n').split(', ')
    food_occurences.update(foods)
    for allergen in allergens:
        if allergen not in mapping:
            mapping[allergen] = set(foods)
        else:
            mapping[allergen] &= set(foods)

# Combine all remaining foods in the mapping
combined = set()
for allergen, foods in mapping.items():
    combined |= foods

# print(all_foods-combined)
# print('All: ', all_foods)
# print('Combined: ', combined)
# print('Diff: ', all_foods - combined)

# The allergens which have NO possible food options together create the solution for part 1
ans = 0
for food in set(all_foods - combined):
    ans += food_occurences[food]

print('Part 1: ', ans)

# We find unique allergen-food pairings (allergens with only 1 possible food), and we remove that food from the others'
# list of possibilities
# We then sort by allergen and combine the foods to get part 2
FOUND = set()
while len(FOUND) < len(mapping.keys()):
    for allergen, foods in mapping.items():
        if allergen not in FOUND and len(foods) == 1:
            for allergen2, foods2 in mapping.items():
                if allergen == allergen2:
                    continue
                foods2 -= foods
            FOUND.add(allergen)

sorted_keys = sorted(list(mapping.keys()))
parts = []
for key in sorted_keys:
    parts.append(list(mapping[key])[0])

print('Part 2: ', ','.join(parts))
