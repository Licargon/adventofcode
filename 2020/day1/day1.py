numbers = []
for line in open('day1.in'):
    numbers.append(int(line))

for i in range(len(numbers)):
    for j in range(i+1, len(numbers)):
        if numbers[i] + numbers[j] == 2020:
            print('Part 1: ', numbers[i] * numbers[j])
        for k in range(j+1, len(numbers)):
            if numbers[i] + numbers[j] + numbers[k] == 2020:
                print('Part 2: ', numbers[i] * numbers[j] * numbers[k])