import re


with open('day16.in') as f:
    input_lines = f.readlines()

constraints_data, your_ticket_data, other_tickets_data = ''.join(input_lines).split('\n\n')

constraints = []

field_names = []

for line in constraints_data.split('\n'):
    field_names.append(line.split(': ')[0])
    ranges = re.findall('(\d+)-(\d+) or (\d+)-(\d+)',
                        line.split(': ')[1])
    if len(ranges):
        constraints.append(list(map(int, ranges[0])))


part1 = 0
valid_tickets = []
for ticket_line in other_tickets_data.strip('\n').split('\n')[1:]:
    values = re.findall('(\d+)', ticket_line)
    values = list(map(int, values))
    ticket_valid = True
    for value in values:
        valid = False
        for c in constraints:
            if c[0] <= value <= c[1] or c[2] <= value <= c[3]:
                valid = True

        if not valid:
            part1 += value
            ticket_valid = False

    if ticket_valid:
        valid_tickets.append(list(map(int, ticket_line.split(','))))


print('Part 1:', part1)
print('Valid tickets: {}/{}'.format(len(valid_tickets), len(other_tickets_data.split('\n')[1:])))


# Imagine the tickets of being in a matrix. This list keeps track of which constraints are valid for which column in
# the data
valid_constraints_for_column = [set()] * len(valid_tickets[0])

# We check, value by value, what constraints satisfy the entirety of this column (all first values, second, ...)
for column in range(len(valid_tickets[0])):
    # By default, we assume that a column is satified by every constraints. As soon as we find a value in the column
    # that fails atleast 1 constraint, we remove it from this list
    valid_constraints = set(list(range(len(constraints))))
    for valid_ticket in valid_tickets:
        value = valid_ticket[column]
        for idx, c in enumerate(constraints):
            if c[0] <= value <= c[1] or c[2] <= value <= c[3]:
                pass
            else:
                valid_constraints.remove(idx)

    valid_constraints_for_column[column] = valid_constraints

# We keep looking over all constraint-sets we found for each column. If we find a set of size 1, it means we found a
# column which values are ONLY satified by 1 constraint, AKA we know that that constraint counts for that value and
# that value ONLY so we can remove it from the other sets. By sheer luck this causes another 1-set to pop up, etc
while not all([len(constraint_set) == 1 for constraint_set in valid_constraints_for_column]):
    for idx, constraints_for_column in enumerate(valid_constraints_for_column):
        if len(constraints_for_column) == 1:
            for idx2, constraints_for_column2 in enumerate(valid_constraints_for_column):
                if idx == idx2:
                    continue
                element = list(constraints_for_column)[0]
                if element in constraints_for_column2:
                    constraints_for_column2.remove(list(constraints_for_column)[0])

part2 = 1
your_ticket_values = list(map(int, your_ticket_data.split('\n')[1].split(',')))
for idx, constraint_id in enumerate(valid_constraints_for_column):
    if field_names[list(constraint_id)[0]].startswith('departure'):
        part2 *= your_ticket_values[idx]

print('Part 2: ', part2)




