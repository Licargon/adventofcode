from pprint import pprint
import math
from collections import defaultdict

with open('day20.in') as f:
    input_lines = f.readlines()
if input_lines[-1] != '\n':
    input_lines.append('\n')


def rotate_matrix(matrix, count):
    new_matrix = matrix[:]
    for _ in range(count+1):
        new_matrix = [''.join(l) for l in list(zip(*new_matrix))[::-1]]
    return new_matrix


def flip_matrix(matrix):
    # Horizontal
    new_matrix = []
    for line in matrix:
        new_matrix.append(''.join(list(reversed(line))))
    return new_matrix


def flip_matrix_vertical(matrix):
    new_matrix = []
    for line in matrix[::-1]:
        new_matrix.append(line[:])
    return new_matrix


def edge_match(m1, m2):
    # Top/bottom match:
    if m1[-1] == m2[0] or m2[-1] == m1[0]:
        return True

    m1_left = [line[0] for line in m1]
    m1_right = [line[-1] for line in m1]
    m2_left = [line[0] for line in m2]
    m2_right = [line[-1] for line in m2]
    if m1_left == m2_right or m1_right == m2_left:
        return True

    return False


def matches_up(m1, m2):
    for i in range(4):
        m1 = rotate_matrix(m1, i)
        for j in range(4):
            m2 = rotate_matrix(m2, j)
            if edge_match(m1, m2):
                return True,
            m1_f = flip_matrix(m1)
            m2_f = flip_matrix(m2)
            matches = [
                edge_match(m1_f, m2),
                edge_match(m1, m2_f),
                edge_match(m1_f, m2_f),
                edge_match(m1, m2)
            ]
            if any(matches):
                return True
    return False


tiles = {}
tile = []
tile_id = None
for line in input_lines:
    if line == '\n':
        tiles[tile_id] = tile
        tile = []
        tile_id = None
    elif line.startswith('Tile'):
        tile_id = int(line.split(' ')[1][:-2])
    else:
        tile.append(line.strip('\n'))


no_of_tiles = len(tiles.keys())
sq_side = int(math.sqrt(no_of_tiles))

tile_ids = list(tiles.keys())

# match keeps track of which tile matches up with which other tile in ANY orientation
match = defaultdict(list)

for id_i in tile_ids:
    for id_j in tile_ids:
        if id_i == id_j:
            continue
        if matches_up(tiles[id_i], tiles[id_j]):
            match[id_i].append(id_j)


# Corners are tiles that only match up with 2 other tiles.
corners = [id for id,tiles in match.items() if len(tiles) == 2]
assert len(corners) == 4
ans = 1
for c in corners:
    ans *= c

print('Part 1: ', ans)

edges = [id for id,tiles in match.items() if len(tiles) == 3]
middles = [id for id,tiles in match.items() if len(tiles) == 4]

# print('{} Corners: '.format(len(corners)), corners)
# print('{} Edges: '.format(len(edges)), edges)
# print('{} Middles: '.format(len(middles)), middles)
assert len(tiles.keys()) == (len(corners) + len(edges) + len(middles))

full_image = [[None for _ in range(sq_side)] for _ in range(sq_side)]


def fill_edge_line(edge_line, corners, used_corners, edges, used_edges):
    last_edge = None
    for idx, cell in enumerate(edge_line, start=1):
        if idx == len(full_image[0]) - 1:
            # Pick the next corner
            for corner in set(corners) - used_corners:
                m = match[corner]
                if last_edge in match[corner]:
                    edge_line[-1] = corner
                    used_corners.add(corner)
                    break
        else:
            for edge in set(edges) - used_edges:
                if last_edge == None:  # We just got here from a corner
                    if edge_line[0] in match[edge]:
                        edge_line[idx] = edge
                        last_edge = edge
                        used_edges.add(edge)
                        break
                else:
                    if last_edge in match[edge]:
                        edge_line[idx] = edge
                        last_edge = edge
                        used_edges.add(edge)
                        break


# Start by constructing the edges
full_image[0][0] = corners[0]
used_corners = set()
used_corners.add(corners[0])
used_edges = set()
cur_edge = None

fill_edge_line(full_image[0], corners, used_corners, edges, used_edges)
left_edge = [row[0] for row in full_image]
fill_edge_line(left_edge, corners, used_corners, edges, used_edges)
for idx, cell in enumerate(left_edge):
    full_image[idx][0] = cell
right_edge = [row[-1] for row in full_image]
fill_edge_line(right_edge, corners, used_corners, edges, used_edges)
for idx, cell in enumerate(right_edge):
    full_image[idx][-1] = cell
fill_edge_line(full_image[-1], corners, used_corners, edges, used_edges)

# Fill in the middle cells
middle_matches = {}
for middle in middles:
    middle_matches[middle] = match[middle]

for row in range(1, len(full_image)-1):
    for col in range(1, len(full_image[0])-1):
        adjacent_cells = {full_image[row-1][col],
                          full_image[row+1][col],
                          full_image[row][col-1],
                          full_image[row][col+1]}
        for cell, cell_matches in middle_matches.items():
            if len(set(cell_matches).intersection(adjacent_cells)) >= 2:
                full_image[row][col] = cell
                middle_matches.pop(cell)
                break


# Time to properly rotate/flip the tiles so we can map out the actual field.

# We first properly align corner 1, so they the 2 unused edges between (corner, edge1, edge2) are top and left
def get_edges_for_tile(tile):
    # Top, bottom, left, right
    e = []
    e.append(tile[0])
    e.append(tile[-1])
    e.append(''.join([line[0] for line in tile]))
    e.append(''.join([line[-1] for line in tile]))
    return e

corner_edges = get_edges_for_tile(tiles[full_image[0][0]])
edge1_edges = get_edges_for_tile(tiles[full_image[0][1]])
edge2_edges = get_edges_for_tile(tiles[full_image[1][0]])

idxs = None
rotated_corner = tiles[full_image[0][0]]
while idxs != [0,2]:
    idxs = [0,1,2,3]
    rotated_corner = rotate_matrix(rotated_corner, 0)
    corner_edges = get_edges_for_tile(rotated_corner)
    for idx, e in enumerate(corner_edges):
        rev = ''.join(list(reversed(e)))
        if e in edge1_edges or e in edge2_edges or rev in edge1_edges or rev in edge2_edges:
            idxs.remove(idx)

full_image_rotated = [[None for _ in range(len(full_image[0]))] for _ in range(len(full_image))]

full_image_rotated[0][0] = rotated_corner

# Fill first line
for idx, cell_id in enumerate(full_image[0][1:], start=1):

    cell = tiles[cell_id]
    edge_to_match = [line[-1] for line in full_image_rotated[0][idx-1]]

    found = False
    for cell in [cell, flip_matrix(cell), flip_matrix_vertical(cell)]:
        original_cell = cell[:]
        if found:
            break
        for i in range(4):
            cell = rotate_matrix(original_cell, i)
            edge = [line[0] for line in cell]
            if edge == edge_to_match:
                full_image_rotated[0][idx] = cell
                found = True
                break

# Fill everything below line 1 lines
for idx in range(1, len(full_image)):
    # Match first edge piece
    edge_cell_id = full_image[idx][0]
    edge_cell = tiles[edge_cell_id]
    # Last line of cell on top
    top_edge_to_match = full_image_rotated[idx-1][0][-1]
    found = False
    for edge_cell in [edge_cell, flip_matrix(edge_cell), flip_matrix_vertical(edge_cell)]:
        original_cell = edge_cell[:]
        if found:
            break
        for i in range(4):
            edge_cell = rotate_matrix(original_cell, i)
            if edge_cell[0] == top_edge_to_match:
                full_image_rotated[idx][0] = edge_cell
                found = True
                break

    # annoying_cell = tiles[full_image[1][1]]
    # for line in annoying_cell:
    #     print(line)
    # raise Exception()
    # Match the rest of the row
    for idx_col, cell_id in enumerate(full_image[idx][1:], start=1):
        cell = tiles[cell_id]
        edge_to_match = [line[-1] for line in full_image_rotated[idx][idx_col - 1]]
        found = False
        for cell in [cell, flip_matrix(cell)]:
            original_cell = cell[:]
            if found:
                break
            for i in range(4):
                cell = rotate_matrix(original_cell, i)
                edge = [line[0] for line in cell]
                if edge == edge_to_match:
                    full_image_rotated[idx][idx_col] = cell
                    break


# print(full_image_rotated)
# Cut off the edges
full_image_cropped = [[None for _ in range(8*sq_side)] for _ in range(8*sq_side)]
for idx_row, row in enumerate(full_image_rotated):
    new_row = []
    for idx_col, col_cell in enumerate(row):
        for idx_line, line in enumerate(col_cell[1:-1]):
            for idx_char, char in enumerate(line[1:-1]):
                full_image_cropped[idx_row * 8 + idx_line][idx_col * 8 + idx_char] = char

for idx_row, row in enumerate(rotate_matrix(full_image_cropped, 3)):
    row_data = ''
    for idx, c in enumerate(row):
        # if idx % 8 == 0:
        #     row_data += ' '
        row_data += c

full_image_cropped = rotate_matrix(full_image_cropped, 3)


def find_nessies(sea):
    nessy = [
        '                  # ',
        '#    ##    ##    ###',
        ' #  #  #  #  #  #   '
    ]
    cnt = 0
    locations = []
    for x in range(len(sea)-20):
        for y in range(len(sea)-3):
            found = True
            for nessy_y in range(3):
                for nessy_x in range(20):
                    if nessy[nessy_y][nessy_x] == '#' and sea[y+nessy_y][x + nessy_x] != '#':
                        found = False
                        break

            if found:
                cnt += 1
                locations.append((y, x))

    return cnt, locations


for sea in [full_image_cropped, flip_matrix(full_image_cropped), flip_matrix_vertical(full_image_cropped)]:
    original_sea = sea[:]
    for i in range(4):
        sea_rotated = rotate_matrix(original_sea, i)
        nessies, locations = find_nessies(sea_rotated)
        if nessies > 0:
            # print('Part 2: ', nessies)
            print('Locations: ', locations)
            print('Part 2: ', ''.join(sea_rotated).count('#') - 15*len(locations))
            break
