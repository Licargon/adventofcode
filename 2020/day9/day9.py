PREAMBLE = 25

with open('day9.in') as f:
    input_lines = f.readlines()
numbers = list(map(int, ''.join(input_lines).split('\n')))
print(numbers)
for idx in range(PREAMBLE, len(numbers)):
    number = numbers[idx]
    valid = False
    for i in range(idx-PREAMBLE, idx):
        for j in range(idx-PREAMBLE+1, idx):
            if numbers[i] + numbers[j] == number:
                valid = True

    if not valid:
        print('Part 1: ', number)
        break

for i in range(len(numbers)):
    for j in range(i+2, len(numbers)):
        if sum(numbers[i:j]) == number:
            print('Part 2: ', (min(numbers[i:j]) + max(numbers[i:j])))
            break

