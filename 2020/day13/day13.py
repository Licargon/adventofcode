import math

import time

with open('day13.in') as f:
    input_lines = f.readlines()

depart_time = int(input_lines[0].strip('\n'))
split_times = input_lines[1].strip('\n').split(',')
bus_times = [int(split_time) for split_time in split_times if split_time != 'x']
bus_times_with_x = []
for bus_time in split_times:
    if bus_time != 'x':
        bus_times_with_x.append(int(bus_time))
    else:
        bus_times_with_x.append('x')


def part1(depart_time, bus_times):
    current_min = math.inf
    current_min_bus = None
    for bus_time in bus_times:
        near_time = (depart_time // bus_time) * bus_time + bus_time
        wait_time = near_time - depart_time
        if wait_time < current_min:
            current_min = wait_time
            current_min_bus = bus_time
    return current_min * current_min_bus


def part2_slow_iteration(bus_times):
    """
    This will probably never converge to a solution
    """
    START_OFFSET = 1
    max_time = max([bus_time for bus_time in bus_times if bus_time != 'x'])
    # Calculate start bus_time closest to the max bus_time
    start_max_time = (START_OFFSET // max_time) * max_time + max_time
    print('Starting search at time ', start_max_time)
    cur_time = start_max_time
    max_index = bus_times.index(max_time)

    while True:
        valid = True
        for idx, bus_time in enumerate(bus_times):
            if bus_time == 'x':
                # No need to check anything for wildcards
                continue
            time_to_check = cur_time - (max_index - idx)
            if time_to_check % bus_time != 0:
                valid = False
                break

        if not valid:
            # Move on to the next possibility
            cur_time += max_time
        else:
            # We need the timestamp of the first bus in the list, so we subtract max_index again.
            return cur_time - max_index


def part2_2(bus_times, START_TIME=100000000000000):
    constraints = []
    offset = 0
    for idx, bus_time in enumerate(bus_times):
        if bus_time != 'x':
            constraints.append((offset, bus_time))
            offset += 1
        else:
            offset += 1

    current = START_TIME
    step = 1
    for offset, bus_time in constraints:
        while (current + offset) % bus_time != 0:
            current += step
        step *= bus_time

    return current


print('Part 1: ', part1(depart_time, bus_times))

# tests = {
#     '17,x,13,19': 3417,
#     '67,7,59,61': 754018,
#     '67,x,7,59,61': 779210,
#     '67,7,x,59,61': 1261476,
#     '1789,37,47,1889': 1202161486
# }
# for input, result in tests.items():
#     test_input = []
#     for bus_time in input.split(','):
#         if bus_time != 'x':
#             test_input.append(int(bus_time))
#         else:
#             test_input.append('x')
#     assert part22(test_input, START_TIME=1) == result
# print('Tests OK')

print('Part 2: ', part2_2(bus_times_with_x))
