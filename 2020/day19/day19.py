import re

with open('day19.in') as f:
    input_lines = [line.strip('\n') for line in f.readlines()]

ruleset = {}


def get_re(rule_no, depth=1):
    # print('    ' * depth, rule_no)
    if '|' in rule_no:
        rule_lists = rule_no.split(' | ')
        return '(' + get_re(rule_lists[0], depth+1) + '|' + get_re(rule_lists[1], depth+1) + ')'
    elif len(rule_no.split(' ')) > 1:
        return ''.join([get_re(rule_part, depth+1) for rule_part in rule_no.split(' ')])
    else:
        rule = ruleset[rule_no]
        if re.match(r'"."', rule):
            return rule
        else:
            return get_re(rule, depth+1)


def get_re_part2(rule_no, depth=1):
    # print('    ' * depth, rule_no)
    if rule_no == '8':
        return '(' + get_re_part2('42') + ')+'
    elif rule_no == '11':
        rule42 = get_re_part2('42')
        rule31 = get_re_part2('31')
        parts = ['{rule42}{{{i}}}{rule31}{{{i}}}'.format(rule42=rule42, rule31=rule31, i=i) for i in range(1, 50)]
        return '(' + '|'.join(parts) + ')'
    if '|' in rule_no:
        rule_lists = rule_no.split(' | ')
        return '(' + get_re_part2(rule_lists[0], depth+1) + '|' + get_re_part2(rule_lists[1], depth+1) + ')'
    elif len(rule_no.split(' ')) > 1:
        return ''.join([get_re_part2(rule_part, depth+1) for rule_part in rule_no.split(' ')])
    else:
        rule = ruleset[rule_no]
        if re.match(r'"."', rule):
            return rule
        else:
            return get_re_part2(rule, depth+1)


msgs = []
scanning_rules = True
for line in input_lines:
    if not line:
        scanning_rules = False
        continue
    if scanning_rules:
        rule_no, rule = line.split(': ')
        assert rule_no not in ruleset
        ruleset[rule_no] = rule
    else:
        msgs.append(line)


regex = get_re('0')
regex = regex.replace('"', '')

part1 = 0

for msg in msgs:
    if bool(re.fullmatch(regex, msg)):
        part1 += 1

print('Part 1: ', part1)

regex = get_re_part2('0')
regex = regex.replace('"', '')

part2 = 0

for msg in msgs:
    if bool(re.fullmatch(regex, msg)):
        part2 += 1

print('Part 2: ', part2)

